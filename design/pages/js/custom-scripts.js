
$(function() {

	//tooltip init
	$('.tip').tooltip();
	
	//colorpicker init
	if($('.colorpicker-default').length > 0) {
		$('.colorpicker-default').colorpicker({
			format: 'hex'
		});
	}

	//panel toggle
	$('.panel.toggle .panel-heading').click(function() {
		if($(this).parent().hasClass('active')) {
			$(this).parent().removeClass('active');
		} else {
			$('.panel.toggle').removeClass('active');
			$(this).parent().toggleClass('active');
		}
		
		return false;
	});
	
});