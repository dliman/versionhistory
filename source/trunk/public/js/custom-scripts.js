jQuery.noConflict();

jQuery( document ).ready(function(  ) {

	//tooltip init
        jQuery('.tip').bootstrapTlp();
	jQuery('.tip-none').hide();
        
	//colorpicker init
	if(jQuery('.colorpicker-default').length > 0) {
		jQuery('.colorpicker-default').colorpicker({
			format: 'hex'
		});
	}

	//panel toggle
	jQuery('.panel.toggle .panel-heading').click(function() {
		if(jQuery(this).parent().hasClass('active')) {
			jQuery(this).parent().removeClass('active');
		} else {
			jQuery('.panel.toggle').removeClass('active');
			jQuery(this).parent().toggleClass('active');
		}
		
		return false;
	});
        
        var path = window.location.href;
       
        jQuery('.navbar-nav.navigate').find('li').each(function(){
            jQuery(this).removeClass('active');
            if(jQuery(this).find('a').attr('href') == path){
                jQuery(this).toggleClass('active');
            }
        });
        
        
               
        jQuery('.datepicker').datepicker();
        
        jQuery('.cancel').click(function(){
           var link = window.location.pathname;
           window.location.href = link;
        });
                
       // jQuery('.add-attachment').bootstrapFileInput();
       
});

