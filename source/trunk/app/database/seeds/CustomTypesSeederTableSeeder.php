<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CustomTypesSeederTableSeeder extends Seeder {

	public function run()
	{
		$now = date('Y-m-d H:i:s');
                $post =  array(
                    array(  'title'=>'added', 'show_label'=>'show', 'icon'=>'fa fa-plus-square', 'color'=>'#A9D86E', 'image'=>'', 'user_id'=>'0', 'project_id'=>'0', 'created_at'=> $now, 'updated_at'=>$now ),
                    array(  'title'=>'fixed', 'show_label'=>'show', 'icon'=>'fa fa-check-square', 'color'=>'#59ace2', 'image'=>'', 'user_id'=>'0', 'project_id'=>'0', 'created_at'=> $now, 'updated_at'=>$now ),
                    array(  'title'=>'removed', 'show_label'=>'show', 'icon'=>'fa fa-minus-square', 'color'=>'#FF6C60', 'image'=>'', 'user_id'=>'0', 'project_id'=>'0', 'created_at'=> $now, 'updated_at'=>$now ),
                );
		DB::table('custom_types')->insert($post);
	}

}