<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersAdminTableSeeder extends Seeder {

	public function run()
	{
                DB::table('users_admin')->insert([
                    'username'   => 'vh_admin',
                    'password'   => Hash::make('mhoBreLDJ3c8'),
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()     
                ]);
	}

}