<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHashSubscribeUpdates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('subscribe_updates', function(Blueprint $table)
		{
			$table->string( 'token' , 60 )->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('subscribe_updates', function(Blueprint $table)
		{
			$table->dropColumn( 'token' );
		});
	}

}
