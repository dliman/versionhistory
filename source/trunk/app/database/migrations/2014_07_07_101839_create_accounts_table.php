<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'accounts', function( $table )
		{
			$table->increments( 'id' );
                        $table->string( 'first_name' );
                        $table->string( 'last_name' );
                        $table->string( 'phone' );
                        $table->enum( 'resive', array('yes','no'))->default('no');
                        $table->integer( 'bill_card' );
                        $table->integer( 'expire_m' );
                        $table->integer( 'expire_y' );
                        $table->string( 'zip' );
                        $table->integer( 'user_id' );
                        $table->timestamps();
		});
                
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::drop( 'accounts' );
	}

}
