<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomizeEmailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customize_email', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->text('subject');
                        $table->text('body');
                        $table->time('send');
                        $table->integer('user_id');
                        $table->integer('project_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customize_email');
	}

}
