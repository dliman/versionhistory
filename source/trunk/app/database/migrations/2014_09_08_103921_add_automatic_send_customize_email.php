<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutomaticSendCustomizeEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customize_email', function(Blueprint $table)
		{
			$table->enum( 'send_automatic', array('yes','no'))->default('yes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customize_email', function(Blueprint $table)
		{
			$table->dropColumn( 'send_automatic' );
		});
	}

}
