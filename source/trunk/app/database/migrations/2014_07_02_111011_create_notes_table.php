<?php

	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateNotesTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			//
			Schema::create( 'notes', function ( $table ) {

				$table->increments( 'id' );

				$table->string( 'title' );
                                $table->string( 'slug' );
				$table->integer( 'type_id' );
				$table->text( 'description' );
				$table->integer( 'user_id' );
				$table->integer( 'section_id' );
                                $table->integer( 'project_id' );
                                $table->integer( 'company_id' );
                                $table->timestamps();

			} );
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			//
		}

	}
