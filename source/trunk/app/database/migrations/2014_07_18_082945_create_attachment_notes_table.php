<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttachmentNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attachment_notes', function( $table )
		{
			$table->increments('id');
                        $table->integer( 'note_id' );
                        $table->text( 'attachment' );
                        $table->timestamps();
			
		});
                
                
                
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attachment_notes');
	}

}
