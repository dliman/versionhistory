<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTimeZones extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('localization_settings', function(Blueprint $table)
		{
                        $table->dropColumn( 'time_zone' );
		});
		Schema::table('localization_settings', function(Blueprint $table)
		{
                        $table->string( 'time_zone',64)->default('America/Los_Angeles');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('localization_settings', function(Blueprint $table)
		{
                        $table->dropColumn( 'time_zone' );
		});
		Schema::table('localization_settings', function(Blueprint $table)
		{
                        $table->enum( 'time_zone', array(
                            'America/Los_Angeles',
                            'America/Denver', 
                            'America/Chicago', 
                            'America/New_York'
                            ))->default('America/Los_Angeles');
		});
	}

}
