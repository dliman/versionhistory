<?php

	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUsersTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			//
			Schema::create( 'users', function ( $table ) {
				$table->increments( 'id' );
				$table->string( 'email' )->unique();
				$table->integer( 'company_id' )->default(0);
                                $table->string( 'password' );
                                $table->string( 'first_name' )->nullable();
                                $table->string( 'last_name' )->nullable();
                                $table->string( 'phone' )->nullable();
                                $table->enum( 'resive', array('yes','no'))->default('no');
                                $table->integer( 'parent_id' )->default(0);
                                $table->string( 'remember_token' )->nullable();

				$table->timestamps();
			} );
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			//
			Schema::drop( 'users' );
		}

	}
