<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeaderTextLayoutSettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('layout_settings', function(Blueprint $table)
		{
                        $table->string( 'header_text_color' );
                        $table->enum( 'header_text_show', array('hide','show'))->default('show');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('layout_settings', function(Blueprint $table)
		{
			$table->dropColumn( 'header_text_color' );
                        $table->dropColumn( 'header_text_show' );
		});
	}

}
