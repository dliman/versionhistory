<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCronSubscribeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cron_subscribe', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->time('send');
                        $table->integer('project_id');
                        $table->text('subject');
                        $table->text('body');
                        $table->enum('status', array( 'queue', 'done' ))->default('queue');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cron_subscribe');
	}

}
