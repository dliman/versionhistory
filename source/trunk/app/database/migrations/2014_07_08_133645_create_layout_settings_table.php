<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create( 'layout_settings', function ( $table ) {
                        $table->increments( 'id' );
                        $table->string( 'header_image' );
                        $table->string( 'header_background' );
                        $table->enum( 'hide_header_area', array('hide','show'))->default('show');
                        $table->string( 'background_image' );
                         $table->string( 'background_color' );
                        $table->text( 'header_code' );
                        $table->text( 'footer_code' );
                        $table->text( 'custom_css' );
                        $table->integer( 'project_id' );
                        $table->integer( 'user_id' );
                        
                        $table->timestamps();
                } );
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('layout_settings', function(Blueprint $table)
		{
			//
		});
	}

}
