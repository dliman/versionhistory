<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom_types', function(Blueprint $table)
		{
			$table->increments( 'id' );
                        $table->string( 'title' );
                        $table->enum( 'show_label', array('hide','show'))->default('show');
                        $table->string( 'icon' );
                        $table->string( 'color' );
                        $table->string( 'image' );
                        $table->integer( 'user_id' );
                        $table->integer( 'project_id' );
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('custom_types');
	}

}
