<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'project_settings', function ( $table ) {
                        $table->increments( 'id' );
                        $table->string( 'page_title' );
                        $table->text( 'meta_description' );
                        $table->text( 'meta_keywords' );
                        $table->string( 'custom_domain' );
                        $table->string( 'upload_logo' );
                        $table->string( 'upload_favicon' );
                        $table->integer( 'user_id' );
                        $table->integer( 'project_id' );
                        $table->timestamps();
                                
                } );  
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'project_settings' );
	}

}
