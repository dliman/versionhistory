<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create( 'feature_settings', function ( $table ) {
                        $table->increments( 'id' );
                        $table->enum( 'public_private', array('public','private'))->default('public');
                        $table->enum( 'show_search_box', array('yes','no'))->default('no');
                        $table->enum( 'show_feature_request_box', array('yes','no'))->default('no');
                        $table->enum( 'show_download_pdf', array('yes','no'))->default('no');
                        $table->enum( 'show_signup_email', array('yes','no'))->default('no');
                        $table->integer( 'project_id' );
                        $table->integer( 'user_id' );
                        $table->timestamps();
                                
                } );
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feature_settings', function(Blueprint $table)
		{
			//
		});
	}

}
