<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIconsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create( 'icons', function ( $table ) {
                        $table->increments( 'id' );
                        $table->string( 'key' );
                        $table->string( 'value' );
                        $table->integer( 'user_id' );
                        $table->timestamps();
                } );
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('icons', function(Blueprint $table)
		{
			//
		});
	}

}
