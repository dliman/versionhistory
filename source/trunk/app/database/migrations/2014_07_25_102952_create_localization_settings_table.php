<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalizationSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('localization_settings', function($table)
		{
			$table->increments('id');
                        $table->enum( 'date_format', array(
                            'm-d-Y',
                            'd-m-Y', 
                            'F d, Y', 
                            'd F Y'
                            ))->default('m-d-Y');
                        $table->enum( 'time_zone', array(
                            'America/Los_Angeles',
                            'America/Denver', 
                            'America/Chicago', 
                            'America/New_York'
                            ))->default('America/Los_Angeles');
                        $table->integer( 'user_id' );
                        $table->integer( 'project_id' );
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('localization_settings');
	}

}
