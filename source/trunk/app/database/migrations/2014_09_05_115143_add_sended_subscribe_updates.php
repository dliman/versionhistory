<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSendedSubscribeUpdates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('subscribe_updates', function(Blueprint $table)
		{
                        $table->timestamp('sended_at');
			$table->index('sended_at');
		});

		Schema::table('projects', function(Blueprint $table)
		{
			$table->index(array('updated_at'));
		});

		Schema::table('sections', function(Blueprint $table)
		{
			$table->index(array('project_id','updated_at'));
		});

		Schema::table('notes', function(Blueprint $table)
		{
			$table->index(array('section_id','updated_at'));
		});
        }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('subscribe_updates', function(Blueprint $table)
		{
                        $table->dropColumn( 'sended_at' );
		});

		Schema::table('projects', function(Blueprint $table)
		{
			$table->dropIndex(array('updated_at'));
		});

		Schema::table('sections', function(Blueprint $table)
		{
			$table->dropIndex(array('project_id','updated_at'));
		});

		Schema::table('notes', function(Blueprint $table)
		{
			$table->dropIndex(array('section_id','updated_at'));
		});
        }

}
