<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => array(
		'domain' => 'sandboxd86cd6511def416c9ccf1618977efb0e.mailgun.org',
		'secret' => 'key-fea6d5923363cde1a6fffc4d3832ae78',
	),

	'mandrill' => array(
		'secret' => '',
	),

	'stripe' => array(
		'model'  => 'User',
		'secret' => 'sk_test_GiMu7aH0Gik6eNjySdDP1Dpi',
                'pub_key'=> 'pk_test_xaW7hpk8mlxyIIBFD0mUbY9F',
	),

);
