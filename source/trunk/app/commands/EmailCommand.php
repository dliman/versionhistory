<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EmailCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:send_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'it will send email to users if a project has any update.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $intervalDays = 1;
        $to = Carbon::now();
        $from = Carbon::now()->subDays($intervalDays);

        $subscribes = SubscribeUpdate::where('sended_at', '<', $from)->with(array(
                    'project.user.customize_email' /* => function($query) use($to) { $query->where('send', '<', $to ); } */,
                ))->get();
        if ($subscribes) {
            $projectIds = [];
            foreach ($subscribes as $subscribe) {
                if (
                       ( !isset($subscribe->project->user->customize_email->send_automatic) ||
                        (isset($subscribe->project->user->customize_email->send_automatic) && $subscribe->project->user->customize_email->send_automatic == 'yes') )
                    &&    
                       ( !isset($subscribe->project->user->customize_email->send) ||
                        (isset($subscribe->project->user->customize_email->send) && $subscribe->project->user->customize_email->send <= $to->toTimeString()) )
                    ) {
                    $projectIds[$subscribe->project_id] = $subscribe->project_id;
                } else {
                    Log::error(array($subscribe->project_id, $subscribe->project->user->customize_email->send, $to->toTimeString(), 'Disabled or It is too early...'));
                }
            }
//Log::error($projectIds);
            if ($projectIds) {
                $projects = Project::whereIn('id', $projectIds)->whereBetween('updated_at', array($from, $to))->with(array(
                        'section' => function($query) use($from, $to) {
                    $query->whereBetween('updated_at', array($from, $to));
                },
                        'section.note' => function($query) use($from, $to) {
                    $query->whereBetween('updated_at', array($from, $to))->orderBy('notes_order');
                },
//                    'section.note.type',
//                    'section.attachment',
                        'user',
                        'user.site_setting',
                        'user.customize_email',
                        'company'
                    ))->get();
//Log::error($projects->toArray());
            if ($projects) {
                $projectIDs = array();
                foreach ($projects as $project) {
                    $projectIDs[$project->id] = $project;
                }
                unset($projects);
                foreach ($subscribes as $row) {
                    if (isset($projectIDs[$row->project_id])) {
                        $project = $projectIDs[$row->project_id];
                        $project->subject = !empty($project->user->customize_email->subject) ? $project->user->customize_email->subject : "Hi, our project has been updated!";
                        $data = array(
                            'companyName' => $project->company->name,
                            'projectName' => $project->title,
                            'sections' => $project->section,
                            'customdomainURL' => !empty($project->user->site_setting->custom_domain) ? $project->user->site_setting->custom_domain : str_replace(array('http://', 'https://'), '', Config::get('app.domain')),
                            'token' => $row->token,
                        );
                        Mail::send('emails.subscribe', $data, function($message) use ($row, $project) {
                            $message->from('notifications@versionhistory.io' , $project->company->name);
                            $message->to($row->email);
                            $message->subject($project->subject);
                        });
Log::error('Send updated ' . $row->project_id);
                    } else {
//Log::error('Not updated ' . $row->project_id);
                    }
                    $c_update = SubscribeUpdate::find($row->id);
                    if ($c_update) {
                        $c_update->sended_at = Carbon::now();
                        if (isset($row->project->user->customize_email->send)) {
                            $send = Carbon::createFromFormat('H:i:s', $row->project->user->customize_email->send);
                            $c_update->sended_at->setTime($send->hour, $send->minute);
                        }
//Log::error('Save sended ' . $c_update->sended_at);
//Log::error($row->project->user->customize_email->send);
                        $c_update->save();
                    }
                }
            }
            }
        } else {
            Log::error('Not ready < :' . $from);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }

}
