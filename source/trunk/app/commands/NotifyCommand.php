<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class NotifyCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:notify';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'it will send email to users if they not subscribed.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
                $now = date('Y-m-d',  strtotime('+7 day'));
                // only for adminCompany
                $cron_subscribe = User::whereRaw("DATE(trial_ends_at)=? AND ISNULL(stripe_subscription) AND id=parent_id", array($now))
                        ->with(array('company'))->get();
                if($cron_subscribe){
                    foreach ($cron_subscribe as $row) {
                        $data = array('title'=>'Hi,', 'company'=>$row->company->name);
                        Mail::send('emails.notify', $data, function($message)  use ($row){
                            $message->from( 'notifications@versionhistory.io' );
                            $message->to( $row->email );
                            $message->subject($row->company->name . "'s VersionHistory.io trial expires in 1 week"); 
                        });
                    }
                } else {
                    Log::error('Not find users with 7 trial days');
                }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
            return array();
//		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
//		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
            return array();
//		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
//		);
	}

}
