<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AddCompany extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:add_company';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Define Subscribe Update (once after migration)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
            $subscribes = SubscribeUpdate::with(array( 'project' ))->get();
            if ($subscribes) {
                foreach ($subscribes as $subscribe) {
                    $updated = false;
                    $newSubscribe = SubscribeUpdate::find($subscribe->id);
                    if( isset($subscribe->project->company_id) && empty($newSubscribe->company_id) ) {
                        $newSubscribe->company_id = $subscribe->project->company_id ;
                        $updated = true;
                    }
                    if( empty($newSubscribe->token) ) {
                        $newSubscribe->token = Illuminate\Support\Str::random(60) ;
                        $updated = true;
                    }
                    if($updated) {
                        $newSubscribe->save();
                    }
                }
            }
            
        }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			
		);
	}

}
