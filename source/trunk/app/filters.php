<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) 
        {
            return Redirect::action('ProjectController@projectList',array('slug'=>Session::get('slug'))); 
            // return Redirect::to('/');
        }
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
//		throw new Illuminate\Session\TokenMismatchException;
            if (Request::ajax())
            {
                    return Response::make('Unauthorized', 401);
            }
            else
            {
                $errors='Please refresh the page to continue.';
                return Redirect::back()->withInput()->withErrors($errors);
            }
	}
});


/*
 * To verify that a user is subscribed
 *  
 */

Route::filter('subscribed', function()
{
    if (Auth::user() ) {
        $parent = User::find(Auth::user()->parent_id);
        if ( ! $parent->subscribed() && ! $parent->isFree() )
        {
            $user = User::with('company')->find(Auth::id());
            $slug = $user->company->slug;
            View::share('slug',$slug);
            return Redirect::to("/admin/{$slug}/account#billing")->with('status', "Your free trial has ended. Please update your payment information below to re-activate your account. Thanks!");
        }
    }
});

/*
 * To verify that a account is free 
 *  
 */
Route::filter('notfree', function()
{
    if (Auth::user() ) {
        $parent = User::find(Auth::user()->parent_id);
        if ( $parent->isFree() )
        {
            $user = User::with('company')->find(Auth::id());
            $slug = $user->company->slug;
            View::share('slug',$slug);
            return Redirect::to("/admin/{$slug}/account#billing");
        }
    }
});

/*
 * To verify that a user is Admin current Company
 *  
 */

Route::filter('isAdminCompany', function()
{
    if (!Auth::user()->isAdminCompany() )
    {
        $errors='Not permitted.';
//        $user = User::with('company')->find(Auth::id());
//        $slug = $user->company->slug;
//        View::share('slug',$slug);
        $slug = Session::get('slug', '');
        if (Request::ajax())
        {
            return Response::make($errors, 403);
        }
        else
        {
            return Redirect::to("/admin/{$slug}/account#user_account")->withErrors($errors);
        }
    }
});

Route::filter('authSuperAdmin', function()
{
    if (!UserAdmin::isAdmin())
    {
        if (Request::ajax())
        {
                return Response::make('Unauthorized', 401);
        }
        else
        {
                return Redirect::to("/vhadmin/login");
        }
    }
});

Route::filter('secure', function () {
    if (! Request::secure()) {
        $secure = Config::get('app.secure', false );
        if(!empty($secure)) {
            return Redirect::secure(
                Request::path(),
                in_array(Request::getMethod(), ['POST', 'PUT', 'DELETE']) ? 307 : 302
            );
        }
    }
});

Route::filter('unsecure', function () {
    if ( Request::secure()) {
        return Redirect::to(
            Request::path(),
            in_array(Request::getMethod(), ['POST', 'PUT', 'DELETE']) ? 307 : 302,
            array(),
            false
        );
    }
});