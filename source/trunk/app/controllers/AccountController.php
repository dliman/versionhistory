<?php

class AccountController	extends BaseController {

	public function __construct() {
		$this->layout = 'layouts.default';
	}
        

	public function showIndex($slug) {
            $company = Company::find(Auth::user()->company_id);
            if($company->slug !== $slug) {
                return Redirect::route('admin-x-account', array($company->slug));
            }
            $this->layout->title = 'VersionHistory.io - Account';
            View::share('slug',$slug);
            $pricePlans = Config::get('app.plan', array());
            $plans = array();
            foreach ($pricePlans as $key => $plan) {
                $plans[$key] = $plan['planTitle'];
            }
            $planUsers = isset($pricePlans[Auth::user()->pricing_plan]['users'])?$pricePlans[Auth::user()->pricing_plan]['users']: 10;
            $this->layout->content = View::make( 'account.account',
                    array(
                        'slug'=>$slug, 
                        'company'=>$company->name, 
                        'planUsers'=>$planUsers,
                        'plans' => $plans
                    ) );
            $this->getCurrentUserAllAccounts();
	}
        
        public function showAuth() {
            $plans = array();
            $pricePlans = Config::get('app.plan', array());
            foreach ($pricePlans as $key => $plan) {
                $plans[$key] = $plan['planTitle'];
            }
           
            $this->layout->title = 'VersionHistory.io - Login';
            $this->layout->content = View::make( 'account.login', array( 'plans' => $plans ) );
            
	}
        
        public function signIn() {
            $user_model = new User;
            
            $credentials = $user_model->setAuthCredentials();
            
            $remember = (Input::has('remember')) ? true : false;
            
            $validator = $user_model->validate( $credentials, 'auth');
      
            if( $validator->fails() ) { 
                
                return Redirect::route('admin-login')
                        ->withInput(Input::except('password') )
                        ->withErrors( $validator );
            } else {
                
                if( Auth::attempt( $credentials, $remember ) ) {
                    $user = User::with('company')->find(Auth::id()); 
                    Session::put('slug', $user->company->slug);
                    View::share('slug',$user->company->slug);
                    return Redirect::route('admin-x-projects', array($user->company->slug));
                         
                } else {
                                     
                    return Redirect::route('admin-login')
                        ->withInput(Input::except('password') )
                        ->withErrors( array( 'Login failed' ) );
                }

            }
            
        }
        
        
        public function signUp() {
            
            $company_model = new Company;
            $user_model = new User;
            
            $company_info = $company_model->setCredentials();               

            $company_valid = $company_model->validate($company_info, 'create');

            if($company_valid->fails()){
                return Redirect::route('admin-login')
                        ->withInput(Input::except( ['password_signup', 'password_confirmation'] ) )
                        ->withErrors( $company_valid );
            } else {
                $company = new Company($company_info);
                $company->save();

                $credentials = $user_model->setCreateCredentials($company->id);  
                $validator = $user_model->validate( $credentials, 'create' );
                if( $validator->fails() ) {                
                return Redirect::route('admin-login')
                        ->withInput(Input::except( ['password_signup', 'password_confirmation'] ) )
                        ->withErrors( $validator );
                } else {

                    $user = new User( $credentials );                
                    $user->password = Hash::make( $user->password ); 
                    $user->trial_ends_at = Carbon::now()->addDays(Config::get('app.trial', 30));
                    if($user->save()) {                        

                        $user->parent_id = $user->id;                        
                        $user->save(); 

                        Auth::login( $user );

                        Event::fire('auth.signup', array($user, $company, $credentials['password']));

                        Session::put('slug', $company->slug);

                        return Redirect::route('admin-x-projects',array($company->slug));
                    } else {
                        Log::error(array('Valid new user NOT saved :', $credentials));
                        return Redirect::route('admin-login')
                        ->withInput(Input::except( ['password_signup', 'password_confirmation'] ) )
                        ->with('errors','Not saved user');
                    }
                }
            }
            
        } 
        
        public function logout() {
            Auth::logout();
            Session::flush();
            return Redirect::route('admin-login');
        }
        
        public function editPersonalInformation(){
            
            $user = User::with('company')->find(Auth::id());
            $slug = $slugOld = $user->company->slug;
            
            $user_info = new User;            
            $company_info = new Company;
            $company_info->company_id = $user->company->id;
            
            $username = Input::get( 'p_username' );
            if(!empty($username) && ($username != $user->company->name)){
                $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $username));
                
                $company_credentials = $company_info->setEditCredentials();
                
                $validate_c = $company_info->validate($company_credentials, 'edit');
                
                if($validate_c->fails()){
                    View::share('slug',$slugOld);
                    return Redirect::to(URL::route('admin-x-account',array($slugOld) ). "#personal")
                            ->withInput(Input::except( ['p_password', 'p_password_confirmation'] ) )
                            ->withErrors( $validate_c );
                } 
            }
            
            $credentials = $user_info->setEditCredentials();    
            $validator_1 = $user_info->validate($credentials,'edit');
            
            if( $validator_1->fails()  ) { 
                View::share('slug',$slugOld);
                return Redirect::to(URL::route('admin-x-account',array($slugOld) )."#personal")
                        ->withInput(Input::except( ['p_password', 'p_password_confirmation'] ) )
                        ->withErrors( $validator_1 );
            } else {
              
               $user->email         = Input::get( 'p_email' );
               $user->first_name    = Input::get( 'p_fname' );
               $user->last_name     = Input::get( 'p_lname' );
               $user->phone         = Input::get( 'p_phone' );
               $user->password      = Hash::make(Input::get( 'p_password' ));
               

               if($user->save() && $slugOld !== $slug) {
                    $company = Company::find($user->company_id);
                    $company->name = Input::get( 'p_username' );
                    $company->slug = $slug;
                    $company->save();
                    Session::put('slug', $slug);
               }
               
               View::share('slug',$slug);
               return Redirect::to(URL::route('admin-x-account',array($slug) )."#personal")
                       ->with('status','Personal Information was saved.');
            }
        }
        
        public function editCommunicationPreferences() {
            $slug=Input::get('slug');
            $user = User::find(Auth::id());
            $user->resive = Input::get( 'resive' ) ? 'yes' : 'no';
            $user->save();
            View::share('slug',$slug);
            return Redirect::to(URL::route('admin-x-account',array($slug) )."#communication")
                    ->with('status','Communication Preferences were saved.');
        }
        
        public function editUserAccounts(){
            $current_user = User::with('company')->find(Auth::id());
            $pricePlans = Config::get('app.plan', array());
            $user_info = new User;
            $planUsers = isset($pricePlans[Auth::user()->pricing_plan]['users'])?$pricePlans[Auth::user()->pricing_plan]['users']: 10;

            if(!Auth::user()->isAdminCompany() || User::countCurrentUserAllAccounts()>=$planUsers ) {
                return Redirect::to(URL::route('admin-x-account', array($current_user->company->slug))."#user_account")
                        ->withErrors( 'You have reached the limit of the pricing plan' );
            }
            
            $credentials = $user_info->setEditAccountCredentials($current_user->company_id);
            
            $validator = $user_info->validate($credentials, 'edit-account');
      
            if( $validator->fails() ) { 
                View::share('slug',$current_user->company->slug);
                return Redirect::to(URL::route('admin-x-account', array($current_user->company->slug))."#user_account")
                        ->withInput(Input::except( ['iuaPassword'] ) )
                        ->withErrors( $validator );
            } else {
                
                $user = new User( $credentials );                
                $user->password = Hash::make( $user->password );
                $user->trial_ends_at = Carbon::now()->addDays(Config::get('app.trial', 30));
                $user->save(); 
                
                View::share('slug',$current_user->company->slug);
                return Redirect::to(URL::route('admin-x-account', array($current_user->company->slug))."#user_account")
                        ->with('status','User Account was saved.');
            }
            
        }
        
        private function getCurrentUserAllAccounts() {
            $accounts = User::getCurrentUserAllAccounts();
            View::share('current_user_accounts', $accounts );
            View::share('count_user_accounts', count($accounts) );
        }
        
        public function editAccount() {
            echo User::find(Input::get('uid'))->toJson();
            exit();
        }
        
        public function saveEditAccount() {
            $user = User::find(Input::get('m_uid'));
            $user_info = new User;
            $credentials = $user_info->setEditAccountCredentialsModal();
            $validator = $user_info->validate($credentials, 'edit-account-modal'); 
            $validator_0 = $user_info->validateEmailModal($user->email);
            if( $validator_0->fails()  ) {   
                View::share('slug',Input::get('slug'));
                return Redirect::to(URL::route('admin-x-account', array(Input::get('slug')))."#user_account")->withErrors( $validator_0 );
                
            }
            if($validator->fails()){
                View::share('slug',Input::get('slug'));
                return Redirect::to(URL::route('admin-x-account', array(Input::get('slug')))."#user_account")->withErrors($validator);
            } else {
               
                $user->email = Input::get( 'm_email' );
                $user->password = Hash::make(Input::get( 'm_password' ));
                $user->first_name = Input::get( 'm_fname' );
                $user->last_name = Input::get( 'm_lname' );
                $user->phone = Input::get( 'm_phone' );
                $user->save();
                return Redirect::to(URL::route('admin-x-account', array(Input::get('slug')))."#user_account")->with('status','User Account was updated.');
            }
        }
        
        public function deleteAccount(){
            // check access - see routing
            if(User::whereId(Input::get('account_uid-hidden'))->delete()) {
                Event::fire('user.deletedByAdminCompany', array(Input::get('account_uid-hidden')));
            }
            return Redirect::to(URL::route('admin-x-account', array(Input::get('slug')))."#user_account")->with('status','Account was removed.');
            
        }
        
        public function editBillingInformation(){
            Stripe::setApiKey(Config::get('services.stripe.secret'));
            $pricePlans = Config::get('app.plan', array());
            $token = Input::get('stripeToken');
            $plan = isset($pricePlans[Auth::user()->pricing_plan]['planId'])?$pricePlans[Auth::user()->pricing_plan]['planId']: 'standard20';
            $fname = Input::get('b_fname');
            $lname = Input::get('b_lname');
            
            $customer = null;
            if(!empty(Auth::user()->stripe_id)) {
                $customer = Auth::user()->subscription($plan)->getStripeCustomer(Auth::user()->stripe_id);
                $card4 = $customer->cards->retrieve($customer->default_card)->last4;
            } else {
                $card4 = '';
            }
            $card4new = Stripe_Token::retrieve($token)->card->last4;
            if($card4 == $card4new) {
                $token = null;
            }

            $trial = Auth::user()->trial_ends_at;
            if (Carbon::now()->gte($trial)) {
                $trial = Carbon::now();
            }

//Log::error($trial);

            $status = 'Payment was done.';
            if( !Auth::user()->subscribed() 
                    || (Auth::user()->onGracePeriod())
                    || (!Auth::user()->stripe_subscription && Auth::user()->onTrial())
                    ) {
                $status = 'Your billing information has been successfully saved.';
                $subscription = Auth::user()->subscription($plan);
                if (Carbon::now()->lt($trial)) {
                    $subscription->trialFor($trial)->maintainTrial();
                } else {
                    $subscription->skipTrial();
                }
                $subscription->create($token, array("email" => Auth::user()->email, 'description' => "{$fname} {$lname}"), $customer );
            } else {
                if($token) {
                    Auth::user()->subscription()->updateCard($token);
                }
                    $status = 'Your Credit Card information was changed.';
            }
            return Redirect::to(URL::route('admin-x-account', array(Input::get('slug')))."#billing")->with('status',$status);
        }

        public function editPricingPlan(){
            Stripe::setApiKey(Config::get('services.stripe.secret'));
            $pricePlans = Config::get('app.plan', array());
            $status = 'Your Pricing Plan was changed.';

            if(isset($pricePlans[Input::get('pricing_plan')]['planId'])) {
                $plan = $pricePlans[Input::get('pricing_plan')]['planId'];
                $planSwap = (Auth::user()->stripe_plan != $plan);
//                if($planSwap || Auth::user()->isFree()) {
                    $user = User::find(Auth::id());
                    $user->pricing_plan = Input::get('pricing_plan');
                    $user->save();
                    $status = 'Your Pricing Plan was changed.';
                    
                    Event::fire('user.editedPricingPlan', array($user->company_id));
//                }
            } else {
                return Redirect::to(URL::route('admin-x-account', array(Input::get('slug')))."#billing")->with('errors','Not defined Pricing Plan');
            }

            $trial = Auth::user()->trial_ends_at;
//Log::error($trial);
            if( Auth::user()->isFree()
                    || !Auth::user()->subscribed() 
                    || (Auth::user()->onGracePeriod())
                    || (!Auth::user()->stripe_subscription && Auth::user()->onTrial())
                    ) {
            } else {
                if($planSwap) {
                    Auth::user()->subscription($plan)
                            ->noProrate()
                            ->trialFor($trial)->maintainTrial()
                            ->swap();
                }
                $status = 'Your Pricing Plan was changed.';
            }
            return Redirect::to(URL::route('admin-x-account', array(Input::get('slug')))."#billing")->with('status',$status);
        }

        public function cancelAccount(){
            $comment = Input::get('comment');
            $userId = Auth::id();
            if(Auth::user()->isAdminCompany()) {
                if(Auth::user()->stripe_subscription) {
                    Auth::user()->subscription()->cancel();
    //                Auth::user()->subscription()->cancelNow();
                }
                $users = User::where('parent_id', '=', $userId)->where('id', '<>', $userId)->get();
                User::where('parent_id', '=', $userId)->delete();
                // @todo Projects need inactive ???
                Event::fire('user.deletedAccountAdminCompany', array($userId, $comment, $users));
            } else {
                User::where('id', '=', $userId)->delete();
                Event::fire('user.deletedAccount', array($userId, $comment));
            }
            Auth::logout();
            Session::flush();
//                return Redirect::back()->with('status','Payments were canceled.');
            return Redirect::route('admin-login')->with('status','Your account has been cancelled.');
        }
        
        
}
