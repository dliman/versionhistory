<?php

class ClientController extends BaseController {

    protected $layout = 'layouts.visitors';

    public function projectListCust() {
            $slug = Core::getSlug();
            return $this->projectList($slug);
    }

    public function projectList($slug) {

        $company = Company::whereSlug($slug)->first();
        if($company){
            $projectsObj = Project::whereCompany_id($company->id)->orderBy('projects_order')->orderBy('updated_at', 'desc')->latest();
            if(Auth::user() && Auth::user()->company_id == $company->id) {
            } else {
                $projectsObj->whereStatus('public') ;
            }
            $projects = $projectsObj->whereNull('inactived_at')->get();
            
            if(!isset($projects[1]) && isset($projects[0])) {
                return Redirect::route('project', Core::isCustomDomain()? array( $projects[0]->slug):array($slug,  $projects[0]->slug));
            }
            Session::put('project', '');

            $settings = $this->getSettings($slug, $projects[0]->user_id);
            
            View::share('settings', $settings );
            $this->layout->title = 'VersionHistory.io - Projects';
            $this->layout->content = View::make('project.visitor.project_list', array(
                    'projects' => $projects,
                    'slug' => $slug,
                    
                ));
        } else {
            App::abort(404, 'Page not found.');
        }
        
    }

    /**
     * Wrapper for custom domain
     * 
     * @param type $project_slug
     * @return type
     */
    public function projectCust($project_slug) {
            $slug = Core::getSlug();
            return $this->project($slug, $project_slug);
    }

    public function projectCustArchived($project_slug) {
            $slug = Core::getSlug();
            return $this->project($slug, $project_slug, true);
    }
    
    public function projectArchived($slug, $project_slug) {
            return $this->project($slug, $project_slug, true);
    }
    
    public function project($slug, $project_slug, $archived = false) {
        $project_model = new Project;
        $project_id = $project_model->getProjectClient($project_slug, $slug);
        if(isset($project_id[0])) {
            Session::put('project', $project_id[0]->pid);
            if($project_id[0]->status != 'public') {
                if(Auth::user()) { 
                    if(Auth::user()->company_id != $project_id[0]->company_id) {
                        App::abort(404, 'Access restricted.');
                    }
                } else {
                    App::abort(404, 'Access restricted.');
                }
            }

//            $archived = Input::get('archived', false);
            $project = Project::whereId($project_id[0]->pid)->with(array(
                    'section'       => function($query) use ($archived) { 
                        if($archived) {
                            $query->whereArchived('yes');
                        } else {
                            $query->whereArchived('no');
                        }
                        $query->orderBy('versions_order')->orderBy('created_at', 'desc'); 
                            
                    },
                    'section.note'  => function($query) { $query->orderBy('notes_order')->orderBy('created_at', 'desc'); },
                    'section.note.type',
                    'section.attachment'
                ))->first();
            if ($project) {
                $settings = $this->getSettings($slug, $project_id[0]->uid, $project_slug);
                $countArchived = Section::whereProjectId($project->id)->whereArchived('yes')->count();

                View::share('settings', $settings);
                
                if(Auth::user() && Auth::user()->company_id == $project->company_id) {
                    $projectsCount = Project::whereCompany_id($project->company_id)->count();
                } else {
                    $projectsCount = Project::whereCompany_id($project->company_id)->whereStatus('public')->count();
                }

                $this->layout->title = isset($project_setting->page_title) ? $project_setting->page_title : 'VersionHistory.io - Notes';
                if(Input::get('pdf',0)) {
                    $nameView = 'project.visitor.notes_pdf';
                } else {
                    $nameView = 'project.visitor.notes';
                }                
                $this->layout->content = View::make($nameView, array(
                            'project' => $project,
                            'slug' => $slug,
                            'project_slug' => $project_slug,
                            'projectsCount' => $projectsCount,
                            'archived'=>$archived,
                            'countArchived'=>$countArchived
                        ));
            } else {
                App::abort(404, 'Page not found.');
            }
        } else {
            App::abort(404, 'Page not found.');
        }
    }

    public function getNoteDetailsCust($project_slug, $section_slug, $note_slug) {
            $slug = Core::getSlug();
            return $this->getNoteDetails($slug, $project_slug, $section_slug, $note_slug);
    }

    public function getNoteDetails($slug, $project_slug, $section_slug, $note_slug ) {
        if(!$slug) {
            $slug = Core::getSlug();
        }
        $project_model = new Project;
        $project_id = $project_model->getProjectClientNoteDetails($project_slug, $slug, $section_slug, $note_slug);
        if(isset($project_id[0])) {
            if($project_id[0]->status != 'public') {
                if(Auth::user()) { 
                    if(Auth::user()->company_id != $project_id[0]->company_id) {
                        App::abort(404, 'Access restricted.');
                    }
                } else {
                    App::abort(404, 'Access restricted.');
                }
            }
            $note = Note::whereId($project_id[0]->nid)->with(array('section', 'section.project', 'type', 'attachment'))->first();
            if ($note) {
                $settings = $this->getSettings($slug, $project_id[0]->uid, $project_slug);
                View::share('settings', $settings);
                Session::put('project', $note->section->project->id);
                $this->layout->title = 'VersionHistory.io - Note Details';
                if(Input::get('pdf',0)) {
                    $nameView = 'project.visitor.note_details_pdf';
                } else {
                    $nameView = 'project.visitor.note_details';
                }
                $this->layout->content = View::make($nameView, array(
                            'note' => $note,
                            'slug' => $slug,
                            'project_slug' => $project_slug,
                            'section_slug' => $section_slug
                ));
            } else {
                App::abort(404, 'Page not found.');
            }
        } else {
            App::abort(404, 'Page not found.');
        }
    }

    public function getDownloadPdf() {
        $url = $_SERVER['HTTP_ORIGIN'] . Input::get('path'). '?pdf=1';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $content = curl_exec($ch);
        curl_close($ch);
        if (!$err) {
            return PDF::load($content, 'A4', 'portrait')->download($_SERVER['SERVER_NAME'] . Input::get('path') /*. '.pdf'*/);
        } else {
            return Redirect::back()->withErrors($errmsg);
        }
    }

    public function getSearch() {
        App::abort(404, 'Page not found.');
    }

    public function search() {

        $filter = Input::get('project');
        Session::put('project', $filter);
        $user_id = Input::get('user_id');
        $keywords = Input::get('search');
        $select_sort = Input::get('select-sort');

        $settings = $this->getSettings(Input::get('slug'), $user_id, Input::get('project_slug'));
        View::share('settings', $settings);

        $results = '';
        if (trim($keywords)) {
            $keywords = strpos($keywords, ',') ?
                    trim(str_replace(',', '', $keywords)) : $keywords;
            $keywords = strpos($keywords, '-') ?
                    trim(str_replace("-", "", $keywords)) : $keywords;

            $arr = explode(" ", $keywords);
            $query_arr = array();
            for ($i = 0; $i < count($arr); $i++) {
                $len = strlen(trim($arr[$i]));
                if ($len == 0) {
                    continue;
                }
                $query_arr[] = trim($arr[$i]);
            }
            $keywords = implode(' ', $query_arr);

            $results = SphinxSearch::search($keywords)
                    ->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_EXTENDED)
                    ->filter('project_id', array($filter))
                    ->query();

            if(!$results) {
                Log::error(SphinxSearch::getErrorMessage());
            }
            $arr = array();

            $res = '';

            if (isset($results['matches']) && (count($results['matches']) > 0) ) {
                foreach ($results['matches'] as $key => $value) {
                    $arr[] = $key;
                }

                $in = implode(',', $arr);
                $section_model = new Section;
                $res = $section_model->getSearchResult($select_sort, $in);
                        

            }
        }
        unset($results);
        $this->layout->title = 'VersionHistory.io - Search';
        $this->layout->content = View::make('project.visitor.search', array(
                    'results' => $res,
                    'keywords' => $keywords,
                    'select_sort' => $select_sort,
                    'project_id' => $filter
        ));
    }

    public function subscribeUpdates() {
        $subscribe_model = new SubscribeUpdate;
        $credentials = $subscribe_model->setClientCredentials();
        
        $validator = $subscribe_model->validate($credentials, 'create');
        
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            $credentials['company_id'] = Project::find($credentials['project_id'])->company_id;
            $add_subscribe = new SubscribeUpdate($credentials);
            $add_subscribe->save();
            return Redirect::back()->with('status','You have successfully subscribed for updates.');
        }
    }


    /**
     * Display the Unsubscribe Updates view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getUnsubscribeUpdates($token = null)
    {
        if (is_null($token)) {
            App::abort(404) ; 
        }
        $subscribe = SubscribeUpdate::whereToken($token)->with(array(
            'project',
            'project.company'
        ))->first();
        if ( empty($subscribe)) {
            App::abort(404) ; 
        }
        if( !empty($subscribe->project->slug) && !empty($subscribe->project->company->slug)) {
            $settings = $this->getSettings($subscribe->project->company->slug, $subscribe->project->user_id, $subscribe->project->slug);
            View::share('settings', $settings);
        }

        $this->layout->title = 'VersionHistory.io - Unsubscribe Updates';
        $this->layout->content =  View::make('project.visitor.unsubscribe')->with(array(
            'token'=> $token,
            'project_name'=> !empty($subscribe->project->title)?$subscribe->project->title:'project',
            'project'=> $subscribe->project,

        ));
    }

    /**
     * Handle a POST request to Unsubscribe Updates.
     *
     * @return Response
     */
    public function postUnsubscribeUpdates()
    {
        $token = Input::get('token', null);
        if (is_null($token)) { App::abort(404) ; }
        
        $subscribe = SubscribeUpdate::whereToken($token)->first();
        if ( $subscribe ) {
            $subscribe->delete();
            return Redirect::to('/');
        } else {
            App::abort(404) ;
        }
    }

    public function requestFeature() {
        $customer_request_model = new CustomerRequest;
        $credentials = $customer_request_model->setCreateCredentials();
        $validator = $customer_request_model->validate($credentials, 'create');
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            $add = new CustomerRequest($credentials);
            $add->save();
            return Redirect::back()->with('status','We have received your feature request. Thank you!');
        }
       
    }

    public function showIndex() {
            $this->layout = View::make('layouts.defaultHTML');
            $this->layout->content = View::make( 'html.index' );
    }

    public function showContact() {
        $this->layout = View::make('layouts.defaultHTML');
        $this->layout->title = 'Contact';
        $this->layout->content = View::make( 'html.contact' );
    }
    
    public function postFormContact() {
        $custom_email_model = new CustomizeEmail;
        $credentials = $custom_email_model->setCredentialsClient();
        
        $validator = $custom_email_model->validate($credentials, 'client');
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            Mail::send('emails.contact', $credentials, function($message)  use ($credentials){
                $message->from( $credentials['email'] );
                $message->to( $credentials['adminEmail'] );
                $message->subject('VersionHistory form: ' . $credentials['subject']); 
            });

            return Redirect::back()->with('status','Your message was sent. Thank you!');
        }
       
    }

    public function showFaq() {
        $this->layout = View::make('layouts.defaultHTML');
        $this->layout->title = 'FAQ';
        $this->layout->content = View::make( 'html.faq' );
    }
        
    public function showFeatures() {
        $this->layout = View::make('layouts.defaultHTML');
        $this->layout->title = 'Features';
        $this->layout->content = View::make( 'html.features' );
    }
        
    public function showPricing() {
        $this->layout = View::make('layouts.defaultHTML');
        $this->layout->title = 'Pricing';
        $this->layout->content = View::make( 'html.pricing' );
    }
        
    public function showPrivacy() {
        $this->layout = View::make('layouts.defaultHTML');
        $this->layout->title = 'Privacy Policy';
        $this->layout->content = View::make( 'html.privacy' );
    }
        
    public function showTerms() {
        $this->layout = View::make('layouts.defaultHTML');
        $this->layout->title = 'Terms of Service';
        $this->layout->content = View::make( 'html.terms' );
    }
    
    public function redirectHtmls($page) {
        return Redirect::to("/{$page}", 301);
    }
        
}
