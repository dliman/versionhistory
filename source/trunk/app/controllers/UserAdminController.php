<?php

class UserAdminController extends \BaseController {

    public function __construct() {
        $this->layout = 'layouts.default';
    }

    /**
     * Display the Home page view for SuperAdmin
     */
    public function showIndex() {
        $users = User::withTrashed()->with(array(
                    'company',
                    'parent' => function($query) {
                        $query->withTrashed();
                    }
                ))->get();
        $pricePlans = Config::get('app.plan', array());
        $this->layout->content = View::make('useradmin.index', array('users' => $users, 'pricePlans' => $pricePlans));
    }

    /**
     * Handle a POST request to login a user.
     * 
     * @return Redirect
     */
    public function postLogin() {
        $credentials = UserAdmin::setCredentials();
        $validator = UserAdmin::validate($credentials);

        if ($validator->fails()) {

            return Redirect::route('vhadmin-login')
                            ->withInput(Input::except('password'))
                            ->withErrors($validator);
        } else {

            if (UserAdmin::attempt($credentials)) {
                return Redirect::route('vhadmin');

            } else {

                return Redirect::route('vhadmin-login')
                                ->withInput(Input::except('password'))
                                ->withErrors(array('Login failed'));
            }
        }
    }

    /**
     * Display the login page view for SuperAdmin
     */
    public function login() {
        $this->layout->content = View::make('useradmin.login');
    }

    /**
     * Handle a logout.
     * 
     * @return Redirect
     */
    public function logout() {
        UserAdmin::logout();

        return Redirect::route('vhadmin');
    }

    /**
     * Handle a AJAX request to make user inactive.
     * 
     * @return Response
     */
    public function makeInactive() {
        if (Request::ajax()) {
            $user = User::find(Input::get('user_id'));
            if (!empty($user)) {
                $user->delete();
                return Response::make('"ok"', 200)->header('Content-type', 'application/json');
            } else {
                return Response::make("Page not found.", 404);
            }
        } else {
            App::abort(404, 'Page not found.');
        }
    }

    /**
     * Handle a AJAX request to make user active.
     * 
     * @return Response
     */
    public function makeActive() {
        if (Request::ajax()) {
            $user = User::withTrashed()->find(Input::get('user_id'));
            if (!empty($user)) {
                $user->restore();
                return Response::make('"ok"', 200)->header('Content-type', 'application/json');
            } else {
                return Response::make("Page not found.", 404);
            }
        } else {
            App::abort(404, 'Page not found.');
        }
    }

    /**
     * 
     * 
     * @return type
     */
    public function userAccount() {
        $user = User::withTrashed()->with(array(
                    'company'
                ))->find(Input::get('user_id'));
        if (!empty($user)) {
            Auth::login($user);
            Session::put('slug', $user->company->slug);
            return Redirect::route('admin-x-projects', array($user->company->slug));
        } else {
            App::abort(404, 'Page not found.');
        }
    }

    /**
     * Handle a AJAX request to edit trial ends.
     * 
     * @return Response
     */
    public function editTrial() {
        $user_model = new User;
        if (Request::ajax()) {
            $credentials = $user_model->setTrialCredentials();

            $validator = $user_model->validate($credentials, 'trial');

            if ($validator->fails()) {
                return Response::make($validator->errors(), 404);
            } else {
                $user = User::withTrashed()->find(Input::get('user_id'));
                if (!empty($user)) {
                    if ($user->trial_ends_at != $credentials['trial_ends_at']) {
                        $user->trial_ends_at = $credentials['trial_ends_at'];
                        $user->save();

                        if ($user->free != 'yes') {
                            $pricePlans = Config::get('app.plan', array());
                            $plan = isset($pricePlans[$user->pricing_plan]['planId']) ? $pricePlans[$user->pricing_plan]['planId'] : 'standard20';
                            $trial = $user->trial_ends_at;
                            if (Carbon::now()->gte($trial)) {
                                $trial = Carbon::now();
                            }

                            if (!$user->subscribed() || (!$user->stripe_subscription && $user->onTrial())) {
                                // Do nothing
                            } else {
                                $subscription = $user->subscription($plan)->noProrate();
                                if (Carbon::now()->lt($trial)) {
                                    $subscription->trialFor($trial);
                                } else {
                                    $subscription->skipTrial();
                                }
                                $subscription->swap();
                            }
                        }
                    }
                    return Response::make('ok', 200);
                } else {
                    return Response::make("Page not found.", 404);
                }
            }
        } else {
            App::abort(404, 'Page not found.');
        }
    }

    /**
     * Handle a AJAX request to edit "free" field.
     * 
     * @return Response
     */
    public function editFree() {
        $user_model = new User;
        if (Request::ajax()) {
            $credentials = $user_model->setFreeCredentials();

            $validator = $user_model->validate($credentials, 'free');

            if ($validator->fails()) {
                return Response::make($validator->errors(), 404);
            } else {
                $user = User::withTrashed()->find(Input::get('user_id'));
                if (!empty($user)) {
                    $user->free = $credentials['free'];
                    $user->save();
                    
                    if ( $user->subscribed() && !(!$user->stripe_subscription && $user->onTrial())) {
                        if ($user->free == 'yes' /* && $user->trashed()*/) {
                            $user->subscription()->cancel();
                        }
                    }
                    return Response::make('ok', 200);
                } else {
                    return Response::make("Page not found.", 404);
                }
            }
        } else {
            App::abort(404, 'Page not found.');
        }
    }

}
