<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProjectController
 *
 * @author anarxist
 */
class ProjectController extends BaseController {
    
    protected $layout = 'layouts.default';
    
    public function showIndex() {
        $this->layout->title = 'VersionHistory.io - Projects';
        $this->layout->content = View::make( 'project.admin.project_list' );
    }
    
    public function projectSetting($slug) {
        $company = Company::find(Auth::user()->company_id);
        if($company->slug !== $slug) {
            return Redirect::action('ProjectController@projectSetting', array($company->slug));
        }
        
        $settings = array();
        $settings['user'] = User::with(array(
            'company',
            'localization',
            'site_setting',
            'layout_setting',
            'icon_manager',
            'feature_setting',
            'customize_email'))->find(Auth::id());

//        $settings['icon_manager_default'] = NoteType::whereUser_id(0)->get();
        
        $settings['subscribes'] = SubscribeUpdate::whereCompany_id(Auth::user()->company_id)->with(array('project'))->get();

        $settings['projects'] =  Project::whereCompany_id(Auth::user()->company_id)->orderBy('title', 'asc')->lists('title','id');
        $settings['projectsAll'] = array(0=>'All Projects');
        foreach ($settings['projects'] as $key => $value) {
            $settings['projectsAll'][$key] = $value;
        }
        $rojectSettingDefault = ProjectSetting::getDefaultSettings(Auth::user()->parent_id, $slug, null, true);
        $placeholder = array(
            'page_title'=> $rojectSettingDefault->page_title, 
            'meta_description'=> $rojectSettingDefault->meta_description, 
            'meta_keywords'=> $rojectSettingDefault->meta_keywords
        );
        $this->layout->title = 'VersionHistory.io - Settings';
        $this->layout->content = View::make( 'project.admin.setting', array('settings'=>$settings, 'placeholder'=>$placeholder) );
    }
    
    public function setLocalization() {
        $localisation = new LocalizationSetting;
        $credentials = $localisation->setCredentials();
        $validate = $localisation->setCredentials($credentials,'create');
        if($validate->fails()){
            return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug'))).'#localization')
                    ->withInput()->withErrors($validate);
        } else {
            $first_note = LocalizationSetting::whereUser_id(Auth::user()->parent_id)->first();
            if($first_note){
                $first_note->date_format = Input::get('date_format');
                $first_note->time_zone = Input::get('time_zone');
                $first_note->save();
                
                return  Redirect::to(URL::route('admin-x-settings',array(Input::get('slug'))).'#localization')
                        ->with('status','Localization Settings were saved.');
            } else {
                $first_note = new LocalizationSetting($credentials);
                $first_note->save();
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug'))).'#localization')
                        ->with('status','Localization Settings were saved.');
            }
        }
    }
    
    public function siteSetting() {
        $project_setting = new ProjectSetting;
        
        $credentials = $project_setting->setCredentials();
            
        $first_note = ProjectSetting::whereUser_id(Auth::user()->parent_id)->first();
        
        $project_setting->user_id = $first_note->user_id;
        
        $validate = $project_setting->validate($credentials, 'create');
        
        if($validate->fails()){
            
            return  Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#site-setting")->withInput()->withErrors($validate);
        } else {
            $destinationPath = public_path().'/files/users/'.Auth::user()->parent_id.'/';
            
            $file_upload_logo = Input::file('upload_logo');
            $filename_upload_logo = '';
            if($file_upload_logo){
                if(!is_dir($destinationPath)){
                    File::makeDirectory($destinationPath, 0775, true);
                }
                $filename_upload_logo = $file_upload_logo->getClientOriginalName();
                $file_upload_logo->move($destinationPath,$filename_upload_logo);
            }
            
            $file_upload_favicon = Input::file('upload_favicon');
            $filename_upload_favicon = '';
            if($file_upload_favicon){
                if(!is_dir($destinationPath)){
                    File::makeDirectory($destinationPath, 0775, true);
                }
                $filename_upload_favicon = $file_upload_favicon->getClientOriginalName();
                $file_upload_favicon->move($destinationPath,$filename_upload_favicon);
            }
            
            if($first_note){
                $first_note->page_title = Input::get('page_title');
                $first_note->meta_description = Input::get('meta_description');
                $first_note->meta_keywords = Input::get('meta_keywords');
                $first_note->custom_domain = Input::get('custom_domain');
                $filename_upload_logo ? $first_note->upload_logo = $filename_upload_logo : '';
                $filename_upload_favicon ? $first_note->upload_favicon = $filename_upload_favicon : '';
                $first_note->save();
                
                
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#site-setting")
                        ->with('status','Site Settings were saved.');
            } else {
                $first_note = new ProjectSetting($credentials);
                $first_note->save();
                $update_setting = ProjectSetting::find($first_note->id);
                if($update_setting){
                    if($filename_upload_logo){
                        $update_setting->upload_logo = $filename_upload_logo;
                    }
                    if($filename_upload_favicon){
                        $update_setting->upload_favicon = $filename_upload_favicon;
                    }
                    $update_setting->save();
                }
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#site-setting")->with('status','Site Settings were saved.');
            }
        }
    }
    
    public function deleteUploadLogo() {
        $user = ProjectSetting::whereUser_id(Auth::user()->parent_id)->first();
        $user->upload_logo = '';
        $user->save();
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#site-setting")->with('status','Logo was deleted.');
               
    }
    
    public function deleteUploadFavicon() {
        $user = ProjectSetting::whereUser_id(Auth::user()->parent_id)->first();
        $user->upload_favicon = '';
        $user->save();
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#site-setting")->with('status','Favicon was deleted.');
    }
    
    public function deleteBackgroundImage() {
        $user = LayoutSetting::whereUser_id(Auth::user()->parent_id)->first();
        $user->background_image = '';
        $user->save();
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#layout-settings")->with('status','Background image was deleted.');
    }
    
    public function deleteHeaderImage() {
        $user = LayoutSetting::whereUser_id(Auth::user()->parent_id)->first();
        $user->header_image = '';
        $user->save();
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#layout-settings")->with('status','Header image was deleted.');
    }
    
    
    public function featureSetting() {
        $feature_setting = new FeatureSetting;
        $custom_type_setting =  new CustomType;
        $credentials = $feature_setting->setCredentials();
        
        $first_row = FeatureSetting::whereUser_id(Auth::user()->parent_id)->first();
        If(!$first_row){
            $feature_setting = new FeatureSetting($credentials);
            $feature_setting->save();
        } else {
          
            $first_row->show_search_box = Input::get('show_search_box') == 'yes' ? 'yes' : 'no' ;
            $first_row->show_feature_request_box = Input::get('show_feature_request_box') == 'yes' ? 'yes' : 'no';
            $first_row->show_download_pdf = Input::get('show_download_pdf') == 'yes' ? 'yes' : 'no';
            $first_row->show_signup_email = Input::get('show_signup_email') == 'yes' ? 'yes' : 'no';
            $first_row->save();
        }
        
       
        
        $destinationPath = public_path().'/files/users/'.Auth::user()->parent_id.'/';
        $file_icon_file = Input::file('icon_file');
        $filename_icon_file = '';
        if($file_icon_file){
            if(!is_dir($destinationPath)){
                File::makeDirectory($destinationPath, 0775, true);
            }
            $filename_icon_file = $file_icon_file->getClientOriginalName();
            $file_icon_file->move($destinationPath,$filename_icon_file);
        }
        if(((Input::get('icon')!='Select Icon...') && (Input::get('icon')!='')) || ($filename_icon_file != '') || (Input::get('icon_id')!=0) ){
            $custom_type = $custom_type_setting->setCredentials();
            $validator = $custom_type_setting->validate($custom_type, 'create');
            if($validator->fails()){
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#feature-settings")
                        ->withInput()->withErrors($validator);
            } else {
                if(Input::get('icon_id')!=0){
                    $edit_type = CustomType::find(Input::get('icon_id'));
                    if($edit_type){
                        $edit_type->title = Input::get('type_title');
                        $edit_type->show_label = Input::get( 'show_label' ) ? 'show' : 'hide';
                        $edit_type->icon = ((Input::get('icon')=='Select Icon...') || (Input::get('icon')=='')) ? '' : Input::get('icon');
                        $edit_type->color = Input::get('type_color');
                        $edit_type->image = $filename_icon_file != '' ?  $filename_icon_file : $edit_type->image;
                        $edit_type->save();
                        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#feature-settings")
                                ->withInput()->with('status','Feature Settings were saved.');
                    }
                } else {
                    $first_custom_type =  new CustomType($custom_type);
                    $first_custom_type->save();
                    return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#feature-settings")
                            ->withInput()->with('status','Feature Settings were saved.');
                }
            }
        }
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#feature-settings")
                ->withInput()->with('status','Feature Settings were saved.');
    }
    
    public function customizeEmail(){
        $localizationSetting = LocalizationSetting::whereUser_id(Auth::id())->first();
        $customize_email = new CustomizeEmail;
        if($localizationSetting){
            $timeZone = $localizationSetting->time_zone;
        } else {
            $timeZone = date_default_timezone_get();
        }
        $credentials = $customize_email->setCredentials();
        
        $validate = $customize_email->validate($credentials, 'create');
        if($validate->fails()){
            return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#email-settings")
                    ->withErrors($validate);
        } else {
            if($credentials['project_id'] > 0) {
                $subscribes = SubscribeUpdate::whereProject_id($credentials['project_id'])->get();
            } else {
                $subscribes = SubscribeUpdate::whereCompany_id(Auth::user()->company_id)->get();
            }
            if(!empty($subscribes)) {
                foreach ($subscribes as $row) {
                    $data = array('title'=>$credentials['subject'], 'content'=>$credentials['body']); 
                    Mail::send('emails.customize', $data, function($message)  use ($row,$credentials){
                        $message->to( $row->email );
                        $message->subject($credentials['subject']); 
                    });
                }
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#email-settings")->with('status','Messages were sended.');
            } else {
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#email-settings")->with('status','Subscribers not found.');
            }
        }
    }

    public function automaticEmail(){
        $localizationSetting = LocalizationSetting::whereUser_id(Auth::user()->parent_id)->first();
        if($localizationSetting){
            $timeZone = $localizationSetting->time_zone;
        } else {
            $timeZone = date_default_timezone_get();
        }
        $credentials = array(
            'subject'=> Input::get('subject'),
            'send_automatic'=>Input::get('send_automatic') == 'yes' ? 'yes' : 'no',
            'send'=>  Carbon::createFromFormat('H:i',Input::get('send'),$timeZone)->setTimezone(date_default_timezone_get()), // date('H:i:s',strtotime(Input::get('send'))),
            'user_id'=>Auth::id(),
            'project_id'=>''
        );
        
        $validate = Validator::make($credentials, array(

        ));
        if($validate->fails()){
            return Redirect::back()->withErrors($validate);
        } else {
            $new_row = CustomizeEmail::whereUser_id(Auth::user()->parent_id)->first();
            if($new_row){
                $new_row->subject = $credentials['subject'];
                $new_row->send_automatic = $credentials['send_automatic'];
                $new_row->send = $credentials['send'];
                $new_row->save();                
            } else {
                $new_row = new CustomizeEmail($credentials);
                $new_row->save();
            }
            $mySubscribers = SubscribeUpdate::whereCompany_id(Auth::user()->company_id)->update(array('sended_at' => '0000-00-00 00:00:00' ));
        }
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#email-settings")
                ->with('status','Settings were saved.');
    }

    public function getEditCustomType() {
        $id = Input::get('type_id');
        return Response::json(CustomType::find($id));
    }
    
    public function editCustomType() {
        $id = Input::get('cid');
        $custom_type = CustomType::find($id);
        $custom_type->title = Input::get('type_title');
        $custom_type->show_label = Input::get( 'show_label' ) ? 'show' : 'hide';
        $custom_type->icon = ((Input::get('icon')=='Select Icon...') || (Input::get('icon')=='')) ? '' : Input::get('icon');
        $custom_type->color = Input::get('type_color');
        $custom_type->image = $filename_icon_file ? $filename_icon_file : $first_row_custom_type->image;
        $custom_type->save();

        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#feature-settings")->withInput();
    }
    
    public function deleteCustomType() {
        CustomType::whereId(Input::get('type_id'))->delete();
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#feature-settings");
    }
    
    public function layoutSetting() {
        $filename_header_image = '';
        $filename_background_image = '';
        $destinationPath = public_path().'/files/users/'.Auth::user()->parent_id.'/';
        
        $file_header_image = Input::file('header_image');
        if($file_header_image){
            $validator = Validator::make(array('header_image'=>$file_header_image), array('header_image'=>'image'));
            if($validator->fails()){
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#layout-settings")->withErrors($validator);
            }else{
                if(!is_dir($destinationPath)){
                    File::makeDirectory($destinationPath, 0775, true);
                }
                
                $filename_header_image = $file_header_image->getClientOriginalName();
                $file_header_image->move($destinationPath,$filename_header_image);
                
            }
        }
        
        $file_background_image = Input::file('background_image');        
        if($file_background_image){
            $validator = Validator::make(array('background_image'=>$file_background_image), array('background_image'=>'image'));
            if($validator->fails()){
                return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#layout-settings")->withErrors($validator);
            } else {
                if(!is_dir($destinationPath)){
                    File::makeDirectory($destinationPath, 0775, true);
                }
                $filename_background_image = $file_background_image->getClientOriginalName();
                $file_background_image->move($destinationPath,$filename_background_image);
            }
            
        }
        
        $credentials = array(
            'header_image'      => $filename_header_image,
            'header_text_color' => Input::get('header_text_color'),
            'header_text_show'  => Input::get('header_text_show')=='hide' ? 'hide' : 'show',
            'header_background' => Input::get('header_background'),
            'hide_header_area'  => Input::get('hide_header_area')=='hide' ? 'hide' : 'show',
            'background_image'  => $filename_background_image,
            'background_color'  => Input::get('background_color'),
            'header_code'       => Input::get('header_code'),
            'footer_code'       => Input::get('footer_code'),
            'custom_css'        => Input::get('custom_css'),
            'project_id'        => Input::get('project_id') ? Input::get('project_id') : 0,
            'user_id'           => Auth::user()->parent_id
        );
        $lauout_first = LayoutSetting::whereUser_id(Auth::user()->parent_id)->first();
        if($lauout_first){
            $lauout_first->header_background = Input::get('header_background');
            $lauout_first->header_text_color = Input::get('header_text_color');
            $lauout_first->header_text_show = Input::get('header_text_show')=='hide' ? 'hide' : 'show';
            $lauout_first->hide_header_area = Input::get('hide_header_area')=='hide' ? 'hide' : 'show';
            $lauout_first->background_color = Input::get('background_color');
            $lauout_first->header_code = Input::get('header_code');
            $lauout_first->footer_code = Input::get('footer_code');
            $lauout_first->custom_css = Input::get('custom_css');
            if($filename_header_image){
                $lauout_first->header_image = $filename_header_image;
            }
            if($filename_background_image){
                $lauout_first->background_image = $filename_background_image;
            }
            $lauout_first->save();
            return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#layout-settings")
                    ->with('status','Layouts Settings were saved.');
        } else {
            $new_layout_setting = new LayoutSetting($credentials);
            $new_layout_setting->save();
            return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#layout-settings")
                    ->with('status','Layouts Settings were saved.');
        }
    }
    
    public function exportProjectDataCsv() {
        $projects = Project::whereCompany_id(Auth::user()->company_id)->with(array(
            'section'=>function($query){ $query->orderBy('id', 'ASC'); },
            'section.note'=>function($query){ $query->orderBy('id', 'ASC'); },
            'section.note.type'
            ))->get();

        $arr = array();
        foreach ($projects as $project) {
            if(count($project->section)>0) {
                foreach ($project->section as $section) {
                    if(count($section->note)>0) {
                        foreach ($section->note as $note) {
                            if($note->type) {
                                $arr[]=array(
                                    'Project Name'=>$project->title, 'Project Description'=>$project->description,
                                    'Version Date'=>$section->released_at, 'Version Name'=>$section->title, 'Version Description'=>$section->description,
                                    'Notes Type'=>$note->type->title,'Notes Name'=>$note->title,'Notes Description'=>$note->description,
                                );
                            } else {
                                $arr[]=array(
                                    'Project Name'=>$project->title, 'Project Description'=>$project->description,
                                    'Version Date'=>$section->released_at, 'Version Name'=>$section->title, 'Version Description'=>$section->description,
                                    'Notes Type'=>'','Notes Name'=>$note->title,'Notes Description'=>$note->description,
                                );
                            }
                        }                        
                    } else {
                        $arr[]=array(
                            'Project Name'=>$project->title, 'Project Description'=>$project->description,
                            'Version Date'=>$section->released_at, 'Version Name'=>$section->title, 'Version Description'=>$section->description,
                            'Notes Type'=>'','Notes Name'=>'','Notes Description'=>'',
                        );
                    }
                }
            } else {
                $arr[]=array(
                    'Project Name'=>$project->title, 'Project Description'=>$project->description,
                    'Version Date'=>'','Version Name'=>'','Version Description'=>'',
                    'Notes Type'=>'','Notes Name'=>'','Notes Description'=>'',
                );
            }
        }
        if(count($arr)>0){
            return CSV::fromArray($arr)->render();
        } else{
            return Redirect::to(URL::route('admin-x-account',array(Input::get('slug')))."#export_data")->with('status','Projects list is empty');
        }
    }
    
    public function exportDataCsv() {
        $subscribes = SubscribeUpdate::whereCompany_id(Auth::user()->company_id)->with(array('project'=>function($query){
            $query->orderBy('id', 'DESC');
        }))->get();
        $arr = array();
        foreach ($subscribes as $subscribe) {
            $arr[]=array(
                'id'=>$subscribe->id,
                'email'=>$subscribe->email,
                'project'=>$subscribe->project->title,
                'subscribe'=>date('d-m-Y', strtotime($subscribe->created_at))
            );
        }
        if(count($arr)>0){
            return CSV::fromArray($arr)->render();
        } else{
            return Redirect::to(URL::route('admin-x-account',array(Input::get('slug')))."#export_data")->with('status','Subscribers list is empty');
        }
        
        
        
    }

    public function subscribeUpdates() {
        $subscribe_update = new SubscribeUpdate;
        $credentials = $subscribe_update->setCredentials();
        
        $validator = $subscribe_update->validate($credentials, 'create');
        
        if ($validator->fails()) {
            return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#email-settings")
                    ->withErrors($validator);
        } else {

            $add_subscribe = SubscribeUpdate::find(Input::get('subscription_id'));
            if($add_subscribe) {
                $add_subscribe->email = $credentials['email'];
                $add_subscribe->project_id = $credentials['project_id'];
                $add_subscribe->user_id = $credentials['user_id'];
                $add_subscribe->company_id = $credentials['company_id'];
                $add_subscribe->token = $credentials['token'];
            } else {
                $add_subscribe = new SubscribeUpdate($credentials);
            }
            $add_subscribe->save();
            return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#email-settings")
                    ->with('status','Subscribers list is successfully updated');
        }
    }

    public function deleteSubscribeUpdates() {
        SubscribeUpdate::whereId(Input::get('type_id'))->delete();
        return Redirect::to(URL::route('admin-x-settings',array(Input::get('slug')))."#email-settings");
    }

    public function getSubscribeUpdates() {
        $id = Input::get('type_id');
        return Response::json(SubscribeUpdate::find($id));
    }
    
    public function exportDataHtml() {
        return Redirect::to(URL::route('admin-x-account',array(Input::get('slug')))."#export_data");
    }


    public function getAddNote($slug, $project, $section) {
        $project_info = new Project;
        $project_id = $project_info->getProjectSections($project, $slug, $section);    
        
               
        $custom_types = CustomType::where( function ($query){
            $query->where('user_id','=', 0)
                    ->orWhere('user_id','=', Auth::user()->parent_id);
        })->get(); 
        
        $project_obj = Project::whereId($project_id[0]->pid )->first();
        if(!empty($project_obj)){
            $this->layout->title = 'VersionHistory.io - Note';
            $this->layout->content = View::make( 'project.admin.add_note', array(
                'section'=> array(
                    'project_id'=>$project_id[0]->pid,
                    'section_id'=>$project_id[0]->sid,
                    'company_id'=>$project_id[0]->cid
                ),
                'project'=> $project_obj,
                'custom_types'=>$custom_types,
                'slug'=>$slug,
                'project_slug'=>$project
                
            ) );
        } else {
            App::abort(404, 'Page not found.');
        }
        
    }
    
    public function addNote() {
        $note_info = new Note;
        $credentials = $note_info->setCredentials();
        $validator = $note_info->validate($credentials, 'create');

        if($validator->fails()){
            if(Request::ajax()){
                return 0;
            } else {
                return Redirect::back()->withInput()->withErrors($validator);
            }
        } else {
            $note = new Note($credentials);
            if($note->save()){
                Section::find($note->section_id)->touch();
                Project::find($note->project_id)->touch();

                $destinationPath = public_path().'/files/'.$note->section_id.'/'.$note->id.'/';
                $files = Input::file('files');
                $res = array();
                if($files[0]){
                    if(!is_dir($destinationPath)){
                        File::makeDirectory($destinationPath, 0775, true);
                    }
                    foreach ($files as $file) {
                        $filename = $file->getClientOriginalName();
                        if($file->move($destinationPath,$filename)){
                            $row = array(
                                'note_id' => $note->id,
                                'attachment'=> $filename
                            );
                            $attach_note = new AttachmentNote( $row );
                            $attach_note->save();
                            $res[] =array('success' => true, 'file' => asset($destinationPath.$filename));
                        }
                    }
                }              
            } 
            View::share('project', Project::whereId( Input::get('project_id') )->first() );
            
            return Redirect::route('admin-x-project',array(Input::get('slug'),Input::get('project')));
        }
    }
    
    public function getEditNote($slug,$project,$section,$note) {
        $custom_types = CustomType::where( function ($query){
            $query->where('user_id','=', 0)
                    ->orWhere('user_id','=', Auth::user()->parent_id);
        })->get();
        $project_info = new Project;
        $project_id = $project_info->getProjectNotes($project, $note, $section, $slug);
        
        $note = Note::whereId($project_id[0]->nid)->with(array('section', 'section.project', 'attachment', 'type'))->first();
       
        if(!empty($note)){
            $this->layout->title = 'VersionHistory.io - Note';
            $this->layout->content = View::make( 'project.admin.edit_note', array(
               'note'=> $note,
               'custom_types'=>$custom_types,
               'slug'=>$slug,
               'project_slug'=>$project,
               'section_slug'=>$section
            ));
        } else {
             App::abort(404, 'Page not found.');
        }   
    }
    
    public function editNoteOrder() {
        if (Request::ajax())
        {
            $notesIds = Input::get( 'order', array() );

            if(is_array($notesIds)) {
                foreach ($notesIds as $key=>$id ) {
                    $note = Note::find($id);
                    if(isset($note->company_id) && Auth::user()->company_id==$note->company_id){
                        $note->notes_order = $key+1;
                        $note->save();
                    } else {
                       return Response::make("Page not found.", 404);
                    }
                }
            }
            return Response::make("ok", 200); 
        } else {
             App::abort(404, 'Page not found.');
        }
    }
    
    public function editVersionOrder() {
        if (Request::ajax())
        {
            $versionsIds = Input::get( 'order', array() );
            if(is_array($versionsIds)) {
                foreach ($versionsIds as $key=>$id ) {
                    $version = Section::find($id);
                    if(isset($version->company_id) && Auth::user()->company_id==$version->company_id){
                        $version->versions_order = $key+1;
                        $version->save();
                    } else {
                       return Response::make('"Page not found."', 404)->header('Content-type', 'application/json');
                    }
                }
            }
            return Response::make('"ok"', 200)->header('Content-type', 'application/json'); 
        } else {
             App::abort(404, 'Page not found.');
        }
    }
    
    public function editProjectOrder() {
        if (Request::ajax())
        {
            $projectsIds = Input::get( 'order', array() );
            if(is_array($projectsIds)) {
                foreach ($projectsIds as $key=>$id ) {
                    $project = Project::find($id);
                    if(isset($project->company_id) && Auth::user()->company_id==$project->company_id){
                        $project->projects_order = $key+1;
                        $project->save();
                    } else {
                       return Response::make('Page not found.', 404); 
                    }
                }
            }
            return Response::make('ok', 200); 
        } else {
             App::abort(404, 'Page not found.');
        }
    }

    public function editNote() {
        $note_info = new Note;
        $title = Input::get( 'title' );
        $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
        $credentials = $note_info->setEditCredentials();
        $validator = $note_info->validate($credentials, 'edit');
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $note = Note::find(Input::get('note_id'));
            if(!empty($note)){
                $note->title = Input::get('title');
                $note->slug = $slug;
                $note->type_id = Input::get('type_id');
                $note->description = Input::get('editor1');
                if($note->save()) {
                    Section::find($note->section_id)->touch();
                    Project::find($note->project_id)->touch();
                }

                $destinationPath = public_path().'/files/'.$note->section_id.'/'.$note->id.'/';
                $files = Input::file('files');
                $res = array();
                if($files[0]){
                    if(!is_dir($destinationPath)){
                        File::makeDirectory($destinationPath, 0775, true);
                    }
                    foreach ($files as $file) {
                        $filename = $file->getClientOriginalName();
                        if($file->move($destinationPath,$filename)){
                            $row = array(
                                'note_id' => Input::get('note_id'),
                                'attachment'=> $filename
                            );
                            $attach_note = new AttachmentNote( $row );
                            $attach_note->save();
                            $res[] =array('success' => true, 'file' => asset($destinationPath.$filename));
                        }
                    }
                }

            View::share('project', Project::find(Input::get('project_id')) );
            return Redirect::route('admin-x-project',array(Input::get('slug'),Input::get('project')));
            } else {
                 App::abort(404, 'Page not found.');
            }
        }
        
        
    }
    
    public function deleteNoteAttachment() {
        if(input::get('flag')== 1){
            $attach = AttachmentNote::whereNote_id(Input::get('note_id'))->with(array('note'))->first();
            AttachmentNote::whereNote_id(Input::get('note_id'))->whereAttachment(Input::get('f_name'))->delete();
            if(isset($attach->note->section_id) && Auth::user()->company_id == $attach->note->company_id) {
                unlink(public_path().'/files/'.$attach->note->section_id.'/'.$attach->note->id.'/'.Input::get('f_name'));
            }
            echo Input::get('f_name');
            exit();
        }
        if(input::get('flag')== 2){
            $attach = AttachmentNote::whereId(Input::get('attach'))->with(array('note'))->first();
            AttachmentNote::find(Input::get('attach'))->delete();
            if(isset($attach->note->section_id) && Auth::user()->company_id == $attach->note->company_id) {
                unlink(public_path().'/files/'.$attach->note->section_id.'/'.$attach->note->id.'/'.Input::get('f_name'));
            }
            echo Input::get('attach');
            exit();
        }
        
    }
    
    public function deleteNote() {
        $note = new Note;
        $nid = Input::get('note_id');
        $project = Project::whereId( Input::get('project_id') )->first();
        if(Auth::user()->company_id == $project->company_id){
            $note->deleteNote($nid);
            View::share('project', $project );
            return Redirect::route('admin-x-project',array(Input::get('slug'),Input::get('project')));
        } else {
             App::abort(404, 'Page not found.');
        }
    }
    
    public function getAddSection($slug, $project) {
        $project_info = new Project;
        $project_obj = $project_info->getProjects($project, $slug);
        if(isset($project_obj[0]->user_id)){
            $this->layout->title = 'VersionHistory.io - Version';
            $this->layout->content = View::make( 'project.admin.add_section', array(
                'project'=> $project_obj[0],
                'slug'=>$slug,
                'project_slug'=>$project
                    
            ) );
        } else {
             App::abort(404, 'Page not found.');
        }
    }
    
    public function addSection() {
        $section_info = new Section; 
        $credentials = $section_info->setCreateCredentials();
        $validator = $section_info->validate($credentials, 'create');

        if($validator->fails()){
            if(Request::ajax()){
                return 0;
            } else {
                return Redirect::back()->withInput()->withErrors($validator);
            }
        } else {
            $section = new Section($credentials);
            if($section->save()){
                Project::find($section->project_id)->touch();
                $destinationPath = public_path().'/files/'.$section->id.'/';
                $files = Input::file('files');
                $res = array();
                if($files[0]){
                    if(!is_dir($destinationPath)){
                        File::makeDirectory($destinationPath, 0775, true);
                    }
                    foreach ($files as $file) {
                        $filename = $file->getClientOriginalName();
                        if($file->move($destinationPath, $filename)){
                            $row = array(
                                'section_id' => $section->id,
                                'attachment'=> $filename
                            );
                            $attach_note = new AttachmentSection( $row );
                            $attach_note->save();
                            $res[] =array('success' => true, 'file' => asset($destinationPath.$filename));
                        }
                    }
                }

            }
            View::share('project', Project::whereId( Input::get('project_id') )->first() );
            return Redirect::route('admin-x-project',array(Input::get('slug'),Input::get('project')));
        }
    }
    
    public function postEditSection() {
        $section_info = new Section; 
        $credentials = $section_info->setEditCredentials();
        $validator = $section_info->validate($credentials, 'edit');
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $section = Section::find(Input::get('section_id'));
            if(Auth::user()->company_id == $section->company_id){
                $section->title = Input::get('title');
                $section->slug = $slug;
                $section->description = Input::get('editor1');
                $section->released_at = $released_at;
                $section->archived = $credentials['archived'];
                if($section->save()) {
                    Project::find($section->project_id)->touch();
                }
                $destinationPath = public_path().'/files/'.$section->id.'/';
                $files = Input::file('files');
                $res = array();
                if($files[0]){
                    if(!is_dir($destinationPath)){
                        File::makeDirectory($destinationPath, 0775, true);
                    }
                    foreach ($files as $file) {
                        $filename = $file->getClientOriginalName();
                        if($file->move($destinationPath, $filename)){
                            $row = array(
                                'section_id' => Input::get('section_id'),
                                'attachment'=> $filename
                            );
                            $attach_note = new AttachmentSection( $row );
                            $attach_note->save();
                            $res[] =array('success' => true, 'file' => asset($destinationPath.$filename));
                        }
                    }
                }
            } else {
                 App::abort(404, 'Page not found.');
            }
        }
        View::share('project', Project::find(Input::get('project_id')) );
        return Redirect::route('admin-x-project',array(Input::get('slug'),Input::get('project')));
        
    }
    
    public function getEditSection($slug, $project, $section) {
        $section_info = new Section;
        $project_id = $section_info->getSection($project, $slug, $section);
             
        $project_obj = Section::whereId($project_id[0]->sid)->with(array('note', 'attachment', 'project'))->first();
        if(isset($project_obj)){
            $this->layout->title = 'VersionHistory.io - Version';
            $this->layout->content = View::make( 'project.admin.edit_section', array(
                'project'=> $project_obj,
                'slug'=>$slug,
                'project_slug'=>$project
            ) );
        } else {
             App::abort(404, 'Page not found.');
        }
    }
        
    public function deleteSectionAttachment() {
        if(input::get('flag')== 1){
            $attach = AttachmentSection::whereSection_id(Input::get('section_id'))->whereAttachment(Input::get('f_name'))->with(array('section'))->first();
            if(isset($attach->section->company_id) && Auth::user()->company_id == $attach->section->company_id) {
                AttachmentSection::whereSection_id(Input::get('section_id'))->whereAttachment(Input::get('f_name'))->delete();
                unlink(public_path().'/files/'.$attach->section_id.'/'.Input::get('f_name'));
            }
            echo Input::get('f_name');
            exit();
        }
        if(input::get('flag')== 2){
            $attach = AttachmentSection::whereId(Input::get('attach'))->with(array('section'))->first();
            if(isset($attach->section->company_id) && Auth::user()->company_id == $attach->section->company_id){
                AttachmentSection::find(Input::get('attach'))->delete();
                unlink(public_path().'/files/'.$attach->section_id.'/'.Input::get('f_name'));
            }
            echo Input::get('attach');
            exit();
        }
    }
    
    public function deleteSection() {
        $section_info = new Section;
        $sid = Input::get('section_id');
        $pid = Input::get('project_id');
        $project = Project::find($pid);
        if(Auth::user()->company_id == $project->company_id) {
            $section_info->deleteSection($sid);
            View::share('project', $project );
            return Redirect::route('admin-x-project',array(Input::get('slug'),Input::get('project')));
        } else {
             App::abort(404, 'Page not found.');
        }
        
    }
    
    public function postFeatureRequest() {
        return Redirect::route('admin-x-project-feature-request', array(Input::get('slug'), Input::get('project'), Input::get('select-sort')));
    }
    
    public function featureRequest($slug, $project_slug, $sort = 1){
        $project_info = new Project;
        $project_id = $project_info->getProjectInfo($project_slug, $slug);
        switch ($sort) {
            case 1:
                $project = CustomerRequest::with(array( 'project'))->whereProject_id($project_id[0]->pid)->whereHidden(false)->orderBy('updated_at', 'DESC')->get();               
                break;

            case 2:
                $project = CustomerRequest::with(array( 'project'))->whereProject_id($project_id[0]->pid)->whereHidden(false)->orderBy('updated_at', 'ASC')->get();
                break;
            
            case 3:
                $project = CustomerRequest::with(array( 'project'))->whereProject_id($project_id[0]->pid)->whereHidden(false)->orderBy('type_id', 'DESC')->get();
                break;
            case 4:
                $project = CustomerRequest::with(array( 'project'))->whereProject_id($project_id[0]->pid)->whereHidden(true)->orderBy('type_id', 'DESC')->get();
                break;
        }
        
        if(!empty($project_id)){
            $settings = $this->getSettings($slug, Auth::user()->parent_id, $project_slug);
            View::share('settings', $settings);
            $this->layout->title = 'VersionHistory.io - Feature Requests';
            $this->layout->content = View::make( 'project.admin.feature_request', array(
                'notes'=>$project,
                'select_sort' => $sort,
                'slug'=>$slug, 
                'project'=>$project_slug,
                'project_title'=>$project_id[0]->p_title
            ) );
        } else {
             App::abort(404, 'Page not found.');
        }
    }

    public function postFeatureRequestHide() {
        if (Request::ajax())
        {
            $requestId = Input::get( 'request' );
            $request = CustomerRequest::with(array( 'project'))->find($requestId);
            if(!empty($request) && Auth::user()->company_id==$request->project->company_id){
                if( $request->hidden != 1 ) {
                    $request->hidden = 1;
                    $request->save();
                }
                return Response::make("ok", 200); //->header('Content-type', 'application/json'); 
            } else {
               return Response::make("Page not found.", 404);
            }
        } else {
             App::abort(404, 'Page not found.');
        }
    }
    
    public function home($slug) {
        $company = Company::find(Auth::user()->company_id);
        if($company->slug !== $slug) {
            return Redirect::action('ProjectController@home', array($company->slug));
        }
        
        $planProjects = Company::countPlanProjects($company->id);

        $addProject = (  Project::whereCompanyId($company->id)->count() < $planProjects );
        $projects = Project::whereCompanyId($company->id)->orderBy('inactived_at')->orderBy('projects_order')->orderBy('updated_at', 'desc')->latest()->get();
            
        $this->layout->title = 'VersionHistory.io - Projects';
        $this->layout->content = View::make( 'project.admin.project_list', array(
            'projects' => $projects,
            'slug'=> $slug,
            'addProject' => $addProject,
            'planProjects'=> $planProjects
        ) );
    }
    
    
    public function projectList($slug) {
        $company = Company::find(Auth::user()->company_id);
        if($company->slug !== $slug) {
            return Redirect::route('admin-x-projects', array($company->slug));
        }
        
        $planProjects = Company::countPlanProjects($company->id);

        $addProject = (  Project::whereCompanyId($company->id)->count() < $planProjects );
        $projects = Project::whereCompanyId($company->id)->orderBy('inactived_at')->orderBy('projects_order')->orderBy('updated_at', 'desc')->latest()->get();

        $this->layout->title = 'VersionHistory.io - Projects';
        $this->layout->content = View::make( 'project.admin.project_list', array(
            'projects'    => $projects,
            'slug'        => $slug,
            'settings'    => $this->getSettings($slug, Auth::id()),
            'addProject'  => $addProject,
            'planProjects'=> $planProjects
        ) );
       
    }
    
    public function project($slug,$project) {
        $project_info = new Project;
        $project_id = $project_info->getProjectId($project, $slug);
        if(isset($project_id[0]->pid)) {
            $project_obj = Project::whereId( $project_id[0]->pid )->with(array(
                        'section'=>function($query){ $query->whereArchived('no')->orderBy('versions_order')->orderBy('created_at', 'desc'); },
                        'section.note'=>  function($query) { $query->orderBy('notes_order')->orderBy('created_at', 'desc'); },
                        'section.note.type' ))->first();

            if(Auth::user()->company_id==$project_obj->company_id){
                $countArchived = Section::whereProjectId($project_obj->id)->whereArchived('yes')->count();
                $this->layout->title = 'VersionHistory.io - Versions';
                $this->layout->content = View::make( "project.admin.project", array(
                    'project'=> $project_obj,
                    'slug'=>$slug,
                    'settings'=> $this->getSettings($slug, Auth::id()),
                    'archived'=>'no',
                    'countArchived'=>$countArchived
                ) );
            } else {
                 App::abort(404, 'Page not found.');
            }
        } else {
             App::abort(404, 'Page not found.');
        }
    }
    
    
    public function projectArchivedSections($slug,$project) {
        $project_info = new Project;
        $project_id = $project_info->getProjectId($project, $slug);
        
        if(isset($project_id[0]->pid)) {
            $project_obj = Project::whereId( $project_id[0]->pid )->with(array(
                        'section'=>function($query){ $query->whereArchived('yes')->orderBy('versions_order')->orderBy('created_at', 'desc'); },
                        'section.note'=>  function($query) { $query->orderBy('notes_order')->orderBy('created_at', 'desc'); },
                        'section.note.type' ))->first();

            if(Auth::user()->company_id==$project_obj->company_id){
                $countArchived = Section::whereProjectId($project_obj->id)->whereArchived('yes')->count();
                $this->layout->title = 'VersionHistory.io - Versions';
                $this->layout->content = View::make( "project.admin.project", array(
                    'project'=> $project_obj,
                    'slug'=>$slug,
                    'settings'=> $this->getSettings($slug, Auth::id()),
                    'archived'=>'yes',
                    'countArchived'=>$countArchived

                ) );
            } else {
                 App::abort(404, 'Page not found.');
            }
        } else {
             App::abort(404, 'Page not found.');
        }
    }
    
    public function addProject(){
        $parentUser = User::find(Auth::user()->parent_id);
        $pricePlans = Config::get('app.plan', array());
        $planProjects = isset($pricePlans[$parentUser->pricing_plan]['projects'])?$pricePlans[$parentUser->pricing_plan]['projects']: 10;

        if(  Project::whereCompanyId(Auth::user()->company_id)->count() >= $planProjects ) {
            return Redirect::back()
                        ->withErrors( 'You have reached the limit of the pricing plan' );
        }
        $project_model = new Project;
        $title = Input::get( 'title' );
        $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
        $credentials = $project_model->setCreateCredentials();
        $validator = $project_model->validate($credentials, 'create');
        
        if($validator->fails()){
            return Redirect::back()
                        ->withInput()
                        ->withErrors( $validator );
        } else {
            $project = new Project( $credentials );
            $project->save();
//            View::share('projects', Project::whereUser_id( Auth::id() )->orderBy('id','desc')->get() );
            $company = Company::find(Auth::user()->company_id);
            if(!empty($company)) {
                return Redirect::route('admin-x-project', array($company->slug,  $slug));
            } else {
                return Redirect::back();
            }
        }
    }
    
    public function getEditProject($slug, $project) {
        $project_info = new Project;
        $project_id = $project_info->getProjectId($project, $slug);
        $project_obj = Project::find($project_id[0]->pid);
        if(isset($project_obj)){
            $this->layout->title = 'VersionHistory.io - Projects';
            $this->layout->content = View::make( 'project.admin.edit_project', array(
                'project'=> $project_obj,
                'slug'=> $slug,
                'project_slug'=> $project
            ) );
        } else {
            App::abort(404, 'Page not found.');
        }
    }
    
    public function postEditProject() {
        $project_model = new Project;
        $title = Input::get( 'title' );
        $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
        $credentials = $project_model->setEditCredentials();
        $validator = $project_model->validate($credentials, 'edit');
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            $project = Project::find(Input::get('project_id'));
            if(Auth::user()->company_id == $project->company_id && is_null($project->inactived_at)){
                $project->title = Input::get('title');
                $project->slug = $slug;
                $project->description = Input::get('description');
                $project->status = Input::get('status');
                $project->save();
                View::share('project', $project );
                return Redirect::route('admin-x-project',array(Input::get('slug'),$slug));
            } else {
                 App::abort(404, 'Page not found.');
            }    
        }
    }
    
    public function deleteProject() {
        $project_model = new Project;
        $id = Input::get('project_id');
        $project = Project::find($id);
        if(Auth::user()->company_id == $project->company_id){
            $project_model->deleteProject($id);
            
            Event::fire('project.deleted', array(Auth::user()->company_id));
           
            return Redirect::route('admin-x-projects',array('slug'=>Session::get('slug')));
        } else {
             App::abort(404, 'Page not found.');
        }
        
    }
    
    public function changeProjectStatus() {
        
        $slug = Input::get('slug');
        $project = Input::get('project');
        $project_info = new Project;
        $project_id = $project_info->getProjectId($project, $slug);
        if(!empty($project_id[0]->pid)){
            $project_obj = Project::find($project_id[0]->pid);
            $project_obj->status = $project_obj->status == 'public' ? 'private' : 'public';
            $project_obj->save();
//        View::share('project', $project_obj );
        }
        return Redirect::route('admin-x-project',array($slug,$project));
    }
    
    
    
        
}
