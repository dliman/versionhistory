<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
        
        public function getSettings($slug, $uid, $project_slug=null) {
            $user = User::find($uid);
            $settings = array();
            $settings['localization'] = LocalizationSetting::whereUser_id($user->parent_id)->first();
            $settings['project_setting'] = ProjectSetting::getDefaultSettings($user->parent_id, $slug, $project_slug);
            $settings['layout_setting'] = LayoutSetting::whereUser_id($user->parent_id)->first();
            $settings['feature_setting'] = FeatureSetting::whereUser_id($user->parent_id)->first();
//            $settings['feature_type'] = CustomType::where('user_id', '=', 0)->orWhere('user_id', '=', $uid)->get();
            $settings['feature_type'] = json_decode('[{"id":"0","title":"Add","show_label":"show","icon":"fa fa-plus-square","color":"#A9D86E","image":""},
                            {"id":"1","title":"Fix","show_label":"show","icon":"fa fa-check-square","color":"#59ace2","image":""},
                            {"id":"2","title":"Remove","show_label":"show","icon":"fa fa-minus-square","color":"#FF6C60","image":""}]'
                            );
            $settings['user_id'] = $uid;
            $settings['slug'] = $slug;
            $settings['project_slug'] = $project_slug;

        return $settings;
    }

}
