<?php

class CustomHelper
{

    static public function isExistSlug($slug){
        $routeCollection = Route::getRoutes();
        $toArray = array();
        foreach ($routeCollection as $value) {
            $key = explode('/', $value->getPath());
            if(!empty($key[0]) && !str_contains($key[0], array('{'))) {
                $toArray[$key[0]] = $value->getPath();
            }
        }

        return isset($toArray[$slug]);
    }

}