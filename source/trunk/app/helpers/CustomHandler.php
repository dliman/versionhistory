<?php

class CustomHandler
{

    public function onSignup($user, $company, $password = null){
        $pricePlans = Config::get('app.plan', array());
        $data = array(
            'company' => $company,
            'user' => $user,
            'plan' => isset($pricePlans[$user->pricing_plan])?$pricePlans[$user->pricing_plan]['planTitle']:'',
            'password' => $password
        );

        $email = Config::get('app.signUpEmail', 'scott@versionhistory.io');
        Mail::send('emails.signup_notifications', $data, function($message) use($email) {
            $message->from('notifications@versionhistory.io' , 'SignUp versionhistory.io');
            $message->to($email);
            $message->subject('Another registration is on the following site: versionhistory.io');
        });
        
        $emailInfo = Config::get('app.adminEmail', 'info@versionhistory.io');
        $emailUser = $user->email;
        Mail::send('emails.signup_user', $data, function($message) use($emailUser, $emailInfo) {
            $message->from($emailInfo , 'VersionHistory.io');
            $message->to($emailUser);
            $message->bcc($emailInfo , 'VersionHistory.io');
            $message->subject('Welcome to VersionHistory.io!');
        });
    }

    public function onDeleteByAdminCompany( $userId ){
        $user = User::onlyTrashed()->with('company')->find($userId);
        $data = array(
            'user' => $user,
            'comment' => 'Deleted By AdminCompany'
        );

        $this->sendDeleteAccountUser($data);
    }

    public function onDeleteAccountAdminCompany( $userId, $comment, $users){
        $user = User::onlyTrashed()->with('company')->find($userId);
        
        $data = array(
            'user' => $user,
            'comment' => $comment,
            'users' => $users
        );

        $this->sendDeleteAccountUser($data);
    }
   
    public function onDeleteAccountUser( $userId, $comment ){
        $user = User::onlyTrashed()->with('company')->find($userId);
        $data = array(
            'user' => $user,
            'comment' => $comment
        );

        $this->sendDeleteAccountUser($data);
    }
    
    private function sendDeleteAccountUser( $data = array() ){
        $email = Config::get('app.adminEmail', 'info@versionhistory.io');
        if( !empty($data) ) {
            Mail::send('emails.deletedAccount', $data, function($message) use($email) {
                $message->from($email , 'Versionhistory.io');
                $message->to($email);
                $message->subject('Account cancellation');
            });
        } else {
            Log:error('Deleted user not exist for send email');
        }
    }

    public function onUpdatePricingPlan($company_id){
        $countProjects = Project::whereCompanyId($company_id)->count();
        $countInactiveProjects = Project::whereNotNull('inactived_at')->count();
        $countPlanProjects = Company::countPlanProjects($company_id);
//Log::error(array(
//    $countProjects, $countInactiveProjects , $countPlanProjects
//));
        if( $countProjects > $countPlanProjects || $countInactiveProjects != 0) {
            $projects = Project::whereCompany_id($company_id)->orderBy('inactived_at')
                    ->orderBy('projects_order')->orderBy('updated_at', 'desc')->latest()->get();
            foreach ($projects as $key => $project) {
                if($key < $countPlanProjects) {
                    $project->inactived_at = null ;
                } else {
                    $project->inactived_at = Carbon::now() ;
                }
                $project->projects_order = $key + 1;
                $project->save();
            }
//Log::error('Recount');
        }


    }
 
    public function onDeletedProjects($company_id){
        $this->onUpdatePricingPlan($company_id);
    }
    
}