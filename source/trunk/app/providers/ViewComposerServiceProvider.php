<?php

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider {

    protected $composers = array(
        'project.admin.project_list' => 'ProjectSettingsComposer',
        'project.visitor.notes' => 'ProjectSettingsComposer',
        'project.visitor.note_details' => 'ProjectSettingsComposer',
        'project.visitor.notes_pdf' => 'ProjectSettingsComposer',
        'project.visitor.note_details_pdf'=> 'ProjectSettingsComposer',
        'project.visitor.unsubscribe'=> 'ProjectSettingsComposer',
        'project.admin.feature_request'=> 'ProjectSettingsComposer',
        'project.admin.project_list'=> 'ProjectSettingsComposer',
        'project.admin.project'=> 'ProjectSettingsComposer',
        'project.admin.project'=> 'ProjectSettingsComposer',
    );

    public function register() {

        foreach ($this->composers as $view => $composer) {
            $this->app->view->composer($view, $composer);
        }
    }

}
