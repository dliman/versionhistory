<?php

$domain = str_replace(array('http://', 'https://'), '', Config::get('app.domain'));
Route::group(array('domain' => "{$domain}"), function () {
    Route::post('stripe/webhook', 'Laravel\Cashier\WebhookController@handleWebhook');
});

Route::group(array('domain' => "{$domain}", 'before' => 'secure'), function () {

//    Route::post('stripe/webhook', 'Laravel\Cashier\WebhookController@handleWebhook');
    /* Account */
    Route::group(array('before' => 'guest'), function() {

        Route::group(array('before' => 'csrf'), function() {
            Route::post('login/sign-in', array( 'as' => 'sign-in', 'uses' => 'AccountController@signIn'));
            Route::post('login/sign-up', array( 'as' => 'sign-up', 'uses' => 'AccountController@signUp'));
            Route::post('forgot-password', array( 'as' => 'forgot-password', 'uses' => 'RemindersController@postRemind'));
            Route::post('password/reset', array( 'as' => 'password-reset-post', 'uses' => 'RemindersController@postReset'));
        });
        Route::get('login', array( 'as' => 'admin-login', 'uses' => 'AccountController@showAuth' ));
        Route::get('password/reset/{token}', array( 'as' => 'password-reset-get', 'uses' => 'RemindersController@getReset'));
    });

    Route::get('/vhadmin/login', array( 'as' => 'vhadmin-login', 'uses' => 'UserAdminController@login'));
    Route::group(array('before' => 'csrf'), function() {
        Route::post('/vhadmin/login', array( 'as' => 'vhadmin-login', 'uses' => 'UserAdminController@postLogin'));
    });
    Route::group(array('before' => 'authSuperAdmin'), function() {
        Route::get('/vhadmin', array( 'as' => 'vhadmin', 'uses' => 'UserAdminController@showIndex'));
        Route::get('/vhadmin/logout', array( 'as' => 'vhadmin-logout', 'uses' => 'UserAdminController@logout'));
        Route::group(array('before' => 'csrf'), function() {
            Route::post('/vhadmin/make-inactive', array( 'as' => 'vhadmin-make-inactive', 'uses' => 'UserAdminController@makeInactive'));
            Route::post('/vhadmin/make-active', array( 'as' => 'vhadmin-make-active', 'uses' => 'UserAdminController@makeActive'));
            Route::post('/vhadmin/user-account', array( 'as' => 'vhadmin-user-account', 'uses' => 'UserAdminController@userAccount'));
            Route::post('/vhadmin/edit-trial', array( 'as' => 'vhadmin-edit-trial', 'uses' => 'UserAdminController@editTrial'));
            Route::post('/vhadmin/edit-free', array( 'as' => 'vhadmin-edit-free', 'uses' => 'UserAdminController@editFree'));
        });
    });
    
    Route::group(array('before' => 'auth'), function() {

        Route::get('/logout', array( 'as' => 'logout', 'uses' => 'AccountController@logout'));
        Route::get('/admin/{slug}/account', array( 'as' => 'admin-x-account', 'uses' => 'AccountController@showIndex'));

        Route::group(array('before' => 'csrf'), function() {
            Route::post('account/cancel-account', array( 'as' => 'account-cancel-account', 'uses' => 'AccountController@cancelAccount'));
            Route::group(array('before' => 'notfree'), function() {
                Route::post('account/edit-billing-info', array( 'as' => 'account-edit-billing-info', 'uses' => 'AccountController@editBillingInformation'));
            });
            Route::post('account/edit-pricing-plan', array( 'as' => 'account-edit-pricing-plan', 'uses' => 'AccountController@editPricingPlan'));
        });

        Route::group(array('before' => 'subscribed'), function() {
            Route::group(array('before' => 'csrf'), function() {
                Route::post('account/edit-personal-info', array( 'as' => 'account-edit-personal-info', 'uses' => 'AccountController@editPersonalInformation'));
                Route::post('account/edit-communication-preferences', array( 'as' => 'account-edit-communication-preferences', 'uses' => 'AccountController@editCommunicationPreferences'));
                
                Route::group(array('before' => 'isAdminCompany'), function() {
                    Route::post('account/edit-user-accounts', array( 'as' => 'account-edit-user-accounts', 'uses' => 'AccountController@editUserAccounts'));
                    Route::any('/admin/{slug}/delete-user', array( 'as' => 'admin-x-delete-user', 'uses' => 'AccountController@deleteAccount'));
                    Route::post('/edit-account', array( 'as' => 'edit-account', 'uses' => 'AccountController@editAccount'));
                });

                //                Route::post('account/cancel-account', 'AccountController@cancelAccount');
                Route::post('project-list/add-project', array( 'as' => 'project-list-add-project', 'uses' => 'ProjectController@addProject'));
                Route::post('/section', array( 'as' => 'section', 'uses' => 'ProjectController@addSection'));
                Route::post('/add-note', array( 'as' => 'add-note', 'uses' => 'ProjectController@addNote'));
                Route::post('/order-note', array( 'as' => 'order-note', 'uses' => 'ProjectController@editNoteOrder'));
                Route::post('/order-version', array( 'as' => 'order-version', 'uses' => 'ProjectController@editVersionOrder'));
                Route::post('/order-project', array( 'as' => 'order-project', 'uses' => 'ProjectController@editProjectOrder'));
                
                Route::post('/delete-note-attachment', array( 'as' => 'delete-note-attachment', 'uses' => 'ProjectController@deleteNoteAttachment'));
                Route::post('/edit-note', array( 'as' => 'edit-note', 'uses' => 'ProjectController@editNote'));
                Route::post('/feature-request', array( 'as' => 'feature-request', 'uses' => 'ProjectController@postFeatureRequest'));
                Route::post('/feature-request-hide', array( 'as' => 'feature-request-hide', 'uses' => 'ProjectController@postFeatureRequestHide'));
                Route::post('edit-project', array( 'as' => 'edit-project', 'uses' => 'ProjectController@postEditProject'));
                Route::post('delete-project', array( 'as' => 'delete-project', 'uses' => 'ProjectController@deleteProject'));
                Route::post('delete-section', array( 'as' => 'delete-section', 'uses' => 'ProjectController@deleteSection'));
                Route::post('/delete-section-attachment', array( 'as' => 'delete-section-attachment', 'uses' => 'ProjectController@deleteSectionAttachment'));
                Route::post('/edit-section', array( 'as' => 'edit-section', 'uses' => 'ProjectController@postEditSection'));
                Route::post('/delete-note', array( 'as' => 'delete-note', 'uses' => 'ProjectController@deleteNote'));
                Route::post('set-localization', array( 'as' => 'set-localization', 'uses' => 'ProjectController@setLocalization'));
                Route::post('site-setting', array( 'as' => 'site-setting', 'uses' => 'ProjectController@siteSetting'));
                Route::post('feature-setting', array( 'as' => 'feature-setting', 'uses' => 'ProjectController@featureSetting'));
                Route::post('layout-setting', array( 'as' => 'layout-setting', 'uses' => 'ProjectController@layoutSetting'));
                Route::post('delete-custom-type', array( 'as' => 'delete-custom-type', 'uses' => 'ProjectController@deleteCustomType'));
                Route::post('/edit-custom-type', array( 'as' => 'edit-custom-type', 'uses' => 'ProjectController@getEditCustomType'));
                Route::post('customize-email', array( 'as' => 'customize-email', 'uses' => 'ProjectController@customizeEmail'));
                Route::post('automatic-email', array( 'as' => 'automatic-email', 'uses' => 'ProjectController@automaticEmail'));
                Route::post('/change-status', array( 'as' => 'change-status', 'uses' => 'ProjectController@changeProjectStatus'));
                Route::post('/save-edit-account', array( 'as' => 'save-edit-account', 'uses' => 'AccountController@saveEditAccount'));

                Route::post('/delete-upload-logo', array( 'as' => 'delete-upload-logo', 'uses' => 'ProjectController@deleteUploadLogo'));
                Route::post('/delete-upload-favicon', array( 'as' => 'delete-upload-favicon', 'uses' => 'ProjectController@deleteUploadFavicon'));
                Route::post('/delete-background-image', array( 'as' => 'delete-background-image', 'uses' => 'ProjectController@deleteBackgroundImage'));
                Route::post('/delete-header-image', array( 'as' => 'delete-header-image', 'uses' => 'ProjectController@deleteHeaderImage'));
                Route::post('/export-csv', array( 'as' => 'export-csv', 'uses' => 'ProjectController@exportDataCsv'));
                Route::post('export-project-csv', array( 'as' => 'export-project-csv', 'uses' => 'ProjectController@exportProjectDataCsv'));
                Route::post('/edit-subscribe-updates', array( 'as' => 'edit-subscribe-updates', 'uses' => 'ProjectController@subscribeUpdates'));
                Route::post('get-subscribe-updates', array( 'as' => 'get-subscribe-updates', 'uses' => 'ProjectController@getSubscribeUpdates'));
                Route::post('delete-subscribe-updates', array( 'as' => 'delete-subscribe-updates', 'uses' => 'ProjectController@deleteSubscribeUpdates'));

//                    Route::post('/export-html', 'ProjectController@exportDataHtml');
            });



            Route::get('/admin/{slug}/home', array( 'as' => 'admin-x-home', 'uses' => 'ProjectController@home'));
            Route::get('/admin/{slug}/projects', array( 'as' => 'admin-x-projects', 'uses' => 'ProjectController@projectList'));
            Route::get('/admin/{slug}/settings', array( 'as' => 'admin-x-settings', 'uses' => 'ProjectController@projectSetting'));
            Route::get('/admin/{slug}/{project}/feature-request/{sort?}', array( 'as' => 'admin-x-project-feature-request', 'uses' => 'ProjectController@featureRequest'));
            Route::get('/admin/{slug}/{project}/{section}/{note}/edit-note', array( 'as' => 'admin-x-project-section-note-edit-note', 'uses' => 'ProjectController@getEditNote'));
            Route::get('/admin/{slug}/{project}/{section}/add-note', array( 'as' => 'admin-x-project-section-add-note', 'uses' => 'ProjectController@getAddNote'));
            Route::get('/admin/{slug}/{project}/add-section', array( 'as' => 'admin-x-project-add-section', 'uses' => 'ProjectController@getAddSection'));
            Route::get('/admin/{slug}/{project}/{section}/edit-section', array( 'as' => 'admin-x-project-section-edit-section', 'uses' => 'ProjectController@getEditSection'));

            Route::get('/admin/{slug}/{project}', array( 'as' => 'admin-x-project', 'uses' => 'ProjectController@project'));
            Route::get('/admin/{slug}/{project}/edit-project', array( 'as' => 'admin-x-project-edit-project', 'uses' => 'ProjectController@getEditProject'));
            Route::get('/admin/{slug}/{project}/archived-sections', array( 'as' => 'admin-x-project-archived-sections', 'uses' => 'ProjectController@projectArchivedSections'));
        });
    });
});

/* visitors */

Core::init();

if (Core::isCustomDomain()) {
    /* Defined custom domain */
    $custom_domain = Core::getCustomDomain();
    Route::group(array('domain' => "{$custom_domain}", 'before' => 'unsecure'), function(){
        Route::get('/', array('as' => 'projectList', 'uses' => 'ClientController@projectListCust')); // {slug}
        Route::get('unsubscribe-updates/{token?}', array( 'as' => '', 'uses' => 'ClientController@getUnsubscribeUpdates'));
        Route::get('/{project}', array('as' => 'project', 'uses' => 'ClientController@projectCust'));  // {slug}
        Route::get('/{project}/archived-sections', array('as' => 'projectArchived', 'uses' => 'ClientController@projectCustArchived'));  // {slug}
        Route::get('/{project}/{section}/{note}', array('as' => 'getNoteDetails', 'uses' => 'ClientController@getNoteDetailsCust'));  // {slug}
        Route::get('/{project?}/search', array('as' => 'getSearch', 'uses' => 'ClientController@getSearch'));  // {slug}

        Route::group(array('before' => 'csrf'), function() {
            Route::post('subscribe-updates', array( 'as' => '', 'uses' => 'ClientController@subscribeUpdates'));
            Route::post('request-feature', array( 'as' => '', 'uses' => 'ClientController@requestFeature'));

            Route::post('/search', array( 'as' => '', 'uses' => 'ClientController@search'));
            Route::post('download-pdf', array('as' => 'download-pdf', 'uses' => 'ClientController@getDownloadPdf'));
            Route::post('unsubscribe-updates', array( 'as' => '', 'uses' => 'ClientController@postUnsubscribeUpdates'));
        });
    });
} else {
    /* Not defined custom domain */
    Route::group(array('domain' => "{$domain}"), function () {
        Route::get('/', array( 'as' => '', 'uses' => 'ClientController@showIndex'));
        Route::get('{page}.html', array( 'as' => '', 'uses' => 'ClientController@redirectHtmls'));
        Route::get('/contact', array( 'as' => '', 'uses' => 'ClientController@showContact'));
        Route::get('/faq', array( 'as' => '', 'uses' => 'ClientController@showFaq'));
        Route::get('/features', array( 'as' => '', 'uses' => 'ClientController@showFeatures'));
        Route::get('/pricing', array( 'as' => '', 'uses' => 'ClientController@showPricing'));
        Route::get('/privacy', array( 'as' => '', 'uses' => 'ClientController@showPrivacy'));
        Route::get('/terms', array( 'as' => '', 'uses' => 'ClientController@showTerms'));
        Route::get('unsubscribe-updates/{token?}', array( 'as' => '', 'uses' => 'ClientController@getUnsubscribeUpdates'));
        Route::get('/{slug}', array('as' => 'projectList', 'uses' => 'ClientController@projectList'));
        Route::get('/{slug}/{project}', array('as' => 'project', 'uses' => 'ClientController@project'));
        Route::get('/{slug}/{project}/archived-sections', array('as' => 'projectArchived', 'uses' => 'ClientController@projectArchived'));
        Route::get('/{slug}/{project}/{section}/{note}', array('as' => 'getNoteDetails', 'uses' => 'ClientController@getNoteDetails'));
        Route::get('/{slug?}/{project?}/search', array('as' => 'getSearch', 'uses' => 'ClientController@getSearch'));

        Route::group(array('before' => 'csrf'), function() {
            Route::post('subscribe-updates', array( 'as' => '', 'uses' => 'ClientController@subscribeUpdates'));
            Route::post('request-feature', array( 'as' => '', 'uses' => 'ClientController@requestFeature'));

            Route::post('/search', array( 'as' => '', 'uses' => 'ClientController@search'));
            Route::post('download-pdf', array('as' => 'download-pdf', 'uses' => 'ClientController@getDownloadPdf'));
            
            Route::post('/form-contact', array( 'as' => '', 'uses' => 'ClientController@postFormContact'));
            Route::post('unsubscribe-updates', array( 'as' => '', 'uses' => 'ClientController@postUnsubscribeUpdates'));
        });
    });
}


//Route::get( 'test/{project}/{id}', 'ClientController@test' );
