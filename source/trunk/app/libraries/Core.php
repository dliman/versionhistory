<?php

Class Core {

    protected static $slug;
    protected static $customDomain;

    static function init() {
        if (!App::runningInConsole()){  
            /* if the app is launched via console, 
             * for example upon this project deploying, 
             * and DB migration hasn't been done yet, 
             * then there occur Exception (Table doesn't exist)
            */
            $url = parse_url(URL::current());
            $current_domain = $url['host'];
            try{
                $projectSetting = ProjectSetting::whereCustomDomain($current_domain)->first();
                if (isset($projectSetting->user_id) && $user = User::with('company')->find($projectSetting->user_id)/* && isset($user->company->slug) */) {
                    self::$slug = $user->company->slug;
                    self::$customDomain = $current_domain;
                }
            } catch (Exception $ex) {
                Log::error((array)$ex);
            }       
        }        
    }

    static function isCustomDomain() {
        return isset(self::$customDomain);
    }

    static function getSlug() {
        return self::$slug;
    }

    static function getCustomDomain() {
        return self::$customDomain;
    }

}
