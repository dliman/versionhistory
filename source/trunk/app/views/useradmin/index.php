    <section id="main-content">
        <section class="wrapper">
            <div class="container">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      Users
                  </header>
                  <div class="panel-body">
                      <div class="adv-table editable-table ">
                          <div class="clearfix">
<?php /*/ ?>
                              <div class="btn-group">
                                  <button id="users-for-admin_new" class="btn green">
                                      Add New <i class="fa fa-plus"></i>
                                  </button>
                              </div>

                              <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>
                              </div>
<?php /*/ ?>
                          </div>
                          <div class="space15"></div>
                          <table class="table table-striped table-hover table-bordered" id="users-for-admin">
                              <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Date signed up</th>
                                  <th>Email address</th>
                                  <th>Company name</th>
                                  <th>Role</th>
                                  <th>Trial ends at</th>
                                  <th>Pricing Plan</th>
                                  <th>Free</th>
                                  <th>Actions</th>
                              </tr>
                              </thead>
                        <?php if(!empty($users)) : ?>
                              <tbody>
                              <?php foreach ($users as $user) : ?>
                              <tr class="">
                                  <td><?php echo $user->id ?></td>
                                  <td><?php echo $user->created_at ?></td>
                                  <td><?php echo $user->email ?></td>
                                  <td class="center"><?php echo (isset($user->company->name)?$user->company->name:'') ?></td>
                                  <td class="center"><?php echo (($user->isAdminCompany())?'Admin Company':'User') ?></td>
                                  <td class="date-trial">
                                      <span class="trial-date"><?php echo !empty($user->trial_ends_at)?$user->trial_ends_at->toDateString():''; ?></span>
                                      <a href="javascript:;" class="edit-trial btn btn-info btn-xs tip-admin pull-right" title=" Edit date " data-user_id="<?php echo $user->id; ?>"><i class="glyphicon glyphicon-edit"></i></a>
                                  </td>
                                  <td>
                                      <?php echo (isset($pricePlans[$user->parent->pricing_plan]['planTitle'])?$pricePlans[$user->parent->pricing_plan]['planTitle']: $user->parent->pricing_plan) ?>
                                  </td>
                                  <td>
                                    <?php if ($user->isAdminCompany()) : ?>  
                                    <div class="btn-group free-btn">
                                      <button data-toggle="dropdown" class="btn <?php echo ($user->free != 'yes')?'btn-success':'btn-warning' ?> dropdown-toggle btn-xs" type="button"> <?php echo $user->free ?> <span class="caret"></span></button>
                                      <ul role="menu" class="dropdown-menu">
                                          <li><a href="javascript:;" class="free-account" data-user_id="<?php echo $user->id; ?>">yes</a></li>
                                          <li><a href="javascript:;" class="free-account" data-user_id="<?php echo $user->id; ?>">no</a></li>
                                      </ul>
                                    </div>
                                    <?php else : ?>
                                      <span class="btn btn-default btn-xs">
                                        <?php echo $user->parent->free; ?>
                                      </span>
                                    <?php endif; ?>
                                  </td>
                                  <td>
                                        <?php echo Form::open(array('action'=>'UserAdminController@userAccount', 'method'=>'post', 'style'=>'display:inline-block;'));?>
                                        <?php echo Form::hidden('user_id',$user->id); ?>
                                        <?php echo Form::token(); ?>
                                        <a href="javascript:;" class="account btn btn-info btn-xs tip-admin" title=" Sign-in "><i class="glyphicon glyphicon-log-in"></i></a>
                                        <?php echo Form::close(); ?>
                                        <a class="<?php echo (!$user->deleted_at)?'make-inactive btn-danger':'make-active btn-success'?> btn btn-xs tip-admin" title="<?php echo (!$user->deleted_at)?'Make inactive':'Make active'?>" href="javascript:;" data-user_id="<?php echo $user->id; ?>">
                                            <i class="glyphicon <?php echo (!$user->deleted_at)?'glyphicon-remove':'glyphicon-ok'?>"></i>
                                            <?php // echo (!$user->deleted_at)?'Make inactive':'Make active'?> 
                                        </a>
                                  </td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                        <?php endif; ?>
                          </table>
                      </div>
                  </div>
              </section>
              <!-- page end-->
             </div>
        </section>
    </section>
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="makeInactive" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'UserAdminController@makeInactive', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will make inactive this user. Do you really want to do this?</p>
                <?php echo Form::hidden('user_id', ''); ?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Inactive', array('class' => "btn btn-success btn-make-inactive")); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="makeActive" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'UserAdminController@makeActive', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will make active this user. Do you really want to do this?</p>
                <?php echo Form::hidden('user_id', ''); ?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Active', array('class' => "btn btn-success btn-make-active")); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="editTrial" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editing</h4>
            </div>
            <?php echo Form::open(array('action' => 'UserAdminController@makeInactive', 'method' => "POST")); ?>
            <div class="modal-body">
                <?php echo Form::label('date_trial_modal', 'Trial ends at'); ?>
                <?php echo Form::text('date_trial_modal','', array('class' => 'form-control')); ?>
                <?php echo Form::hidden('user_id', ''); ?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Save', array('class' => "btn btn-success btn-edit-trial")); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<script type="text/javascript">
    
var EditableTable = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            var oTable = jQuery('#users-for-admin').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 15,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ],
                "aaSorting": [[ 0, "desc" ]],
            });

//            jQuery('#users-for-admin').on( 'page.dt', function () {
//console.log('Page change');
//                MakeClick.inactive();
//
//                MakeClick.active();
//        
////                var info = oTable.page.info();
////                $('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
//            } );

            jQuery('#users-for-admin_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#users-for-admin_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

        }

    };

}();

var MakeClick = function () {

    return {

        //main function to initiate the module
        inactive: function () {
            jQuery('.make-inactive').on("click",function(e){
                e.preventDefault();
                var user_id = jQuery(this).data('user_id');
                jQuery('#makeInactive').find('input[name="user_id"]').val(user_id);
                jQuery('#makeInactive').modal();
            });
        },
        active: function () {
            jQuery('.make-active').on("click",function(e){
                e.preventDefault();
                var user_id = jQuery(this).data('user_id');
                jQuery('#makeActive').find('input[name="user_id"]').val(user_id);
                jQuery('#makeActive').modal();
            });
        },
        account: function () {
            jQuery('.account').on("click",function(e){
                e.preventDefault();
                jQuery(this).closest('form').submit();
            });
        },
        editTrial: function () {
            jQuery('.edit-trial').on("click",function(e){
                e.preventDefault();
                var user_id = jQuery(this).data('user_id');
                var date = jQuery(this).parent().find('span.trial-date').text();
                jQuery('#editTrial').find('input[name="user_id"]').val(user_id);
                jQuery('#editTrial').find('input[name="date_trial_modal"]').val(date)
                    .bootstrapDP({
                        format: 'yyyy-mm-dd'
                    }); 
                jQuery('#editTrial').modal();
            });
        },
    };

}();            

    jQuery(document).ready(function($) {
        
        jQuery('.tip-admin').bootstrapTlp();

        MakeClick.inactive();

        MakeClick.active();
        
        MakeClick.account();
        
        MakeClick.editTrial();
        
        jQuery('.btn-make-inactive').click(function(e){
            e.preventDefault();
            var uid = $(this).closest('form').find('input[name="user_id"]').val();
            var token = $(this).closest('form').find('input[name="_token"]').val();
            jQuery.ajax({
              type: "POST",
              url: "/vhadmin/make-inactive",
              data: { _token: token, user_id:uid },
              success: function(msg){
                 jQuery('#makeInactive').modal('hide');
                 jQuery('.make-inactive[data-user_id='+uid+']')
                         .toggleClass('make-inactive').unbind('click')
                         .toggleClass('make-active').toggleClass('btn-success').toggleClass('btn-danger')
                         .html('<i class="glyphicon glyphicon-ok"></i>').attr('data-original-title', 'Make active');
                    MakeClick.active();
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                 console.log(XMLHttpRequest, textStatus, errorThrown);
                 jQuery('#makeInactive').modal('hide');
                 alert(errorThrown);
              }
            });
        });

        jQuery('.btn-make-active').click(function(e){
            e.preventDefault();
            var uid = $(this).closest('form').find('input[name="user_id"]').val();
            var token = $(this).closest('form').find('input[name="_token"]').val();
            $.ajax({
              type: "POST",
              url: "/vhadmin/make-active",
              data: { _token: token, user_id:uid },
              success: function(msg){
                 jQuery('#makeActive').modal('hide');
                 jQuery('.make-active[data-user_id='+uid+']')
                         .toggleClass('make-inactive').unbind('click')
                         .toggleClass('make-active').toggleClass('btn-success').toggleClass('btn-danger')
                         .html('<i class="glyphicon glyphicon-remove"></i>').attr('data-original-title', 'Make inactive');
                    MakeClick.inactive();
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                 console.log(XMLHttpRequest, textStatus, errorThrown);
                 jQuery('#makeActive').modal('hide');
                 alert(errorThrown);
              }
            });
        });
        
        jQuery('.btn-edit-trial').click(function(e){
            e.preventDefault();
            var uid = $(this).closest('form').find('input[name="user_id"]').val();
            var date_trial = $(this).closest('form').find('input[name="date_trial_modal"]').val();
            var token = $(this).closest('form').find('input[name="_token"]').val();
            var spinner = jQuery( '<span class="processing pull-left"></span>' ).insertAfter(
                        jQuery('.edit-trial[data-user_id='+uid+']').parent().find('span.trial-date')
                    );
            jQuery.ajax({
              type: "POST",
              url: "/vhadmin/edit-trial",
              data: { _token: token, user_id:uid, date_trial:date_trial },
              success: function(msg){
                 jQuery('#editTrial').modal('hide');
                 jQuery('.edit-trial[data-user_id='+uid+']')
                         .parent().find('span.trial-date').text(date_trial);
                 spinner.remove();
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                 console.log(XMLHttpRequest, textStatus, errorThrown);
                 jQuery('#editTrial').modal('hide');
                 alert(errorThrown);
                 spinner.remove();
              }
            });
        });

        jQuery('.free-account').click(function(e){
            e.preventDefault();
            var thisBtn = jQuery(this);

            var uid = thisBtn.data('user_id');
            var free = (thisBtn.text()==='yes')?'yes':'no';
            var token = '<?php echo Session::token()?>';
            var spinner = jQuery( '<span class="processing"></span>' ).insertAfter(thisBtn.parents('.free-btn'));
            jQuery.ajax({
              type: "POST",
              url: "/vhadmin/edit-free",
              data: { _token: token, user_id:uid, free:free },
              success: function(msg){
                var mainBtn = thisBtn.parents('.free-btn').find('.btn.dropdown-toggle').html(free+' <span class="caret"></span>');
                if(free === 'yes') {
                    mainBtn.removeClass('btn-success').addClass('btn-warning');
                } else {
                    mainBtn.removeClass('btn-warning').addClass('btn-success');
                }
                spinner.remove();
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                 console.log(XMLHttpRequest, textStatus, errorThrown);
                 alert(errorThrown);
                 spinner.remove();
              }
            });
        });
        
        EditableTable.init();
        
        
    });


</script>