<section class="login wrapper">
<div class="container">

    <div class="logo-wrapper">
        <img src="/img/logo.png" alt="" class="img-responsive">
    </div>
    

    <div class="row panel">

        <div class="form-signin">
            <?php echo Form::open(array('action' => 'UserAdminController@postLogin', 'method' => 'post', 'class' => 'form-signin', 'autocomplete' => 'off')); ?>
            <h2 class="form-signin-heading">Sign in</h2>
            <div class="login-wrap">    
                <div class="form-group">
                    <?php echo Form::label('username', 'Name: '); ?>
                    <?php echo Form::text('username', Input::old('username'), array('class' => 'form-control','autocomplete' => 'off')); ?>    
                </div>
                <div class="form-group">
                    <?php echo Form::label('password', 'Password: '); ?>
                    <?php echo Form::password('password', array('placeholder' => '', 'class' => 'form-control','autocomplete' => 'off')); ?>    
                </div>

                <?php echo Form::submit('Login', array('class' => 'btn btn-lg btn-login')); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>

        </div>

    </div>
</div>
</section>
