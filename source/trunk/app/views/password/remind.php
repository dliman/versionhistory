<?php if (Session::has('error')){
   trans(Session::get('reason'));
} elseif (Session::has('success')){
  echo 'An email with the password reset has been sent.';
}?>
 
<?php echo Form::open(array('action'=>'RemindersController@postRemind', 'method'=>"POST")) ;?>
  <?php echo Form::label('email', 'Email');?>
  <?php echo Form::email('email','', array());?>
  <?php echo Form::submit('Send Reminder'); ?>
<?php echo Form::close() ;