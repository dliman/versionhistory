<?php if (Session::has('error')){
  echo '<pre>',  print_r(Session::get('error')).'</pre>' ;
}?>
<div class="wrapper">
    <div class="container">
        <div class="row panel">
            <div class="login-wrap">
                <div class="col-sm-12">
                    <h2 class="form-signin-heading">Reset Account Password</h2>
                    <?php echo Form::open(array('action'=>'RemindersController@postReset', 'method'=>'post'));?>
                    <div class="form-group">
                        <?php echo Form::label('email','E-mail'); ?>
                        <?php echo Form::email('email', '', array('class'=>'form-control') ); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Form::label('password','Password'); ?>
                        <?php echo Form::password('password', array('class'=>'form-control') ); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Form::label('password','Password confirmation'); ?>
                        <?php echo Form::password('password_confirmation', array('class'=>'form-control') ); ?>
                    </div>
                    <div class="form-group">
                        <?php echo Form::submit('Reset Password', array('class'=>'btn btn-lg btn-primary btn-shadow'));?>
                    </div>
                    <?php echo Form::hidden('token', $token);?>
                    <?php echo Form::token();?>
                    <?php echo Form::close();?>
                </div>
            </div>
        </div>
    </div>
</div>