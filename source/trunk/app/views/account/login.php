<section class="login wrapper">
<div class="container">

    <div class="logo-wrapper">
        <img src="/img/logo.png" alt="" class="img-responsive">
    </div>
    

    <div class="row panel">

        <div class="col-sm-6">
            <?php echo Form::open(array('action' => 'AccountController@signUp', 'method' => 'post', 'class' => 'form-signin form-signup', 'autocomplete' => 'off')); ?>
            <h2 class="form-signin-heading">Register a New Account</h2>
            <div class="login-wrap">

                <div class="form-group">
                    <?php echo Form::label('email_signup', 'Email Address'); ?>
                    <?php echo Form::email('email_signup', Input::old('email_signup'), array('class' => 'form-control', 'autocomplete' => 'off')) ?>    
                </div>

                <div class="form-group">
                    <?php echo Form::label('username_signup', 'Company Name'); ?>
                    <?php echo Form::text('username_signup', Input::old('username_signup'), array('class' => 'form-control', 'autocomplete' => 'off')); ?>    
                </div>

                <div class="form-group">
                    <?php echo Form::label('password_signup', 'Password: ') ?>
                    <?php echo Form::password('password_signup', array('placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off')) ?>    
                </div>

                <div class="form-group">
                    <?php echo Form::label('password_confirmation', 'Password confirmation: ') ?>
                    <?php echo Form::password('password_confirmation', array('placeholder' => '', 'class' => 'form-control', 'autocomplete' => 'off')) ?>    
                </div>

                <div class="form-group">
                    <?php echo Form::label('pricing_plan', 'Pricing Plan (You can always switch later)'); ?>
                    <?php echo Form::select('pricing_plan', $plans, Input::old('pricing_plan') ? Input::old('pricing_plan') : 'standard20', array('class' => 'form-control', 'autocomplete' => 'off')) ?>    
                </div>

                <?php echo Form::submit('Try it Free!', array('class' => 'btn btn-lg btn-primary btn-shadow')) ?>
                <?php echo Form::token(); ?>

                <?php echo Form::close() ?>


            </div>


        </div>

        <div class="col-sm-1 or-sinin">
            <span><span>OR</span></span>
        </div>

        <div class="col-sm-5">
            <?php echo Form::open(array('action' => 'AccountController@signIn', 'method' => 'post', 'class' => 'form-signin', 'autocomplete' => 'off')); ?>
            <h2 class="form-signin-heading">Sign in</h2>
            <div class="login-wrap">    
                <div class="form-group">
                    <?php echo Form::label('email_signin', 'Email: '); ?>
                    <?php echo Form::email('email_signin', Input::old('email_signin'), array('class' => 'form-control','autocomplete' => 'off')); ?>    
                </div>
                <div class="form-group">
                    <?php echo Form::label('password_signin', 'Password: '); ?>
                    <?php echo Form::password('password_signin', array('placeholder' => '', 'class' => 'form-control','autocomplete' => 'off')); ?>    
                </div>
                <label class="checkbox">
                    <?php echo Form::checkbox('remember', 'remember', false, array('class' => 'name')); ?>&nbsp;Remember me
                    <span class="pull-right">
                        <a data-toggle="modal" href="#myModal" id="forgotPassword"> Forgot Password?</a>

                    </span>
                </label>

                <?php echo Form::submit('Login', array('class' => 'btn btn-lg btn-login')); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>





            <!-- Modal -->
            <div aria-hidden="true" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Forgot Password ?</h4>
                        </div>



                        <?php echo Form::open(array('action' => 'RemindersController@postRemind', 'method' => "POST", 'autocomplete' => 'off')); ?>
                        <div class="modal-body">
                            <p>Enter your e-mail address below to reset your password.</p>
                            <?php echo Form::email('email', '', array('placeholder' => 'Email', 'autocomplete' => 'off', 'class' => 'form-control placeholder-no-fix')); ?>

                        </div>
                        <div class="modal-footer">
                            <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                            <?php echo Form::submit('Submit', array('class' => "btn btn-success")); ?>
                            <?php echo Form::token(); ?>
                        </div>
                        <?php echo Form::close(); ?>
                    </div>
                </div>
            </div>
            <!-- modal -->

            

        </div>

    </div>
</div>
</section>
