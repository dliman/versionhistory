<?php if($errors->any()) : ?>
  <ul>
    <?php echo implode('', $errors->all('<li>:message</li>')) ?>
  </ul>
<?php endif ?>

<br />

<?php echo Form::open( array( 'action' => 'AccountController@signIn' ) ) ?>
<div>
    <?php echo Form::label('email', 'Email: ') ?>
    <?php echo Form::email('email') ?>    
</div>

<div>
    <?php echo Form::label('password', 'Password: ') ?>
    <?php echo Form::password('password') ?>    
</div>

<div>
    <?php echo Form::submit('Log-in') ?>
</div>

<?php echo Form::close() ?>

<br />

<?php echo Form::open( array( 'action' => 'AccountController@signUp' ) ) ?>
<div>
    <?php echo Form::label('email', 'Email: ') ?>
    <?php echo Form::email('email') ?>    
</div>

<div>
    <?php echo Form::label('username', 'User name: ') ?>
    <?php echo Form::text('username') ?>    
</div>

<div>
    <?php echo Form::label('password', 'Password: ') ?>
    <?php echo Form::password('password') ?>    
</div>

<div>
    <?php echo Form::label('password_confirmation', 'Password confirmation: ') ?>
    <?php echo Form::password('password_confirmation') ?>    
</div>

<div>
    <?php echo Form::submit('Log-in') ?>
</div>

<?php echo Form::close() ?>