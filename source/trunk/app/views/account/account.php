    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->

                <div class="row">

                    <div class="col-sm-12">
                        <h2 class="page-title">Account</h2>
                        <?php echo Form::open(array('action' => 'AccountController@editPersonalInformation', 'method' => 'post')); ?>

                        <div class="panel personal toggle active">
                            <div class="panel-heading">
                                <h3>Personal Information <i class="fa fa-chevron-right"></i></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('p_fname', 'First Name'); ?>
                                        <?php echo Form::text('p_fname', Auth::user()->first_name, array('class' => 'form-control')); ?>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('p_lname', 'Last Name'); ?>
                                        <?php echo Form::text('p_lname', Auth::user()->last_name, array('class' => 'form-control')); ?>                                                                            
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('p_username', 'Company Name'); ?>
                                        <?php echo Form::text('p_username', $company, array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('p_phone', 'Phone Number'); ?>
                                        <?php echo Form::text('p_phone', Auth::user()->phone, array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('p_email', 'Email Address'); ?>
                                        <?php echo Form::email('p_email', Auth::user()->email, array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('p_password', 'Password: '); ?>
                                        <?php echo Form::password('p_password', array('placeholder' => '', 'class' => 'form-control')); ?> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('p_password_confirmation', 'Confirm Password'); ?>
                                        <?php echo Form::password('p_password_confirmation', array('placeholder' => '', 'class' => 'form-control')); ?>
                                    </div>
                                </div>
                                <?php echo Form::submit('Save Changes', array('class' => 'btn btn-primary')); ?>
                                <?php echo Form::token();?>
                                <?php echo Form::hidden('anchor_flag','personal');?>
                                <?php echo Form::hidden('slug',$slug);?>

                            </div>
                        </div>
                        <?php echo Form::close(); ?>
                        <?php echo Form::open(array('action' => 'AccountController@editCommunicationPreferences', 'method' => 'post')); ?>
                        <div class="panel communication toggle">
                            <div class="panel-heading">
                                <h3>Communication Preferences <i class="fa fa-chevron-right"></i></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12 form-group">
                                        <div class="checkbox">
                                            <label>
                                                <?php echo Form::checkbox('resive', 'yes', Auth::user()->resive == 'yes' ? true : false, array('class' => '')); ?>&nbsp;Receive communication from us
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php echo Form::submit('Save Changes', array('class' => 'btn btn-primary')); ?>
                                <?php echo Form::token();?>
                                <?php echo Form::hidden('anchor_flag','communication');?>
                                <?php echo Form::hidden('slug',$slug);?>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>
                        
                        <div class="panel user_account toggle">
                            <div class="panel-heading">
                                <h3>User Accounts <i class="fa fa-chevron-right"></i></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php if (isset($current_user_accounts)) : ?>
                                        <table class="table table-bordered">
                                            <?php foreach ($current_user_accounts as $current_user_account) : ?>
                                            <tr>
                                                <td><?php echo $current_user_account->email; ?></td>
                                                <td><?php if(Auth::user()->isAdminCompany()) : ?>
                                                    <?php echo Form::open(array('action'=>'AccountController@editAccount', 'method'=>'post', 'style'=>'display:inline-block;'));?>
                                                    <?php echo Form::hidden('uid',$current_user_account->id); ?>
                                                    <?php echo Form::token(); ?>
                                                    <a href="" class="account-edit">
                                                        <i class="fa fa-pencil"></i> edit</a><?php echo Form::close(); ?> | <a href="<?php echo action('AccountController@deleteAccount',array($slug, $current_user_account->id)) ?>" class="text-danger account-delete" data-uid="<?php echo $current_user_account->id; ?>"><i class="fa fa-times"></i> delete</a>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>                                            
                                        </table>
                                        <?php endif; ?>
                                        
                                        <?php if( Auth::user()->isAdminCompany() && $count_user_accounts<$planUsers ) :  ?>
                                        <?php echo Form::open(array('action' => 'AccountController@editUserAccounts', 'method' => 'post')); ?>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <?php echo Form::label('iuaEmailAddress', 'Email Address'); ?> 
                                                <?php echo Form::email('iuaEmailAddress', '', array( 'class'=>'form-control' ) ); ?>
                                                
                                            </div>
                                            <div class="col-sm-3">
                                                <?php echo Form::label('iuaPassword', 'Password'); ?>
                                                <?php echo Form::password('iuaPassword', array('placeholder' => '', 'class' => 'form-control')); ?>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="display-block">&nbsp;</label>
                                                <?php echo Form::submit('Save', array('class'=>'btn btn-primary')) ?>
                                                <?php echo Form::token();?>
                                                <?php echo Form::hidden('anchor_flag','user_account');?>
                                                <?php echo Form::hidden('slug',$slug);?>
                                            </div>
                                        </div>
                                        <?php echo Form::close();  ?>
                                        <?php else : ?>
                                        <?php if( Auth::user()->isAdminCompany() ) :  ?>
                                        You have reached the limit of the pricing plan
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    <?php if(Auth::user()->isAdminCompany()) : ?>
                        <div class="panel billing toggle">
                            <div class="panel-heading">
                                <h3>Billing Information <i class="fa fa-chevron-right"></i></h3>
                            </div>
                            <div class="panel-body">
                        <?php echo Form::open(array('action' => 'AccountController@editPricingPlan', 'method' => 'post', 'id'=>"plan-form")); ?>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('pricing_plan', 'Pricing Plan'); ?>
                                        <?php echo Form::select('pricing_plan', $plans, Input::old('pricing_plan') ? Input::old('pricing_plan') : Auth::user()->pricing_plan, array('class' => 'form-control', 'autocomplete' => 'off')) ?>    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::hidden('slug',$slug);?>
                                        <?php echo Form::submit('Save Pricing Plan', array('class' => 'btn btn-primary')); ?>
                                    </div>
                                </div>
                        <?php echo Form::close(); ?>
                        <?php if(!Auth::user()->isFree() ) : ?>
                        <?php echo Form::open(array('action' => 'AccountController@editBillingInformation', 'method' => 'post', 'id'=>"payment-form")); ?>
                                <?php if( !Auth::user()->subscribed()) : ?>
                                <div class="alert alert-warning" role="alert"> Your free trial has ended. Please update your payment information below to re-activate your account. Thanks! </div>
                                <?php endif; ?>
                                <?php if(!empty(Auth::user()->last_four)) : ?>
                                <div class="alert alert-success" role="alert"> The following Credit Card is used: <strong>*****<?php echo Auth::user()->last_four;?></strong> </div>
                                <label>
                                    Change Credit Card Information
                                </label>
                                <?php else : ?>
                                <label>
                                    Add Credit Card Information
                                </label>
                                <?php endif; ?>
                                <div class="alert alert-danger payment-errors" role="alert" style="display: none;"></div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('b_fname', 'First Name'); ?>
                                        <?php echo Form::text('b_fname', Auth::user()->first_name, array('class' => 'form-control')) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('b_lname', 'Last Name'); ?>
                                        <?php echo Form::text('b_lname', Auth::user()->last_name, array('class' => 'form-control')) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <?php echo Form::label('b_bill_card', 'Credit Card #'); ?>
                                        <?php echo Form::text('b_bill_card', Auth::user()->bill_card, array('class' => 'form-control', 'data-stripe'=>"number")) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-1">
                                        <?php echo Form::label('cvc', 'CVC'); ?>
                                        <?php echo Form::text('cvc', '', array('class' => 'form-control', 'data-stripe'=>"cvc")) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label>
                                            Expiration Date
                                        </label>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php
                                                
                                                   
                                                echo Form::select('expire_m', array('Month' => 'Month',
                                                            '1'=>'January (01)', 
                                                            '2'=>'February (02)', 
                                                            '3'=>'March (03)', 
                                                            '4'=>'April (04)', 
                                                            '5'=>'May (05)', 
                                                            '6'=>'June (06)', 
                                                            '7'=>'July (07)', 
                                                            '8'=>'August (08)', 
                                                            '9'=>'September (09)', 
                                                            '10'=>'October (10)', 
                                                            '11'=>'November (11)', 
                                                            '12'=>'December (12)'
                                                        ), 
                                                        Auth::user()->expire_m ? Auth::user()->expire_m : 'Month', 
                                                        array('class' => 'form-control', 'data-stripe'=>"exp-month"));
                                                ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?php
                                                echo Form::select('expire_y', array('Year' => 'Year',
                                                            '2014' => '2014',
                                                            '2015' => '2015',
                                                            '2016' => '2016',
                                                            '2017' => '2017',
                                                            '2018' => '2018',
                                                            '2019' => '2019',
                                                            '2020' => '2020',
                                                            '2021' => '2021',
                                                            '2022' => '2022',
                                                            '2023' => '2023',
                                                            '2025' => '2024',
                                                            '2025' => '2025'
                                                        ), 
                                                        Auth::user()->expire_y ? Auth::user()->expire_y : 'Year', 
                                                        array('class' => 'form-control', 'data-stripe'=>"exp-year"));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6 col-md-3">
                                        <?php echo Form::label('zip', 'Zip / Postal Code'); ?>
                                        <?php echo Form::text('zip', Auth::user()->zip, array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                                <?php echo Form::submit('Save Credit Card', array('class' => 'btn btn-primary')); ?>
                                <?php echo Form::token();?>
                                <?php echo Form::hidden('anchor_flag','billing');?>
                                <?php echo Form::hidden('slug',$slug);?>
                        <?php echo Form::close(); ?>
                        <?php else : ?>
                                <div class="alert alert-info" role="alert"> The account is <strong>FREE</strong> </div>
                        <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
<?php /*/ ?>
                        <div class="panel receipts_data toggle">
                            <div class="panel-heading">
                                <h3>Receipts <i class="fa fa-chevron-right"></i></h3>
                            </div>
                            <div class="panel-body receipts">
                                <p>May 1, 2014 <a href="" class="btn btn-default">Download</a></p>
                                <p>April 1, 2014 <a href="" class="btn btn-default">Download</a></p>
                                <p>March 1, 2014 <a href="" class="btn btn-default">Download</a></p>
                                <p>February 1, 2014 <a href="" class="btn btn-default">Download</a></p>
                            </div>
                        </div>
<?php /*/ ?>
                        <div class="panel export_data toggle">
                            <div class="panel-heading">
                                <h3>Export Data <i class="fa fa-chevron-right"></i></h3>
                            </div>
                            <div class="panel-body receipts">
                                <div class="col-md-6 col-xs-12">
<?php /*/ ?>
                                    <div class="col-md-12 col-xs-12 form-group">
                                        <div class="col-md-6 col-xs-12">Export all data as HTML</div>
                                        <div class="col-md-6 col-xs-12">
                                            <?php echo Form::open(array('action'=>'ProjectController@exportDataHtml','method'=>'post')); ?>
                                            <?php echo Form::submit('Download', array('class'=>"btn btn-primary")); ?>
                                            <?php echo Form::token(); ?>
                                            <?php echo Form::hidden('slug',$slug);?>
                                            <?php echo Form::close();?>
                                        </div>
                                    </div>
<?php /*/ ?>
                                    <div class="col-md-12 col-xs-12 form-group">
                                        <div class="col-md-6 col-xs-12">Export all projects</div>
                                        <div class="col-md-6 col-xs-12">
                                            <?php echo Form::open(array('action'=>'ProjectController@exportProjectDataCsv','method'=>'post')); ?>
                                            <?php echo Form::submit('Download', array('class'=>"btn btn-primary")); ?>
                                            <?php echo Form::token(); ?>
                                            <?php echo Form::hidden('slug',$slug);?>
                                            <?php echo Form::close();?>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-xs-12 form-group">
                                        <div class="col-md-6 col-xs-12">Export all email addresses</div>
                                        <div class="col-md-6 col-xs-12">
                                            <?php echo Form::open(array('action'=>'ProjectController@exportDataCsv','method'=>'post')); ?>
                                            <?php echo Form::submit('Download', array('class'=>"btn btn-primary")); ?>
                                            <?php echo Form::token(); ?>
                                            <?php echo Form::hidden('slug',$slug);?>
                                            <?php echo Form::close();?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <?php echo Form::open(array('action' => 'AccountController@cancelAccount', 'method' => 'post')); ?>
                        <div class="panel cancel_account toggle">
                            <div class="panel-heading">
                                <h3>Cancel Account <i class="fa fa-chevron-right"></i></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group col-sm-8">
                                        <?php echo Form::label('iCancel', 'Reason for cancelling?'); ?>
                                        <?php echo Form::textarea('iCancel', '', array('class' => 'form-control account-cancel-comment')); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-8">
                                        <?php echo Form::submit('Cancel Account', array('class' => 'btn btn-danger pull-right account-cancel')); ?>
                                        <?php echo Form::hidden('anchor_flag','cancel_account');?>
                                        <?php echo Form::hidden('slug',$slug);?>
                                        <?php echo Form::token(); ?>                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php echo Form::close(); ?>

                    </div>
                </div>

               
           </div>
        </section>
    </section>
   
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="AccountDelete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'AccountController@deleteAccount', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete this account. Do you really want to do this?</p>
                <?php echo Form::hidden('account_uid-hidden', '', array('class'=>'account_uid-hidden')); ?>
                <?php echo Form::hidden('slug', $slug); ?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="AccountEdit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Account</h4>
            </div>
            
            <?php echo Form::open(array('action' => 'AccountController@saveEditAccount', 'method' => 'post')); ?>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <?php echo Form::label('m_fname', 'First Name'); ?>
                            <?php echo Form::text('m_fname', '', array('class' => 'form-control')); ?>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <?php echo Form::label('m_lname', 'Last Name'); ?>
                            <?php echo Form::text('m_lname', '', array('class' => 'form-control')); ?>                                                                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <?php echo Form::label('m_phone', 'Phone Number'); ?>
                            <?php echo Form::text('m_phone', '', array('class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <?php echo Form::label('m_email', 'Email Address'); ?>
                            <?php echo Form::email('m_email', '', array('class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <?php echo Form::label('m_password', 'Password: '); ?>
                            <?php echo Form::password('m_password', array('placeholder' => '', 'class' => 'form-control')); ?> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <?php echo Form::label('m_password_confirmation', 'Confirm Password'); ?>
                            <?php echo Form::password('m_password_confirmation', array('placeholder' => '', 'class' => 'form-control')); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                    <?php echo Form::submit('Save Changes', array('class' => 'btn btn-primary')); ?>
                    <?php echo Form::token();?>
                    <?php echo Form::hidden('anchor_flag','user_account');?>
                    <?php echo Form::hidden('slug',$slug);?>
                    <?php echo Form::hidden('m_uid','');?>
                    
                </div>
                
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="AccountCancel" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure you want to Cancel?</h4>
            </div>
            <?php echo Form::open(array('action' => 'AccountController@cancelAccount', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>If you click Yes, your account will be canceled immediately.</p>
                <?php echo Form::hidden('slug', $slug); ?>
                <?php echo Form::hidden('comment', '', array('class'=>'account-cancel-comment-hidden')); ?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('No', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Yes', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<script type="text/javascript">
 jQuery( document ).ready(function($) {
    $('#b_bill_card').removeAttr('name');
    $('#cvc').removeAttr('name');
    $('select[name=expire_m]').removeAttr('name');
    $('select[name=expire_y]').removeAttr('name');
     
    function strpos( haystack, needle, offset){		
	var i = haystack.indexOf( needle, offset ); 
	return i >= 0 ? i : false;
    }
    
    var p = window.location.hash.substring(1);
   
    if(p.length>0){
        $('.toggle').removeClass('active');
        $('.'+p).addClass('active');
        
    }

    $('.account-cancel').click(function(e){
            e.preventDefault();
            $('.account-cancel-comment-hidden').val($('.account-cancel-comment').val());
            $('#AccountCancel').modal();
        });
        
    $('.account-delete').click(function(e){
            e.preventDefault();
            var uid = $(this).data('uid');
            $('.account_uid-hidden').val(uid);
            $('#AccountDelete').modal();
        });
    
    $('.account-edit').click(function(e){
            e.preventDefault();
            var uid = $(this).closest('form').find('input[name="uid"]').val();
            var token = $(this).closest('form').find('input[name="_token"]').val();
            $.post('/edit-account', { _token: token, uid:uid }, function(data){
               
                $('#AccountEdit').modal();
                $('input[name="m_fname"]').val(data.first_name);
                $('input[name="m_lname"]').val(data.last_name);
                $('input[name="m_phone"]').val(data.phone);
                $('input[name="m_email"]').val(data.email);
                $('input[name="m_uid"]').val(data.id);
               
            },'json').error(function( msg ) {
                    alert( msg.responseText );
                });
            
        });
      
      function stripeResponseHandler(status, response) {
            var $form = $('#payment-form');
            if (response.error) {
              $form.find('.payment-errors').text(response.error.message).show();
              $form.find('input[type="submit"]').prop('disabled', false);
            } else {
              // response contains id and card, which contains additional card details
              var token = response.id;
              // Insert the token into the form so it gets submitted to the server
              $form.append($('<input type="hidden" name="stripeToken" />').val(token));
              // and submit
              $form.get(0).submit();
            }
      };
      
      $('#payment-form').submit(function(event) {
        var $form = $(this);
        
        $form.append($('<input type="hidden" data-stripe="name" />').val($form.find('input[name="b_fname"]').val()+' '+$form.find('input[name="b_lname"]').val()));
        
        // Disable the submit button to prevent repeated clicks
        $form.find('input[type="submit"]').prop('disabled', true);

        Stripe.card.createToken($form, stripeResponseHandler);

        // Prevent the form from submitting with the default action
        return false;
      });  
        
 });
</script>

