<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description"
	      content="VersionHistory.io provides better communication to your customres about your application's version history">
	<meta name="author" content="VersionHistory.io">
	<meta name="keywords"
	      content="VersionHistory.io, version history, release notes, change log, versionhistory, updates, software, web application, saas, web app">

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="icon" type="image/x-icon" href="/favicon.ico" />

	<title><?php echo isset( $title ) ? $title : '' ?></title>

	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/bootstrap-reset.css" rel="stylesheet">
	<!--external css-->
	<link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
        <link href="/assets/bootstrap-datepicker/css/datepicker.css" rel="stylesheet"/>
        <link href="/assets/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet"/>
        <link href="/css/bootstrap-timepicker.min.css" rel="stylesheet"/>
        <link href="/assets/data-tables/DT_bootstrap.css" rel="stylesheet"/>
	<!-- Custom styles for this template -->
	<link href="/css/style.css" rel="stylesheet">
	<link href="/css/style-responsive.css" rel="stylesheet"/>
	
        
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
	<!--[if lt IE 9]>
	<script src="/js/html5shiv.js"></script>
	<script src="/js/respond.min.js"></script>
	<![endif]-->
        <link rel="stylesheet" href="/css/blueimp-gallery.min.css">
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="/css/jquery.fileupload.css">
        <link rel="stylesheet" href="/css/jquery.fileupload-ui.css">
        <link rel="stylesheet" href="/css/icon-picker.min.css" rel="stylesheet"/>
        
        <!-- CSS adjustments for browsers with JavaScript disabletemplate-uploadd -->
        <noscript><link rel="stylesheet" href="/css/jquery.fileupload-noscript.css"></noscript>
        <noscript><link rel="stylesheet" href="/css/jquery.fileupload-ui-noscript.css"></noscript>
        <link href="/css/style-custom.css" rel="stylesheet"/>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">
            Stripe.setPublishableKey('<?php echo Config::get('services.stripe.pub_key'); ?>');
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-53855658-1', 'auto');
            ga('send', 'pageview');

        </script>

</head>

<body class="full-width settings-body">
<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <?php include_once app_path() . '/views/layouts/navigate.php'; ?>
    </header>
	<?php echo isset( $content ) ? $content : '' ?>
    
</section>
<div class="clearfix"></div>
<footer class="footer">
	<?php include_once app_path() . '/views/layouts/footer.php'; ?>
</footer><!-- .footer -->	
	
       <!-- js placed at the end of the document so the pages load faster -->
         <!-- Modal -->
            <?php if (Session::has('error')||($errors->any())): ?>
            
            <div aria-hidden="false" role="dialog" tabindex="-1" id="errorModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body btn-danger">
                                <?php 
                                    if ($errors->any()){ echo implode('', $errors->all(':message<br>')); }
                                    if (Session::has('error')) { echo Session::get('error'); }
                                ?>
                            </div>
                            <div class="modal-footer btn-danger">
                                <button data-dismiss="modal" class="btn btn-default">OK!</button>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- modal -->
            <?php endif; ?> 
            <?php if (Session::has('status')): ?>
            <!-- Modal -->
            <div aria-hidden="true" role="dialog" tabindex="-1" id="statusModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Status Message</h4>
                        </div>
                        <div class="modal-body">
                            <?php if (Session::has('status')) { echo Session::get('status'); } ?>
                        </div>
                        <div class="modal-footer">
                            <?php echo Form::button('Ok', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->
            <?php endif; ?> 
        <script src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="/js/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap-file-input.js"></script>
        <script type="text/javascript">
            var datepickerB3 = jQuery.fn.datepicker;
            jQuery.fn.bootstrapDP = datepickerB3;
            
            var bootstrapTooltip = jQuery.fn.tooltip.noConflict();
            jQuery.fn.bootstrapTlp = bootstrapTooltip;
        </script>
        <script type="text/javascript" src="/js/iconPicker.js"></script>

        <script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
        <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
        <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
        <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
        <script src="/js/jquery.iframe-transport.js"></script>
        <script src="/js/jquery.fileupload.js"></script>
        <script src="/js/jquery.fileupload-process.js"></script>
        <script src="/js/jquery.fileupload-image.js"></script>
        <script src="/js/jquery.fileupload-audio.js"></script>
        <script src="/js/jquery.fileupload-video.js"></script>
        <script src="/js/jquery.fileupload-validate.js"></script>
        <script src="/js/jquery.fileupload-ui.js"></script>
        
        <script class="include" type="text/javascript" src="/js/jquery.dcjqaccordion.2.7.js"></script>
        <script type="text/javascript" src="/js/hover-dropdown.js"></script>
        <script type="text/javascript" src="/js/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="/js/jquery.nicescroll.js"></script>
        <script type="text/javascript" src="/assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="/assets/data-tables/DT_bootstrap.js"></script>
        <script type="text/javascript" src="/js/respond.min.js" ></script>

        <!--common script for all pages-->
        <script src="/js/common-scripts.js"></script>

            <!-- must be added for ckeditor to work -->
       
        
  


        
        <script src="/js/custom-scripts.js"></script>
        <?php if (Session::has('error')||($errors->any())): ?>
            <script type="text/javascript">
                jQuery.noConflict();
               
                jQuery(document).ready(function($) {
                    $('#errorModal').modal();
                    
                    
                    console.log(window.location);
                    if(window.location.pathname == '/login' || window.location.pathname == '/vhadmin/login'){
                        $('.header').hide();
                    }
                    
                    
                });
                
            </script>
        <?php endif; ?>
        <?php if (Session::has('status')): ?>
        <script type="text/javascript">
            jQuery.noConflict();

            jQuery(document).ready(function($) {
                $('#statusModal').modal();
            });

        </script>
        <?php endif; ?>
        <script type="text/javascript">
                jQuery.noConflict();
               
                jQuery(document).ready(function($) {
                    if(window.location.pathname == '/login' || window.location.pathname == '/vhadmin/login'){
                        $('.header').hide();
                    }
                    
                    
                });
                
        </script>

        
</body>
</html>
