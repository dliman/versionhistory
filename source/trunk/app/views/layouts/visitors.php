<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo isset($settings['project_setting']->meta_description) ? $settings['project_setting']->meta_description : 'VersionHistory.io provides better communication to your customres about your application&rsquo;s version history' ?>">
    <meta name="author" content="VersionHistory.io">
    <meta name="keywords" content="<?php echo isset($settings['project_setting']->meta_keywords) ? $settings['project_setting']->meta_keywords : 'VersionHistory.io, version history, release notes, change log, versionhistory, updates, software, web application, saas, web app';?>">

    <title><?php if(isset($settings['project_setting']->page_title) && !empty($settings['project_setting']->page_title)){
        echo $settings['project_setting']->page_title;
    } else {
        echo isset( $title ) ? $title : '';
    } ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!--<link href="/css/navbar-fixed-top.css" rel="stylesheet">-->

      <!-- Custom styles for this template -->
    <link href="/css/style-visitors.css" rel="stylesheet">
    <link href="/css/style-responsive.css" rel="stylesheet" />
    <link href="/css/style-custom-visitors.css" rel="stylesheet" />
    <link rel="shortcut icon" href="<?php echo isset($settings['project_setting']->upload_favicon) && 
                                    !empty($settings['project_setting']->upload_favicon) ? 
                                    '/files/users/'.$settings['project_setting']->user_id.'/'.$settings['project_setting']->upload_favicon : '/favicon.ico'; ?>" type="image/x-icon">
        <link rel="icon" href="<?php echo isset($settings['project_setting']->upload_favicon) && 
                                    !empty($settings['project_setting']->upload_favicon) ? 
                                    '/files/users/'.$settings['project_setting']->user_id.'/'.$settings['project_setting']->upload_favicon : '/favicon.ico'; ?>" type="image/x-icon">
    <style><?php echo (isset($settings['layout_setting']->custom_css) && !empty($settings['layout_setting']->custom_css)) ? strip_tags($settings['layout_setting']->custom_css) : ''; ?></style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-53855658-1', 'auto');
      ga('send', 'pageview');

    </script>
  </head>

  <body class="full-width releases-body" style="
      <?php echo (isset($settings['layout_setting']->background_color) && !empty($settings['layout_setting']->background_color)) ? 
      'background-color:'.$settings['layout_setting']->background_color.';':'';  ?> 
      <?php echo (isset($settings['layout_setting']->background_image) && !empty($settings['layout_setting']->background_image)) ? 
      'background-image: url(//'.$_SERVER['HTTP_HOST'].'/files/users/'.$settings['layout_setting']->user_id.'/'.$settings['layout_setting']->background_image.'); background-repeat:repeat;':'';  ?>">

        <section id="container" class="with-banner">
            <!--header start-->
            
           
            <?php include_once app_path() . '/views/layouts/header.php'; ?>
            <!--main content start-->
            
            <?php echo isset( $content ) ? $content : '' ?>

        </section>
        <div class="clearfix"></div>
        <footer class="footer">
            <?php include_once app_path() . '/views/layouts/footer.php'; ?>
        </footer><!-- .footer -->

      <!-- Modal -->
            <?php if (Session::has('error')||($errors->any())): ?>
            
            <div aria-hidden="false" role="dialog" tabindex="-1" id="errorModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body btn-danger">
                                <?php 
                                    if ($errors->any()){ echo implode('', $errors->all(':message<br>')); }
                                    if (Session::has('error')) { echo Session::get('error'); }
                                    
                                ?>
                            </div>
                            <div class="modal-footer btn-danger">
                                <button data-dismiss="modal" class="btn btn-default">OK!</button>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- modal -->
            <?php endif; ?> 
            <?php if (Session::has('status')): ?>
            <!-- Modal -->
            <div aria-hidden="true" role="dialog" tabindex="-1" id="statusModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                             <h4>Status Message</h4>
                        </div>
                        <div class="modal-body">
                            <?php if (Session::has('status')) { echo Session::get('status'); } ?>
                        </div>
                        <div class="modal-footer">
                            <?php echo Form::button('Ok', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->
            <?php endif; ?> 
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/js/jquery.dcjqaccordion.2.7.js"></script>
    <script type="text/javascript" src="/js/hover-dropdown.js"></script>
    <script src="/js/jquery.scrollTo.min.js"></script>
    <script src="/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/js/respond.min.js" ></script>

    <!--common script for all pages-->
    <script src="/js/common-scripts.js"></script>
    <?php if (Session::has('error')||($errors->any())): ?>
        <script type="text/javascript">
            jQuery.noConflict();

            jQuery(document).ready(function($) {
                $('#errorModal').modal();
            });

        </script>
    <?php endif; ?>
    <?php if (Session::has('status')): ?>
        <script type="text/javascript">
            jQuery.noConflict();

            jQuery(document).ready(function($) {
                $('#statusModal').modal();
            });

        </script>
    <?php endif; ?>
  </body>
</html>
