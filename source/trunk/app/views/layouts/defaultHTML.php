<!DOCTYPE html>
<html lang="en" class=" ">
<head>
  <meta charset="utf-8" />
  <title><?php echo isset( $title ) ? $title . ' | ' : ''; ?>VersionHistory.io - a Better Version History for Your Product</title>
  <meta name="description" content="VersionHistory.io provides a better way to manage and communicate your version history, change logs and release notes to your customers." />
   <meta name="keywords" content="versionhistory.io, version history, change log, release notes, version updates, change logs, web app, saas" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="css/font.css" type="text/css" />
  <link rel="stylesheet" href="css/landing.css" type="text/css" />
  <link rel="stylesheet" href="css/app.css" type="text/css" />
  <link rel="stylesheet" href="css/custom.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js"></script>
    <script src="js/ie/respond.min.js"></script>
    <script src="js/ie/excanvas.js"></script>
  <![endif]-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-53855658-1', 'auto');
      ga('send', 'pageview');

    </script>
</head>
<body class="">
  	
  <!-- header -->
  <header id="header" class="navbar navbar-fixed-top bg-white box-shadow b-b b-light"  data-spy="affix" data-offset-top="1">
    <div class="container">
      <div class="navbar-header">        
        <a href="/" class="navbar-brand"><img src="images/logo.png" alt="" class="m-r-sm"></a>
        <button class="btn btn-link visible-xs" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right navigate">
          <li>
            <a href="<?php echo action('ClientController@showIndex'); ?>/">Home</a>
          </li>
          <li>
            <a href="<?php echo action('ClientController@showFeatures'); ?>">Features</a>
          </li>
          <li>
            <a href="<?php echo action('ClientController@showPricing'); ?>">Pricing</a>
          </li>
          <li class="">
            <a href="<?php echo action('ClientController@showFaq'); ?>">FAQ</a>
          </li>
          <li>
            <a href="<?php echo action('ClientController@showContact'); ?>">Contact</a>
          </li>
          <li>
            <div class="m-t-sm">
                <?php if(Auth::check()) : ?>
				<a href="<?php echo action('ProjectController@projectList',array('slug'=>Session::get('slug'))); ?>" class="btn btn-link btn-sm">Projects</a> 
				<a href="/logout" class="btn btn-sm btn-success m-l"><strong>Log out</strong></a>
                <?php else : ?>
				<a href="/login" class="btn btn-link btn-sm">Sign in</a> 
				<a href="/login" class="btn btn-sm btn-success m-l"><strong>Sign up</strong></a>
                <?php endif; ?>
			</div>
          </li>
        </ul>
      </div>
    </div>
  </header>
  <!-- / header -->

<?php
    echo isset( $content ) ? $content : '' 
?>

  <!-- footer -->
  <footer id="footer">
    <div class="bg-primary text-center">
      <div class="container wrapper">
        <div class="m-t m-b">
          Ready to give it a try? No credit card required
          <a href="/login" target="_blank" class="btn btn-lg btn-warning b-white bg-empty m-sm">30 Day Free Trial</a>
        </div>
      </div>
      <!-- <i class="fa fa-caret-down fa-4x text-primary m-b-n-lg block"></i> -->
    </div>
    <div class="bg-dark dker wrapper">
      <div class="container m-b-lg">
        <div class="row">
          <div class="col-sm-3" data-ride="animated777" data-animation="fadeInLeft" data-delay="300">
            <h5 class="text-white m-b m-t-lg">VersionHistory.io</h5>
            <p>
				<a href="/">Home</a><br>
				<a href="features">Features</a><br>
				<a href="pricing">Pricing</a><br>
				<a href="faq">FAQ</a>
            </p>
          </div>
          <div class="col-sm-3" data-ride="animated777" data-animation="fadeInLeft" data-delay="600">
            <h5 class="text-white m-b m-t-lg">Support</h5>
            <p>
<?php /*/ ?>
				<a href="help">Help Desk</a><br>
<?php /*/ ?>
				<a href="contact">Contact Us</a>
            </p>
          </div>
          <div class="col-sm-3" data-ride="animated777" data-animation="fadeInLeft" data-delay="900">
            <h5 class="text-white m-b m-t-lg">Legal</h5>
            <p>
				<a href="terms">Terms of Service</a><br>
				<a href="privacy">Privacy Policy</a>
            </p>
          </div>
          <div class="col-sm-3" data-ride="animated777" data-animation="fadeInLeft" data-delay="1200">
            <div class="m-b m-t-lg">
            	<img src="images/logo-footer.png" alt="" class="img-responsive">
            </div>
          </div>
        </div>
        <div class="text-center">
        </div>
      </div>
    </div>
  </footer>
  <!-- / footer -->

         <!-- Modal -->
            <?php if (Session::has('error')||($errors->any())): ?>
            
            <div aria-hidden="false" role="dialog" tabindex="-1" id="errorModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body btn-danger">
                                <?php 
                                    if ($errors->any()){ echo implode('', $errors->all(':message<br>')); }
                                    if (Session::has('error')) { echo Session::get('error'); }
                                ?>
                            </div>
                            <div class="modal-footer btn-danger">
                                <button data-dismiss="modal" class="btn btn-default">OK!</button>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- modal -->
            <?php endif; ?> 
            <?php if (Session::has('status')): ?>
            <!-- Modal -->
            <div aria-hidden="true" role="dialog" tabindex="-1" id="statusModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Status Message</h4>
                        </div>
                        <div class="modal-body">
                            <?php if (Session::has('status')) { echo Session::get('status'); } ?>
                        </div>
                        <div class="modal-footer">
                            <?php echo Form::button('Ok', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->
            <?php endif; ?> 

  <script src="js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.js"></script>
  <!-- App -->
  <script src="js/app.js"></script> 
  <script src="js/slimscroll/jquery.slimscroll.min.js"></script>
  
  <script src="js/appear/jquery.appear.js"></script>
  <script src="js/scroll/smoothscroll.js"></script>
  <script src="js/landing.js"></script>
  <script src="js/app.plugin.js"></script>
        <?php if (Session::has('error')||($errors->any())): ?>
            <script type="text/javascript">
                jQuery.noConflict();
               
                jQuery(document).ready(function($) {
                    $('#errorModal').modal();
                });
                
            </script>
        <?php endif; ?>
        <?php if (Session::has('status')): ?>
        <script type="text/javascript">
            jQuery.noConflict();

            jQuery(document).ready(function($) {
                $('#statusModal').modal();
            });

        </script>
        <?php endif; ?>  
</body>
</html>