
<div class="banner<?php echo (!empty($settings['layout_setting']->header_image) ?'-custom':'') ?>" style="overflow:hidden; <?php
echo isset($settings['layout_setting']->header_background) ?
        'background-color:' . $settings['layout_setting']->header_background . ';' : '';
echo (isset($settings['layout_setting']->hide_header_area) && $settings['layout_setting']->hide_header_area == 'hide') ?
        'display:none;' : '';
?>">
    <div class="container">
        <!--logo start-->
        <?php if(isset($settings['project_setting']->upload_logo) && !empty($settings['project_setting']->upload_logo)): ?>
        <a href="/<?php // echo URL::action('ClientController@project', array('id' => Session::get('project'))); ?>" class="logo">
            <img src="<?php echo (isset($settings['project_setting']->upload_logo) && !empty($settings['project_setting']->upload_logo)) ?
             URL::to('/files/users/' . $settings['project_setting']->user_id . '/' . $settings['project_setting']->upload_logo) : '';
?>" alt="" class="img-responsive"></a>
        <?php else : ?>
            <?php if ( !(isset($settings['layout_setting']->header_image) && !empty($settings['layout_setting']->header_image))) :?>
        <a href="/" class="logo"> <img src="/img/logo-small.png" alt="" class="img-responsive"></a>
            <?php endif; ?>
        <?php endif; ?>
        <!--logo end-->
        <?php echo (isset($settings['layout_setting']->header_image) && !empty($settings['layout_setting']->header_image)) ?
                '<img src="' . URL::to('/files/users/' . $settings['layout_setting']->user_id . '/' . $settings['layout_setting']->header_image) . '"  class="img-responsive" alt="" style="display:block; margin:0 auto;" >' : '';
        ?>
<?php echo (isset($settings['layout_setting']->header_code) && !empty($settings['layout_setting']->header_code)) ? $settings['layout_setting']->header_code : ''; ?>
    </div>
</div>
