<?php echo Form::open(array(
    'url'=>'search',
//    'action'=>'ClientController@search', 
    'method'=>'post', 
    'class'=>"form-inline", 
    'style'=>(isset($settings['feature_setting']->show_search_box) && ($settings['feature_setting']->show_search_box=='no')) ? 'display:none;' : '')); ?>
<?php echo Form::text('search','',array('class'=>"form-control", 'placeholder'=>"Search...")) ?>
<button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
<?php echo Form::hidden('project',Session::get('project') ); ?>
<?php echo Form::hidden('user_id',$settings['user_id']); ?>
<?php echo Form::hidden('slug',$settings['slug']); ?>
<?php echo Form::hidden('project_slug',$settings['project_slug']); ?>
<?php echo Form::hidden('select-sort',1);?> 
<?php echo Form::token(); ?>
<?php echo Form::close(); ?>

<div class="panel" 
     style="<?php echo ((isset($settings['feature_setting']->show_feature_request_box) && ($settings['feature_setting']->show_feature_request_box=='no'))&&
                        (isset($settings['feature_setting']->show_download_pdf) && ($settings['feature_setting']->show_download_pdf=='no')) &&
                        (isset($settings['feature_setting']->show_signup_email) && ($settings['feature_setting']->show_signup_email=='no')) ) ? 'display:none;' : ''; ?>">
    <div class="panel-body">
        <a data-toggle="modal" href="#subscribeUpdates" class="btn btn-default btn-block" style="<?php echo (isset($settings['feature_setting']->show_signup_email) && ($settings['feature_setting']->show_signup_email=='no')) ? 'display:none;' : ''; ?>">
            <i class="fa fa-envelope"></i> Subscribe to Updates</a>
        <?php echo Form::open(array(
            'action'=>'ClientController@getDownloadPdf', 
            'method'=>'post', 
            'style'=>(isset($settings['feature_setting']->show_download_pdf) && ($settings['feature_setting']->show_download_pdf=='no')) ? 'display:none;' : '')) ?>
        <button type='submit' class="btn btn-default btn-block pdf-download" ><i class="fa fa-download"></i> Download as PDF</button>
        <?php echo Form::token(); ?>
        <?php echo Form::hidden('path', $_SERVER['REQUEST_URI']); ?>
        <?php echo Form::close(); ?>
        <a href="#requestFeature" data-toggle="modal"
           class="btn btn-default btn-block" 
           style="<?php echo (isset($settings['feature_setting']->show_feature_request_box) && ($settings['feature_setting']->show_feature_request_box=='no')) ? 'display:none;' : ''; ?>">
            <i class="fa fa-cogs"></i> Feature Request</a>
    </div>
</div>

<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="subscribeUpdates" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Subscribe to Updates</h4>
            </div>
            <?php echo Form::open(array('action' => 'ClientController@subscribeUpdates', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>Enter your e-mail address bellow to receive updates on this release:</p>
                <?php echo Form::email('my_email', '', array('placeholder' => 'Email', 'autocomplete' => 'off', 'class' => 'form-control placeholder-no-fix')); ?>
                <?php echo Form::hidden('project_id', Session::get('project')); ?>
                <?php echo Form::hidden('user_id',$settings['user_id']); ?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Submit', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->

<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="requestFeature" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Request a Feature</h4>
            </div>
            <?php echo Form::open(array('action' => 'ClientController@requestFeature', 'method' => "POST", 'class'=>"form-horizontal", 'role'=>"form")); ?>
            <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6 col-xs-12">
                            <label class="control-label">Select Type</label>
                                <?php $feature_type = array();
                                foreach ($settings['feature_type'] as $type) {
                                    $feature_type[$type->id]=$type->title;
                                } ?>
                                <?php echo Form::select('feature_type', $feature_type,'0',array('class'=>'form-control')) ?>
                             
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <label class="control-label">Email&nbsp;</label>
                                <?php echo Form::email('my_email', '', array('placeholder' => 'Email', 'autocomplete' => 'off', 'class' => 'form-control placeholder-no-fix')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label">Description </label>
                                <?php echo Form::textarea('description','',array('class'=>'form-control','cols'=>10, 'rows'=>2)) ?>
                        </div>
                    </div>
                <?php echo Form::hidden('project_id', Session::get('project')); ?>
                <?php echo Form::hidden('user_id',$settings['user_id']); ?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Submit', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->