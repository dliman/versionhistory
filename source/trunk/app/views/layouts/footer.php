<div class="container">
    <?php echo (isset($settings['layout_setting']->footer_code) && !empty($settings['layout_setting']->footer_code)) ? 
    $settings['layout_setting']->footer_code : ''; ?>
    <p class="copyright-p">Copyright 2014. <a href="//<?php echo str_replace(array('http://', 'https://'), '', Config::get('app.domain')); ?>">Powered by VersionHistory.io</a></p>
</div>