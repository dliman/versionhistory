<div class="container">
    <div class="navbar-header ">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="fa fa-bars"></span>
        </button>

        <!--logo start-->
        <a href="" class="logo"><img src="/img/logo-small.png" alt=""></a>
        <!--logo end-->
        <div class="horizontal-menu navbar-collapse collapse pull-right">
            <ul class="nav navbar-nav navigate">
                <?php if(Auth::check()): ?>
<?php /*/ ?>
                <li><a href="<?php echo action('ProjectController@home',array('slug'=>Session::get('slug'))); ?>">Home</a></li>
<?php /*/ ?>
                <li><a href="<?php echo action('ProjectController@projectList',array('slug'=>Session::get('slug'))); ?>">Projects</a></li>
                <li><a href="<?php echo action('AccountController@showIndex',array('slug'=>Session::get('slug'))); ?>">Account</a></li>
                <li><a href="<?php echo action('ProjectController@projectSetting',array('slug'=>Session::get('slug'))); ?>">Settings</a></li>
                <li><a href="<?php echo action('AccountController@logout'); ?>">Log Out</a></li>

                <?php endif; ?>
                <?php if(UserAdmin::isAdmin()): ?>
                <li><a href="<?php echo action('UserAdminController@showIndex'); ?>">HomeAdmin</a></li>
                <li><a href="<?php echo action('UserAdminController@logout'); ?>">SuperAdmin Log Out</a></li>
                <?php endif; ?>
            </ul>

        </div>

    </div>
</div>