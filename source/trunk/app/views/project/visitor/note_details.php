
    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->


                <div class="row">
                    <div <?php echo ((isset($settings['feature_setting']->show_feature_request_box) && ($settings['feature_setting']->show_feature_request_box=='no'))&&
            (isset($settings['feature_setting']->show_download_pdf) && ($settings['feature_setting']->show_download_pdf=='no')) &&
            (isset($settings['feature_setting']->show_signup_email) && ($settings['feature_setting']->show_signup_email=='no')) &&
            (isset($settings['feature_setting']->show_search_box) && ($settings['feature_setting']->show_search_box=='no'))) ? 
                    'style="display:none;"' : 'class="col-md-3 col-md-push-9 sidebar"'; ?>>
                        <?php include_once app_path() . '/views/layouts/aside_menu.php'; ?>
                    </div>

                    <div class="<?php echo ((isset($settings['feature_setting']->show_feature_request_box) && ($settings['feature_setting']->show_feature_request_box=='no'))&&
            (isset($settings['feature_setting']->show_download_pdf) && ($settings['feature_setting']->show_download_pdf=='no')) &&
            (isset($settings['feature_setting']->show_signup_email) && ($settings['feature_setting']->show_signup_email=='no')) &&
            (isset($settings['feature_setting']->show_search_box) && ($settings['feature_setting']->show_search_box=='no'))) ? 'col-md-12' : 'col-md-9 col-md-pull-3'; ?>">
                       
                        <h4><a href="<?php echo route('project', Core::isCustomDomain()? array($project_slug):array($slug, $project_slug)) ?>"><i class="fa fa-chevron-left"></i> <?php echo $note->section->project->title; ?></a></h4>
                        <div class="panel add-release-notes">
                            <div class="panel-body">
                                <h3 style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>">
                                   <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                        <i class="<?php echo $note->type->icon ?>"></i>
                                    <?php elseif (isset($note->type->image) && !empty($note->type->image)) : ?>
                                        <img src="/files/users/<?php echo $note->user_id; ?>/<?php echo $note->type->image; ?>" width='13' height='15'>
                                    <?php endif; ?>
                                    &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                    <?php echo $note->title; ?>
                                </h3>
                                <p><?php $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'm-d-Y';?>
                                    <?php echo date($date_format, strtotime($note->updated_at));?></p>
                                <hr>
                                <p><?php echo strip_tags($note->description); ?></p>
                                <?php if(isset($note->attachment)) : ?>
                                <ul>
                                    <?php foreach ($note->attachment as $attachment) { ?>
                                    <li><a href="<?php echo url('/files/'.$note->section_id.'/'.$note->id.'/'.$attachment->attachment) ?>" target="_blank"><i class="glyphicon glyphicon-paperclip"></i> <?php echo $attachment->attachment?> </a></li>
                                    <?php } ?>
                                </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
   



