
      <section id="main-content">
          <section class="wrapper">
              <div class="container">
			  <!-- page start-->
			  
			  <div class="row">
               
                	<h2><?php echo $project->title; ?></h2>
                    	<p><?php echo $project->description; ?></p>
                        
                        
                        <?php if($project->section): ?>
                        
                        <?php for( $j=0; $j<count($project->section); $j++): ?>
                        <?php  $section = $project->section[$j]; ?>
                        <div class="panel version">
                                <div class="panel-body">
                                        <h3><?php echo $section->title; ?> <small>
                                            <?php $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'M j, Y';?>
                                                
                                            <?php echo date($date_format, strtotime($section->released_at )); ?></small></h3>
                                        <p><?php echo strip_tags($section->description); ?></p>
                                  <ul class="changes">
                                    <?php foreach($section->note as $note): ?>
                                      
                                    <li>
                                        <?php if($j==0): ?>
                                        <a href="<?php echo route('getNoteDetails', Core::isCustomDomain()?array('project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug) :  array('slug'=>$slug, 'project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug)); ?>" style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>">
                                        <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                            <i class="<?php echo $note->type->icon ?>"></i>
                                        <?php elseif (isset($note->type->image) && !empty($note->type->image)) : ?>
                                            <img src="<?php echo URL::to('/files/users/' . $project->user_id . '/' . $note->type->image); ?>" width='13' height='15'>
                                        <?php endif; ?>
                                        &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                        <?php echo strip_tags($note->title); ?>&nbsp;
                                        <?php $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'm-d-Y';?>
                                        <?php // echo '(' . date($date_format, strtotime($note->updated_at)) . ')';?></a>
                                        <?php else: ?>
                                        <a href="<?php echo route('getNoteDetails', Core::isCustomDomain()?array('project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug) : array('slug'=>$slug, 'project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug)); ?>" style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>">
                                                <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                                    <i class="<?php echo $note->type->icon ?>"></i>
                                                <?php elseif (isset($note->type->image) && !empty($note->type->image)) : ?>
                                                    <img src="<?php echo URL::to('/files/users/' . $project->user_id . '/' . $note->type->image); ?>" width='13' height='15'>
                                                <?php endif; ?>
                                        &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                                <?php echo strip_tags($note->title); ?>
                                        </a>
                                        <?php endif; ?>
                                    </li>
                                    <?php endforeach; ?>
                                    </ul>
                                </div>
                        </div>
                        <?php endfor; ?>
                       <?php endif; ?>
					
					
				</div>
			  </div>
                          
              <!-- page end-->
                </div>
          </section>
      </section>
     