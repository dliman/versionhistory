
    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->
                <div class="row">
                        <h4><i class="fa fa-chevron-left"></i> <?php echo $note->section->project->title; ?></h4>                    
                        <div class="panel add-release-notes">
                            <div class="panel-body">
                                <h3 style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>">
                                   <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                        <i class="<?php echo $note->type->icon ?>"></i>
                                    <?php elseif (isset($note->type->image) && !empty($note->type->image)) : ?>
                                        <img src="/files/users/<?php echo $note->user_id; ?>/<?php echo $note->type->image; ?>" width='13' height='15'>
                                    <?php endif; ?>
                                    &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                    <?php echo $note->title; ?>
                                </h3>
                                <p><?php $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'm-d-Y';?>
                                    <?php echo date($date_format, strtotime($note->updated_at));?></p>
                                <hr>
                                <p><?php echo strip_tags($note->description); ?></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
   



