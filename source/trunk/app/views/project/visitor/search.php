
      <section id="main-content">
          <section class="wrapper">
              <div class="container">
			  <!-- page start-->
			  
			  <div class="row">
				<div class="col-md-3 col-md-push-9 sidebar">
					<?php include_once app_path() . '/views/layouts/aside_menu.php'; ?>
				</div>
				
			  	<div class="col-md-9 col-md-pull-3">
					<h4><a href="<?php echo route('projectList', Core::isCustomDomain()?array(): array($settings['slug'])); ?>"><i class="fa fa-chevron-left"></i> Return to Release</a></h4>
					
					<h2>Search Results</h2>
					<?php if(!empty($results)): ?> 
					<div class="sort-by row">
						<div class="col-sm-4">
                                                     <?php echo Form::open(array(
                                                    'action'=>'ClientController@search', 
                                                    'method'=>'post', 
                                                    'class'=>'sorting-form'
                                                    )); ?>
                                                    <?php echo Form::select('select-sort', array(
                                                    '1'=>'Sort by Relevance',
                                                    '2'=>'Sort by Date'), isset($select_sort) ? $select_sort : 1, array('class'=>'form-control select-sort') ); ?>
                                                
                                                <?php echo Form::hidden('search', $keywords); ?>
                                                <?php echo Form::hidden('project',$project_id); ?>
                                                <?php echo Form::hidden('user_id',$settings['user_id']); ?>
                                                <?php echo Form::hidden('slug',$settings['slug']); ?>
                                                <?php echo Form::hidden('project_slug',$settings['project_slug']); ?>
                                                
                                                <?php echo Form::token(); ?>
                                                <?php echo Form::close(); ?>
							
						</div>
					</div>
					
                                        <?php // Log::error($results); ?>
                                        <?php foreach ($results as $result):?>
                                                <div class="panel search-result">
                                                    <div class="panel-body">
                                                        <a href="<?php echo route('getNoteDetails', Core::isCustomDomain()?array(
                                                            $result->p_slug, 
                                                            $result->s_slug, 
                                                            $result->n_slug,
                                                                ) : array(
                                                            $settings['slug'], 
                                                            $result->p_slug, 
                                                            $result->s_slug, 
                                                            $result->n_slug,
                                                                )) ?>" target="_blank">
                                                        <h3 style="color:<?php echo $result->t_color; ?>;">
                                                                <?php if(!empty($result->t_icon)): ?>
                                                                    <i class="<?php echo $result->t_icon;  ?>"></i>
                                                                <?php elseif (!empty($result->t_image)) : ?>
                                                                    <img src="/files/users/<?php echo $result->uid; ?>/<?php echo $result->t_image; ?>" width='13' height='15'>
                                                                <?php endif; ?>
                                                                &nbsp;<?php echo (!empty($result->t_show_label) && $result->t_show_label=='show' && !empty($result->t_title))?$result->t_title.':':'';  ?>&nbsp;
                                                                <?php echo $result->n_title; ?>
                                                        </h3></a>
                                                        <p><?php echo strip_tags($result->n_description); ?></p>
                                                    </div>
                                                </div>
                                            
                                                
                                        <?php endforeach; ?>
					<?php else: ?>
                                        <div class="panel search-result">
                                            <div class="panel-body">
                                                <h3 class="text-primary">Nothing found.</h3>
                                            </div>
                                        </div>
					<?php endif; ?>
										
				</div>
			  </div>
                         
		</div>
          </section>
      </section>
      
<script type="text/javascript">
    jQuery( document ).ready(function() {
    
        jQuery('.select-sort').change(function(){
            jQuery('.sorting-form').submit();
        });
        
    });
</script>