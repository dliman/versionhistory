
      <section id="main-content">
          <section class="wrapper">
              <div class="container">
			  <!-- page start-->
			  
			  <div class="row">
				<div <?php echo ((isset($settings['feature_setting']->show_feature_request_box) && ($settings['feature_setting']->show_feature_request_box=='no'))&&
                                                (isset($settings['feature_setting']->show_download_pdf) && ($settings['feature_setting']->show_download_pdf=='no')) &&
                                                (isset($settings['feature_setting']->show_signup_email) && ($settings['feature_setting']->show_signup_email=='no')) &&
                                                (isset($settings['feature_setting']->show_search_box) && ($settings['feature_setting']->show_search_box=='no'))) ? 
                                'style="display:none;"' : 'class="col-md-3 col-md-push-9 sidebar"'; ?>>
					<?php include_once app_path() . '/views/layouts/aside_menu.php'; ?>
				</div>
				
			  	<div class="<?php echo ((isset($settings['feature_setting']->show_feature_request_box) && ($settings['feature_setting']->show_feature_request_box=='no'))&&
                                                        (isset($settings['feature_setting']->show_download_pdf) && ($settings['feature_setting']->show_download_pdf=='no')) &&
                                                        (isset($settings['feature_setting']->show_signup_email) && ($settings['feature_setting']->show_signup_email=='no')) &&
                                                        (isset($settings['feature_setting']->show_search_box) && ($settings['feature_setting']->show_search_box=='no'))) ? 
                                'col-md-12' : 'col-md-9 col-md-pull-3'; ?>">
                                <?php if(isset($projectsCount) && $projectsCount > 1) : ?>
				<h4><a href="<?php echo route('projectList', Core::isCustomDomain()?array():array('slug'=>$slug)); ?>"><i class="fa fa-chevron-left"></i> Projects </a></h4>
                                <?php endif; ?>
                        <?php if(!(!empty($settings['layout_setting']->header_text_show) && $settings['layout_setting']->header_text_show=='hide')): ?>        
                	<h2 <?php echo (!empty($settings['layout_setting']->header_text_color)) ? 'style="color: '.$settings['layout_setting']->header_text_color.';"' : ''; ?>><?php echo $project->title; ?></h2>
                    	<p <?php echo (!empty($settings['layout_setting']->header_text_color)) ? 'style="color: '.$settings['layout_setting']->header_text_color.';"' : ''; ?>><?php echo $project->description; ?></p>
                        <?php endif; ?>
                        
                        
                        <?php if($project->section): ?>
                        
                        <?php for( $j=0; $j<count($project->section); $j++): ?>
                        <?php  $section = $project->section[$j]; ?>
                        <div class="panel version">
                                <div class="panel-body">
                                        <h3><?php echo $section->title; ?> <small>
                                            <?php $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'M j, Y';?>
                                                
                                            <?php echo date($date_format, strtotime($section->released_at )); ?>
                                            <?php if($section->archived == 'yes') : ?> <span class="label label-default">archived</span> <?php endif; ?>
                                        </small></h3>
                                        <p><?php echo strip_tags($section->description); ?></p>
                                        <?php if(isset($section->attachment)) : ?>
                                        <ul>
                                            <?php foreach ($section->attachment as $attachment) { ?>
                                            <li><a href="<?php echo url('/files/'.$section->id.'/'.$attachment->attachment) ?>" target="_blank"><i class="glyphicon glyphicon-paperclip"></i> <?php echo $attachment->attachment?> </a></li>
                                            <?php } ?>
                                        </ul>
                                        <?php endif; ?>
                                  <ul class="changes">
                                    <?php foreach($section->note as $note): ?>
                                      
                                    <li>
                                        <?php if($j==0): ?>
                                        <a href="<?php echo route('getNoteDetails', Core::isCustomDomain()?array('project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug) : array('slug'=>$slug, 'project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug)); ?>" style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>">
                                        <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                            <i class="<?php echo $note->type->icon ?>"></i>
                                        <?php elseif (isset($note->type->image) && !empty($note->type->image)) : ?>
                                            <img src="/files/users/<?php echo $project->user_id; ?>/<?php echo $note->type->image; ?>" width='13' height='15'>
                                        <?php endif; ?>
                                        &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                        <?php echo strip_tags($note->title); ?>&nbsp;
                                        <?php $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'm-d-Y';?>
                                        <?php // echo '(' . date($date_format, strtotime($note->updated_at)) . ')';?></a>
                                        <?php else: ?>
                                        <a href="<?php echo route('getNoteDetails', Core::isCustomDomain()?array('project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug) : array('slug'=>$slug, 'project_slug'=>$project_slug, 'section_slug'=>$section->slug, 'note_slug'=>$note->slug)); ?>" style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>">
                                                <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                                    <i class="<?php echo $note->type->icon ?>"></i>
                                                <?php elseif (isset($note->type->image) && !empty($note->type->image)) : ?>
                                                    <img src="/files/users/<?php echo $project->user_id; ?>/<?php echo $note->type->image; ?>" width='13' height='15'>
                                                <?php endif; ?>
                                                &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                                <?php echo strip_tags($note->title); ?>
                                        </a>
                                        <?php endif; ?>
                                    </li>
                                    <?php endforeach; ?>
                                    </ul>
                                </div>
                        </div>
                        <?php endfor; ?>
                       <?php endif; ?>
                       <?php if($countArchived) : ?>
                        <a href="<?php echo route((!$archived)?'projectArchived':'project',Core::isCustomDomain()? array('project_slug'=>$project_slug) :array('slug'=>$slug, 'project_slug'=>$project_slug)); ?>"
                           class="btn btn-default pull-right"> <?php echo (!$archived)?'Show older versions':'Show latest versions'; ?> </a>
                       <?php endif; ?>
				</div>
			  </div>
              <!-- page end-->
                </div>
          </section>
      </section>
     