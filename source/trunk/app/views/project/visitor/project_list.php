
    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->

                <div class="row">
                                   
                    <?php if (isset($projects) && !empty($projects[0])): ?>
                    <div class="col-md-12 ">
                        <?php foreach ($projects as $project): ?>
                            
                            <div class="panel project-list">
                                <div class="panel-body">
                                    <div class="project">
                                        <h3>
                                            <a href="<?php echo route('project',Core::isCustomDomain()? array('project_slug'=>$project->slug) :array('slug'=>$slug, 'project_slug'=>$project->slug)); ?>"><?php echo $project->title; ?></a>
                                    <!-- <span class="label label-success">public</span> -->
                                        </h3>
                                        <p><?php echo $project->description; ?></p>

                                    </div>
                                </div>
                            </div>
                       
                        
                        <?php endforeach; ?> 
                    </div>
                     <?php else: ?>
                        <div class="col-md-12 ">
                            <div class="panel">
                                <div class="panel-body">
                                    <p>No projects found for your review</p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </section>
    