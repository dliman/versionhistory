<div class="wrapper">
    <div class="container">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Are you sure?</h4>
                </div>
                <?php echo Form::open(array('action'=>'ClientController@postUnsubscribeUpdates', 'method'=>'post'));?>
                <div class="modal-body">
                    <p>Don't want to receive emails from <?php echo $project_name ; ?>. Do you really want to do this?</p>
                </div>
                <div class="modal-footer">
                    <?php echo Form::hidden('token', $token);?>
                    <a href="<?php echo url('/'); ?>" class="btn btn-default">Cancel</a>
                    <?php echo Form::submit('Unsubscribe', array('class' => "btn btn-success")); ?>
                </div>
                <?php echo Form::close(); ?>
            </div>
        </div>
    </div>
</div>