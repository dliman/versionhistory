<section id="main-content">
        <section class="wrapper">
            <div class="container">
            <!-- page start-->

            <div class="row">
                <div class="col-md-12">

                    <h4>
                        <a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project_slug)); ?>">
                            <i class="fa fa-chevron-left"></i> <?php echo $note->section->project->title; ?> </a>
                    </h4>
                    <?php /*if ($errors->any()) : ?>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <?php echo implode('', $errors->all(':message<br>')); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif */ ?>

                    <div id="validation-errors"></div>


                    <div class="panel add-release-notes">
                        <div class="panel-body">
                            <?php
                            echo Form::open(array(
                                'action' => 'ProjectController@editNote',
                                //'action' => 'UploadHandler@editNote',
                                'method' => 'post',
                                'id' => 'fileupload',
                                'enctype' => "multipart/form-data"
                            ));
                            ?>

                            <div class="row form-group">

                                <div class="col-sm-9 col-xs-12">
                                    <p>Note Name</p>
                                    <?php echo Form::text(
                                            'title', 
                                            Input::old('title') ? Input::old('title') : $note->title, 
                                            array(
                                                'class' => "form-control title"
                                                )); ?>
                                </div>

                                <div class="col-sm-3 col-xs-12">
                                    <p>Select Type</p>
                                    <?php if(isset($custom_types)){
                                        $types = array();
                                        foreach ($custom_types as $custom_type) {
                                            $types[$custom_type->id]=$custom_type->title;
                                        }
                                    } ?>
                                    <?php echo Form::select('type_id', $types, Input::old('type_id') ? Input::old('type_id') : (isset($note->type->id)?$note->type->id:''), array('class' => 'form-control type')); ?>
                                    
                                </div>
                                
                               
                            </div>
                            <p>Note Details (optional)</p>
                            <div class="form-group">
                                <?php echo Form::textarea('editor1', Input::old('description') ? Input::old('description') : $note->description, array('id'=>'editor1', 'class' => 'form-control ckeditor', 'rows' => '6')) ?>
                            </div>
                            <div class="trakingRedactor" style='display:none'></div>
                            <div class="row fileupload-buttonbar form-button-block">
                                <div class="col-lg-12">
                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn btn-success fileinput-button attach-file">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Add Attachment</span>
                                        <input type="file" name="files[]" multiple>
                                    </span>


                                </div>
                                <!-- The global progress state -->
                                <div class="col-lg-12 fileupload-progress fade">
                                    <!-- The global progress bar -->
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                    </div>
                                    <!-- The extended global progress state -->
                                    <div class="progress-extended">&nbsp;</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <table role="presentation" class="table table-attached-files">
                                    <tbody class="attached-files">
                                        <?php if($note->attachment):?>
                                        <?php foreach ($note->attachment as $attachment) :?>
                                        <tr>
                                            <td><p data-attach="<?php echo $attachment->id ?>" class="name"><?php echo $attachment->attachment; ?></p></td>
                                            <td><i class="glyphicon glyphicon-remove-circle cancel" data-attach="<?php echo $attachment->id ?>"></i></td>
                                        </tr>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </tbody></table>
                                <!-- The table listing the files available for upload/download -->
                                <table role="presentation" class="table table-upload-files"><tbody class="files"></tbody></table>
                            </div>
                            <div class="clearfix"></div>





                            <div class="col-mg-12 fileupload-buttonbar form-button-block">
                                <?php echo Form::submit('Save',array('class'=>"start btn btn-success ")); ?>
                                <?php // echo Form::button('Cancel',array('class'=>"cancel btn btn-default ")); ?>
                                <a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project_slug)); ?>" class="btn btn-default">Cancel</a>
                                <?php echo Form::button('Delete',array('class'=>"delete btn btn-danger  pull-right")); ?>
                                
                                <!-- The global file processing state -->
                                <span class="fileupload-process"></span>
                                <?php echo Form::token(); ?>
                                <?php echo Form::hidden('section_id', $note->section->id, array('class'=>'section_id')); ?>
                                <?php echo Form::hidden('note_id', $note->id, array('class'=>'note_id')); ?>
                                <?php echo Form::hidden('project_id', $note->section->project->id, array('class'=>'project_id')); ?>
                                <?php echo Form::hidden('slug', $slug, array('class'=>'slug'));?>
                                <?php echo Form::hidden('project', $project_slug, array('class'=>'project'));?>
                            </div>

                            <?php echo Form::close(); ?>
                        </div>
                    </div>


                </div>
            </div>

           
            </div>
        </section>
    </section>
    
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="ModalNoteDelete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'ProjectController@deleteNote', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your note. Do you really want to do this?</p>
                <?php echo Form::hidden('note_id', $note->id, array('class'=>'note-hidden')); ?>
                <?php echo Form::hidden('project_id', $note->section->project->id); ?>
                <?php echo Form::hidden('slug', $slug, array('class'=>'slug'));?>
                <?php echo Form::hidden('project', $project_slug, array('class'=>'project'));?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <p class="name">{%=file.name%}</p>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}

    <i class="glyphicon glyphicon-upload start"></i>


    {% } %}
    {% if (!i) { %}

    <i class="glyphicon glyphicon-remove-circle cancel"></i>

    {% } %}
    </td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <p class="name">{%=file.name%}</p>
    </td>
    
    <td>
    
    <i class="glyphicon glyphicon-remove-circle cancel"></i>
    
    </td>
    </tr>
    {% } %}

</script>
<script type="text/javascript">
    jQuery( document ).ready(function($) {
        CKEDITOR.replace('editor1');
        var editor_text;
        function updateTrakingRedactor(){
            editor_text = CKEDITOR.instances.editor1.getData();
            $('.trakingRedactor').html(editor_text);
        }
        timer = setInterval( function(){ 
            updateTrakingRedactor();
        },100);
            'use strict';

        // Initialize the $ File Upload widget:
        $('#fileupload').fileupload();

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            {
                url: '/edit-note',  
                complete: function (e, data) {
                    window.location.href = window.location.origin+'/admin/'+jQuery('.slug').val()+'/'+jQuery('.project').val();
                }
            }
        ).on('fileuploadsubmit', function (e, data) {
            data.formData = {
                'title': $('.title').val(),
                'type_id' : $('.type').val(),
                'editor1': $('.trakingRedactor').html(),
                'section_id': $('.section_id').val(),
                'note_id': $('.note_id').val(),
                'project_id': $('.project_id').val(),
                'slug': $('.fileupload-buttonbar .slug').val(),
                'project': $('.fileupload-buttonbar .project').val(),
                '_token' : $('.fileupload-buttonbar').find('input[name="_token"]').val()
            };
        });

        $('.form-button-block').find('.start').click(function(){

            var table_files_upload = $('.table-upload-files').find('.template-upload').size();
            var table_files_download = $('.table-upload-files').find('.template-download').size();
            if( (table_files_upload == 0) || (table_files_download > 0) ){
                $('#fileupload').submit();
            }
        });           

        $('.table-upload-files').on('click', '.template-download .cancel', function(){
            var f_name = $(this).closest('.template-download').find('.name').text();
            var token = $('.fileupload-buttonbar').find('input[name="_token"]').val();
            var note_id = $('.fileupload-buttonbar').find('input[name="note_id"]').val();
            var flag = 1;
            $.post('/delete-note-attachment',{flag: flag, note_id: note_id, f_name: f_name, _token: token },function(data){

            });
        });

        $('.table-attached-files').on('click', '.cancel', function(){
            var attach = $(this).data('attach');
            var f_name = $('p[data-attach="'+attach+'"]').text();
            var token = $('.fileupload-buttonbar').find('input[name="_token"]').val();
            var flag = 2;
            $.post('/delete-note-attachment',{flag: flag, attach: attach, f_name: f_name, _token: token },function(data){
                $('.table-attached-files').find('.cancel').each(function(){
                    if($(this).attr('data-attach') == data){
                        $(this).closest('tr').remove();
                    }
                });
            });

        });
        
        $('.delete').click(function(){
             $('#ModalNoteDelete').modal();
        });
    });
</script>


