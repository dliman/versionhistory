    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->

                <div class="row">
                    <div class="col-md-12">

                        <div class="panel add-new">
                            <div class="panel-body">
                                <?php echo Form::open(array('action' => 'ProjectController@postEditProject', 'method' => 'post', 'class' => 'form-horizontal')); ?>


                                <div class="form-group">
                                    <?php echo Form::label('title', 'Project Name', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php echo Form::text('title', Input::old('title')?Input::old('title'):$project->title, array('placeholder' => '', 'class' => 'form-control')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php
                                        echo Form::select('status', array(
                                            'public' => 'Public',
                                            'private' => 'Private'
                                                ), Input::old('status')?Input::old('status'):$project->status, array('class' => 'form-control'));
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo Form::label('description', 'Description (optional)', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php echo Form::text('description', Input::old('description')?Input::old('description'):$project->description, array('class' => 'form-control')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo Form::submit('Save', array('class' => 'btn btn-primary btn-shadow btn-block')); ?>
                                        <?php echo Form::hidden('project_id', $project->id); ?>
                                        <?php echo Form::hidden('slug', $slug); ?>
                                        <?php echo Form::hidden('project', $project_slug); ?>
                                        <?php echo Form::token(); ?>
                                    </div>
                                </div>

                                <?php echo Form::close(); ?>
                            </div>
                        </div>
                        
                    </div>
                </div>

                
            </div>
        </section>
    </section>
    
