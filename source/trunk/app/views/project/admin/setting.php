
    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->

                <div class="row">

                    <div class="col-sm-12">
                        <h2 class="page-title">Settings &amp; Customizations</h2>
                        <?php echo Form::open(array('action'=>'ProjectController@setLocalization', 'method'=>'post',)); ?>
                            <div class="panel localization toggle active">
                                <div class="panel-heading">
                                    <h3>Localization Settings <i class="fa fa-chevron-right"></i></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-md-3">
                                            <label for="iDateFormat">
                                                Date format
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <?php echo Form::select('date_format', array(
                                                "m-d-Y"=>'MM-DD-YYYY',
                                                "d-m-Y"=>'DD-MM-YYYY',
                                                "F d, Y"=>'Month DD, YYYYY',
                                                "d F Y"=>'DD Month YYYYY',
                                            ),Input::old('date_format') ? Input::old('date_format'):
                                                    (isset($settings['user']->localization) && 
                                                        !empty($settings['user']->localization->time_zone)) ? 
                                                            $settings['user']->localization->date_format : "m-d-Y" , array('class'=>"form-control")); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-7 col-md-6">
                                            <label for="iTimeZone">
                                                Time Zone
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <?php 
                                            $timeZones = array();
                                            $dateTimeUTC = new DateTime("now", new DateTimeZone("UTC"));
                                            foreach (DateTimeZone::listIdentifiers() as $key => $value) {
                                                $dateTimeZone = new DateTimeZone($value);
                                                $offset = sprintf('%+d:00', ($dateTimeZone->getOffset($dateTimeUTC)!=0) ? round($dateTimeZone->getOffset($dateTimeUTC)/3600) : 0) ;
//                                                $sign = ($dateTimeZone->getOffset($dateTimeUTC)<0)?'':'+';
                                                $timeZones[$value] =  "(GMT {$offset}) {$value} " ;
                                            };
                                                echo Form::select('time_zone', $timeZones
//                                                        array(
//                                                "America/Los_Angeles"=>'(GMT -8:00) Pacific Time (US &amp; Canada)',
//                                                "America/Denver"=>'(GMT -7:00) Mountain Time (US &amp; Canada)',
//                                                "America/Chicago"=>'(GMT -6:00) Central Time (US &amp; Canada)',
//                                                "America/New_York"=>'(GMT -5:00) Eastern Time (US &amp; Canada)',
//                                            )
                                                        , Input::old('time_zone') ? Input::old('time_zone'):
                                                    (isset($settings['user']->localization) && 
                                                        !empty($settings['user']->localization->time_zone)) ? 
                                                            $settings['user']->localization->time_zone : "America/Los_Angeles", array('class'=>"form-control")); ?>
                                            
                                        </div>
                                    </div>
                                    <?php echo Form::submit('Save Changes',array('class'=>"btn btn-primary")); ?>
                                    <?php echo Form::token();?>
                                    <?php echo Form::hidden('project_id', '');?>
                                    <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
                                </div>
                            </div>
                            <?php echo Form::close(); ?>
                        
                            <?php echo Form::open(array('action'=>'ProjectController@siteSetting', 'method'=>'post','files'=>true)); ?>
                            <div class="panel site-setting toggle">
                                <div class="panel-heading">
                                    <h3>Site Settings <i class="fa fa-chevron-right"></i></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="iPageTitle">
                                                Page Title
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <?php echo Form::text('page_title', Input::old('page_title') ? Input::old('page_title') :  
                                                    (isset($settings['user']->site_setting->page_title) ? 
                                                        $settings['user']->site_setting->page_title : ''),array('class'=>"form-control", 'placeholder'=>$placeholder['page_title']) ); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="iMetaDesc">
                                                Meta Description
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <?php echo Form::textarea('meta_description', 
                                                    Input::old('meta_description') ? Input::old('meta_description') :  
                                                    (isset($settings['user']->site_setting->meta_description) ? 
                                                        $settings['user']->site_setting->meta_description : ''),
                                                    array('class'=>"form-control", 'placeholder'=>$placeholder['meta_description'], 'cols'=>"30", 'rows'=>"5") ); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="iMetaKey">
                                                Meta Keywords
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <?php echo Form::text('meta_keywords', Input::old('meta_keywords') ? Input::old('meta_keywords') : 
                                                    (isset($settings['user']->site_setting->meta_keywords) ? 
                                                        $settings['user']->site_setting->meta_keywords : ''), array('class'=>"form-control", 'placeholder'=>$placeholder['meta_keywords'])); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="iDomain">
                                                Custom Domain
                                                <i class="fa fa-question-circle tip" title='To setup a custom domain, you will need to create a CNAME record (a subdomain name, such as "version") at your domain registrar and point it to: versionhistory.io. Once that is done, enter in the full domain here (example: version.your-domain.com).' data-placement="right"></i>
                                            </label>
                                            <?php echo Form::text('custom_domain', Input::old('custom_domain') ? Input::old('custom_domain') : 
                                                    (isset($settings['user']->site_setting->custom_domain) ? 
                                                        $settings['user']->site_setting->custom_domain : ''), array('class'=>"form-control")); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <div class="col-md-6 col-xs-12">
                                                <label for="iLogo">
                                                Upload Logo
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                                </label>
                                                <?php echo Form::file('upload_logo', array('class'=>"default")); ?>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <table role="presentation" class="table table-attached-files">
                                                    <tbody class="attached-files">
                                                        <?php if(isset($settings['user']->site_setting)&& !empty($settings['user']->site_setting->upload_logo)):?>
                                                        <tr>
                                                            <td><p class="name"><?php echo $settings['user']->site_setting->upload_logo; ?></p></td>
                                                            <td><i class="glyphicon glyphicon-remove-circle delete-upload-logo"></i></td>
                                                        </tr>
                                                        <?php endif;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <div class="col-md-6 col-xs-12">
                                                <label for="iFavIcon">Upload a Fav Icon <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i></label>
                                                <?php echo Form::file('upload_favicon', array('class'=>"default")); ?>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <table role="presentation" class="table table-attached-files">
                                                    <tbody class="attached-files">
                                                        <?php if(isset($settings['user']->site_setting)&& !empty($settings['user']->site_setting->upload_favicon)):?>
                                                        <tr>
                                                            <td><p class="name"><?php echo $settings['user']->site_setting->upload_favicon; ?></p></td>
                                                            <td><i class="glyphicon glyphicon-remove-circle delete-upload-favicon"></i></td>
                                                        </tr>
                                                        <?php endif;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <?php echo Form::submit('Save Changes',array('class'=>"btn btn-primary")); ?>
                                    <?php echo Form::token();?>
                                    <?php echo Form::hidden('project_id','');?>
                                    <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
                                </div>
                            </div>
                            <?php echo Form::close(); ?>
                        
                            <?php echo Form::open(array('action'=>'ProjectController@featureSetting', 'method'=>'post', 'files'=>true)); ?>
                            <div class="panel feature-settings toggle">
                                <div class="panel-heading">
                                    <h3>Feature Settings <i class="fa fa-chevron-right"></i></h3>
                                </div>
                                <div class="panel-body">
<!--                                    <div class="row">
                                        <div class="form-group col-md-3 col-sm-4">
                                            <label for="public_private">
                                                Default Public or Private
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <?php echo Form::select('public_private',array(
                                                'public'=>'Public',
                                                'private'=>'Private'                                                
                                            ), Input::old('public_private') ? 
                                                    Input::old('public_private') : 
                                                isset($settings['user']->feature_setting) ? 
                                                    $settings['user']->feature_setting->public_private : 'public' , array('class'=>"form-control")); ?>
                                            
                                        </div>
                                    </div>-->
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-8">
                                            <label>
                                                Visitor Actions
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <div class="checkbox">
                                                <label>
                                                    <?php echo Form::checkbox('show_search_box', 'yes', 
                                                            Input::old('show_search_box') ? 
                                                                ( Input::old('show_search_box') == 'yes' ? true : false ) : 
                                                            (isset($settings['user']->feature_setting->show_search_box) ? 
                                                                ($settings['user']->feature_setting->show_search_box=='yes' ? true : false ): true )); ?>&nbsp;Show Search Box
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <?php echo Form::checkbox('show_feature_request_box', 'yes', 
                                                            Input::old('show_feature_request_box') ? ( Input::old('show_feature_request_box') == 'yes' ? true : false ) : 
                                                                (isset($settings['user']->feature_setting->show_feature_request_box) ? 
                                                                    ($settings['user']->feature_setting->show_feature_request_box=='yes' ? true : false) : true )); ?>&nbsp;Show Feature Request Box
                                                </label>
                                            </div>

                                            <div class="checkbox">
                                                <label>
                                                    <?php echo Form::checkbox('show_download_pdf', 'yes', 
                                                            Input::old('show_download_pdf') ? ( Input::old('show_download_pdf') == 'yes' ? true : false ) : 
                                                                (isset($settings['user']->feature_setting->show_download_pdf) ? 
                                                                    ($settings['user']->feature_setting->show_download_pdf=='yes' ? true : false) : true )); ?>&nbsp;Show Download PDF
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <?php echo Form::checkbox('show_signup_email', 'yes', 
                                                            Input::old('show_signup_email') ? ( Input::old('show_signup_email') == 'yes' ? true : false ) : 
                                                            (isset($settings['user']->feature_setting->show_signup_email) ? 
                                                                    ($settings['user']->feature_setting->show_signup_email=='yes' ? true : false ) : true )); ?>&nbsp;Show Sign Up for Email Notifications
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php if(isset($settings['user']->icon_manager) && count($settings['user']->icon_manager)>0): ?>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-8">
                                            <label>
                                                Manage Custom Types
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            
                                            
                                            <?php foreach ($settings['user']->icon_manager as $icon): ?>
                                            
                                            
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <p class="custom-type">
                                                        <span style="<?php echo isset($icon->color) ? 'color:'.$icon->color : ''; ?>">
                                                            <?php if(isset($icon->icon) && !empty($icon->icon)): ?>
                                                            <i class="<?php echo $icon->icon;?>"></i>
                                                            <?php elseif (isset($icon->image) && !empty($icon->image)) : ?>
                                                            <img src="/files/users/<?php echo Auth::id(); ?>/<?php echo $icon->image; ?>" width='13' height='15'>
                                                            <?php endif; ?>
                                                            <?php echo $icon->title; ?>
                                                        </span>
                                                       
                                                        <a href="<?php echo action('ProjectController@getEditCustomType'); ?>" class="edit-custom-type" data-type_id="<?php echo $icon->id; ?>"><i class="fa fa-pencil"></i> edit</a> | <a href="javascript:;" class="text-danger delete-custom-type" data-type_id="<?php echo $icon->id; ?>"><i class="fa fa-times"></i> delete</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                            
                                            
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-8">
                                            <label>
                                                Add/Edit Type
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <?php echo Form::label('type_title', 'Name'); ?>
                                                    <?php echo Form::text('type_title', Input::old('title') ? Input::old('title') : '', array('class'=>"form-control")); ?>
                                                    <span class="checkbox inline">
                                                        <label>
                                                            <?php echo Form::checkbox('show_label', Input::old('show_label')=='show' ? 'show' : 'hide',  true, array()); ?>&nbsp;Show Name Label
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <label for="type_color">Color</label>
                                                    <?php echo Form::text('type_color', Input::old('type_color') ? Input::old('type_color') : '', array('class'=>"colorpicker colorpicker-default form-control")); ?>
                                                </div>
                                            </div>
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <label for="icon">Icon</label>
                                                    
                                                    <?php echo Form::hidden('icon', ''); ?>
                                                    <?php echo Form::hidden('icon_id',0);?>
                                                    <div class="btn-group select-icon icon-picker">
                                                        <a class="btn btn-default" href="#" style='background-color: transparent; border-color: #e2e2e4; color: #a9a9a9;'><span class='selected-icon'>Select Icon...</span>
                                                          <span class="fa fa-caret-down"></span></a>
<!--                                                        <ul class="dropdown-menu">
                                                          <li><a href="#">Select Icon...</a></li>
                                                          <li><a href="#" data-type="fa fa-plus-square"><i class='fa fa-plus-square'></i> Added</a></li>
                                                          <li><a href="#" data-type="fa fa-check-square"><i class='fa fa-check-square'></i> Fixed</a></li>
                                                          <li><a href="#" data-type="fa fa-minus-square"><i class='fa fa-minus-square'></i> Removed</a></li>

                                                        </ul>-->
                                                    </div>
                                                    
                                                    <span class="checkbox inline">
                                                        or
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <label>&nbsp;</label>
                                                    <?php echo Form::file('icon_file',array('class'=>"default inline-block")); ?>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo Form::submit('Save Changes', array('class'=>"btn btn-primary")) ?>
                                    <?php echo Form::hidden('project_id','');?>
                                    <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
                                    <?php echo Form::token(); ?>
                                </div>
                            </div>
                            <?php echo Form::close(); ?>

                            <?php echo Form::open(array('action' => 'ProjectController@layoutSetting', 'method'=>'post', 'files'=>true)); ?>
                            <div class="panel layout-settings toggle">
                                <div class="panel-heading">
                                    <h3>Layout &amp; Design Settings <i class="fa fa-chevron-right"></i></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-6 col-xs-12">
                                                <label for="header_image">
                                                Header Image
                                                <i class="fa fa-question-circle tip-none" title="" data-placement="right" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit."></i>
                                                </label>
                                                <?php echo Form::file('header_image',array('class'=>"default margin-bottom-10")); ?>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <table role="presentation" class="table table-attached-files">
                                                    <tbody class="attached-files">
                                                        <?php if(isset($settings['user']->layout_setting)&& !empty($settings['user']->layout_setting->header_image)):?>
                                                        <tr>
                                                            <td><p class="name"><?php echo $settings['user']->layout_setting->header_image; ?></p></td>
                                                            <td><i class="glyphicon glyphicon-remove-circle delete-header-image"></i></td>
                                                        </tr>
                                                        <?php endif;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-12 col-xs-12">
                                                <?php echo Form::text('header_background',  isset($settings['user']->layout_setting) ? $settings['user']->layout_setting->header_background : '' , array('class'=>"colorpicker-default form-control", 'placeholder'=>"Header background color")); ?>
                                            </div>
                                            <div class="col-md-12 col-xs-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <?php echo Form::checkbox('hide_header_area','hide', isset($settings['user']->layout_setting) ? ($settings['user']->layout_setting->hide_header_area == 'hide' ? true : false) : false, array()) ?>&nbsp;Hide Header Area
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-12 col-xs-12">
                                                <label for="header_text_color">
                                                Header Text
                                                <i class="fa fa-question-circle tip-none" title="" data-placement="right" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit."></i>
                                                </label>
                                                <?php echo Form::text('header_text_color',  !empty($settings['user']->layout_setting->header_text_color) ? $settings['user']->layout_setting->header_text_color : '' , array('class'=>"colorpicker-default form-control", 'placeholder'=>"Header text color")); ?>
                                            </div>
                                            <div class="col-md-12 col-xs-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <?php echo Form::checkbox('header_text_show','hide', !empty($settings['user']->layout_setting->header_text_show) ? ($settings['user']->layout_setting->header_text_show == 'hide' ? true : false) : false, array()) ?>&nbsp;Hide Header Text
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-6 col-xs-12">
                                                <label for="background_image">
                                                Background Image
                                                <i class="fa fa-question-circle tip-none" title="" data-placement="right" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit."></i>
                                                </label>
                                                <?php echo Form::file('background_image',array('class'=>"default margin-bottom-10")); ?>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <table role="presentation" class="table table-attached-files">
                                                    <tbody class="attached-files">
                                                        <?php if(isset($settings['user']->layout_setting)&& !empty($settings['user']->layout_setting->background_image)):?>
                                                        <tr>
                                                            <td><p class="name"><?php echo $settings['user']->layout_setting->background_image; ?></p></td>
                                                            <td><i class="glyphicon glyphicon-remove-circle delete-background-image"></i></td>
                                                        </tr>
                                                        <?php endif;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-12 col-xs-12">
                                                <?php echo Form::text('background_color', isset($settings['user']->layout_setting) ? $settings['user']->layout_setting->background_color : '', array('class'=>"colorpicker-default form-control", 'placeholder'=>"Background color")); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="header_code">
                                                Header include code
                                                <i class="fa fa-question-circle tip-none" title="" data-placement="right" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit."></i>
                                            </label>
                                            <?php echo Form::textarea('header_code',isset($settings['user']->layout_setting) ? $settings['user']->layout_setting->header_code : '',array('cols'=>"30", 'rows'=>"5", 'class'=>"form-control")); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="footer_code">
                                                Footer include code
                                                <i class="fa fa-question-circle tip-none" title="" data-placement="right" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit."></i>
                                            </label>
                                            <?php echo Form::textarea('footer_code',isset($settings['user']->layout_setting) ? $settings['user']->layout_setting->footer_code : '',array('cols'=>"30", 'rows'=>"5", 'class'=>"form-control")); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="custom_css">
                                                Custom CSS code
                                                <i class="fa fa-question-circle tip-none" title="" data-placement="right" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit."></i>
                                            </label>
                                            <?php echo Form::textarea('custom_css',isset($settings['user']->layout_setting) ? $settings['user']->layout_setting->custom_css : '',array('cols'=>"30", 'rows'=>"10", 'class'=>"form-control")); ?>
                                        </div>
                                    </div>
                                    <?php echo Form::submit('Save Changes', array('class'=>"btn btn-primary")) ?>
                                    <?php echo Form::hidden('project_id','');?>
                                    <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
                                    <?php echo Form::token(); ?>
                                </div>
                            </div>
                        <?php echo Form::close(); ?>

                        <?php echo Form::open(array('action'=>'ProjectController@subscribeUpdates', 'method'=>'post')); ?>
                            <div class="panel email-settings toggle">
                                <div class="panel-heading">
                                    <h3>Email Settings<i class="fa fa-chevron-right"></i></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-8">

                                            <a href="#automaticEmail" data-toggle="modal" class="btn btn-default">Schedule Automated Email to Subscribers</a>

                                        </div>
                                        <div class="form-group col-md-6 col-sm-8">

                                            <a href="#customizeEmail" data-toggle="modal" class="btn btn-default">Send Email Now to Subscribers</a>

                                        </div>
                                    </div>

                                    <?php if(isset($settings['subscribes']) && count($settings['subscribes'])>0): ?>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label>
                                                Manage Subscribes
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            
                                            <?php foreach ($settings['subscribes'] as $subscribe): ?>
                                            
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <p class="custom-type">
                                                        <span class="">
                                                            <?php echo $subscribe->email; ?>
                                                        </span>
                                                        <span class="">
                                                            <?php echo  (isset($subscribe->project->title)? $subscribe->project->title:''); ?>
                                                        </span>
                                                        <span class="">
                                                            <a href="javascript:;" class="edit-subscribe-updates" data-type_id="<?php echo $subscribe->id; ?>"><i class="fa fa-pencil"></i> edit</a> | <a href="javascript:;" class="text-danger delete-subscribe-updates" data-type_id="<?php echo $subscribe->id; ?>"><i class="fa fa-times"></i> delete</a>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                            
                                            
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-8">
                                            <label>
                                                Add/Edit Subscribe
                                                <i class="fa fa-question-circle tip-none" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." data-placement="right"></i>
                                            </label>
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <?php echo Form::label('email', 'Email'); ?>
                                                    <?php echo Form::text('email', Input::old('email') ? Input::old('email') : '', array('class'=>"form-control")); ?>
                                                </div>
                                            </div>
                                            <div class="row form-group form-inline">
                                                <div class="col-sm-12">
                                                    <?php echo Form::label('project_id', 'Project'); ?>
                                                    <?php echo Form::select('project_id', $settings['projects'], Input::old('project_id') ? Input::old('project_id') : '',  array('class'=>"form-control")); ?>
                                                    <?php // echo Form::text('project_id', Input::old('project_id') ? Input::old('project_id') : '', array('class'=>"form-control")); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo Form::submit('Save Changes', array('class'=>"btn btn-primary")) ?>
                                    <?php echo Form::hidden('user_id','');?>
                                    <?php echo Form::hidden('subscription_id','');?>
                                    <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
                                    <?php // echo Form::token(); ?>
                                </div>
                            </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>

                
            </div>
        </section>
    </section>
    

<div aria-hidden="true" role="dialog" tabindex="-1" id="deleteCustomtype" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>



            <?php echo Form::open(array('action' => 'ProjectController@deleteCustomType', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your custom type. Do you really want to do this?</p>
                <?php echo Form::hidden('type_id', ''); ?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
                <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="delete-subscribe-updates" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>



            <?php echo Form::open(array('action' => 'ProjectController@deleteSubscribeUpdates', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete the email subscriber. Do you really want to do this?</p>
                <?php echo Form::hidden('type_id', ''); ?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
                <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="automaticEmail" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Schedule Automated Email to Subscribers</h4>
            </div>



            <?php echo Form::open(array('action' => 'ProjectController@automaticEmail', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>Please set the message for sending for your subscribers</p>
                <div class="row form-group form-inline">
                    <div class="col-sm-12">
                        <?php //echo Form::label('type_title', 'Name'); ?>
                        <?php //echo Form::text('type_title', Input::old('title') ? Input::old('title') : '', array('class'=>"form-control")); ?>
                        <span class="checkbox inline">
                            <label> 
                                <?php echo Form::checkbox('send_automatic', 'yes',
                                        empty($settings['user']->customize_email->send_automatic) || (!empty($settings['user']->customize_email->send_automatic) && 
                            ($settings['user']->customize_email->send_automatic == 'yes')) ? true : false, array()); ?>&nbsp;Enable Automated Emails
                            </label>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                    <?php echo Form::label('subject', 'Subject'); ?>
                    <?php echo Form::text('subject',  
                            (isset($settings['user']->customize_email) && 
                            !empty($settings['user']->customize_email->subject)) ? 
                            $settings['user']->customize_email->subject : '',array('class'=>"form-control"));?>
                        </div>
                    </div>
                </div>
<?php /*/ ?>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php echo Form::label('mess_body', 'Body'); ?>
                            <?php echo Form::textarea('mess_body',
                                    (isset($settings['user']->customize_email) && 
                            !empty($settings['user']->customize_email->body)) ? 
                            $settings['user']->customize_email->body : '',array('class'=>"form-control", 'cols'=>10, 'rows'=>2)) ?>
                        </div>
                    </div>
                </div>
<?php /*/ ?>
                 <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="bootstrap-timepicker" style="width: 100%; position: relative;top: 100%;height: 70px;">   
                                <?php echo Form::label('send', 'Set Email Sending Time'); ?>
                                <?php echo Form::text('send',
                                        (isset($settings['user']->customize_email) && 
                            !empty($settings['user']->customize_email->send)) ? 
                            Carbon::createFromFormat('H:i:s',$settings['user']->customize_email->send,date_default_timezone_get())
                                        ->setTimezone(!empty($settings['user']->localization->time_zone)?$settings['user']->localization->time_zone:date_default_timezone_get())->toTimeString() : '',array('class'=>"form-control timepicker")) ?>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Submit', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
                <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="customizeEmail" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Send Email Now to Subscribers</h4>
            </div>



            <?php echo Form::open(array('action' => 'ProjectController@customizeEmail', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>Please set the message for sending for your subscribers</p>
                <div class="row form-group">
                    <div class="col-sm-12">
                        <?php echo Form::label('project_id', 'Project');?>
                        <?php echo Form::select('project_id', $settings['projectsAll'], Input::old('project_id') ? Input::old('project_id') : '',  array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                    <?php echo Form::label('subject', 'Subject'); ?>
                    <?php echo Form::text('subject', '',array('class'=>"form-control"));?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php echo Form::label('mess_body', 'Body'); ?>
                            <?php echo Form::textarea('mess_body','',array('class'=>"form-control", 'cols'=>10, 'rows'=>2)) ?>
                        </div>
                    </div>
                </div>
<?php /*/ ?>                
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="bootstrap-timepicker" style="width: 100%; position: relative;top: 100%;height: 70px;">   
                                <?php echo Form::label('send', 'Set Email Sending Time'); ?>
                                <?php echo Form::text('send',
                                        (isset($settings['user']->customize_email) && 
                            !empty($settings['user']->customize_email->send)) ? 
                            Carbon::createFromFormat('H:i:s',$settings['user']->customize_email->send,date_default_timezone_get())->setTimezone($settings['user']->localization->time_zone)->toTimeString() : '',array('class'=>"form-control timepicker")) ?>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
<?php /*/ ?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Submit', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
                <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="DeleteUploadLogo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'ProjectController@deleteUploadLogo', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your logo. Do you really want to do this?</p>
                
                <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="DeleteUploadFavicon" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'ProjectController@deleteUploadFavicon', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your favicon. Do you really want to do this?</p>
                <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="DeleteBackgroundImage" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'ProjectController@deleteBackgroundImage', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your background image. Do you really want to do this?</p>
               <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="DeleteHeaderImage" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'ProjectController@deleteHeaderImage', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your header image. Do you really want to do this?</p>
                <?php echo Form::hidden('slug', $settings['user']->company->slug);?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<script type="text/javascript">
    jQuery( document ).ready(function($) {
        $('.select-icon > a').click(function(e){
            e.preventDefault();
        });
        $('.delete-custom-type').click(function(e){
            e.preventDefault();
            var type_id = $(this).data('type_id');
            $('#deleteCustomtype').find('input[name="type_id"]').val(type_id);
            $('#deleteCustomtype').modal();
        });
        
        $('.edit-custom-type').click(function(e){
            e.preventDefault();
            $.post('/edit-custom-type', {type_id: $(this).data('type_id'), _token: $(this).closest('form').find('input[name="_token"]').val() }, function(data){
                $('input[name="type_color"]').val(data.color);
                $('input[name="icon"]').val(data.icon);
                $('.selected-icon').html('<i class="'+data.icon+'"></i>');
                $('input[name="icon_id"]').val(data.id);
                $('input[name="type_title"]').val(data.title);
                $('input[name="show_label"]').prop('checked', (data.show_label == 'hide') ? false : true);
            }, 'json');
        });
        
        $('.delete-subscribe-updates').click(function(e){
            e.preventDefault();
            var type_id = $(this).data('type_id');
            $('#delete-subscribe-updates').find('input[name="type_id"]').val(type_id);
            $('#delete-subscribe-updates').modal();
        });
        
        $('.edit-subscribe-updates').click(function(e){
            e.preventDefault();
            $.post('/get-subscribe-updates', {type_id: $(this).data('type_id'), _token: $(this).closest('form').find('input[name="_token"]').val() }, function(data){
                $('input[name="subscription_id"]').val(data.id);
                $('select[name="project_id"]').val(data.project_id);
                $('input[name="email"]').val(data.email);
                $('input[name="user_id"]').val(data.user_id);
            }, 'json');
        });
        
        $('.timepicker').timepicker({
            template: 'dropdown',
            defaultTime: 'current',
            showMeridian: false
        });
        
        var p = window.location.hash.substring(1);
   
        if(p.length>0){
            $('.toggle').removeClass('active');
            $('.'+p).addClass('active');

        }
        
        $('.delete-upload-logo').click(function(){
            $('#DeleteUploadLogo').modal();
        });
        $('.delete-upload-favicon').click(function(){
            $('#DeleteUploadFavicon').modal();
        });
        $('.delete-background-image').click(function(){
           $('#DeleteBackgroundImage').modal();
        });
        $('.delete-header-image').click(function(){
            $('#DeleteHeaderImage').modal();
        });
        
        $(".icon-picker").iconPickerCustom();

    });
    
(function($) {

    $.fn.iconPickerCustom = function( options ) {
        
        var mouseOver=false;
        var $popup=null;
        var icons=new Array("adjust","align-center","align-justify","align-left","align-right","arrow-down","arrow-left","arrow-right","arrow-up","asterisk","backward","ban-circle","barcode","bell","bold","book","bookmark","briefcase","bullhorn","calendar","camera","certificate","check","chevron-down","chevron-left","chevron-right","chevron-up","circle-arrow-down","circle-arrow-left","circle-arrow-right","circle-arrow-up","cloud","cloud-download","cloud-upload","cog","collapse-down","collapse-up","comment","compressed","copyright-mark","credit-card","cutlery","dashboard","download","download-alt","earphone","edit","eject","envelope","euro","exclamation-sign","expand","export","eye-close","eye-open","facetime-video","fast-backward","fast-forward","file","film","filter","fire","flag","flash","floppy-disk","floppy-open","floppy-remove","floppy-save","floppy-saved","folder-close","folder-open","font","forward","fullscreen","gbp","gift","glass","globe","hand-down","hand-left","hand-right","hand-up","hd-video","hdd","header","headphones","heart","heart-empty","home","import","inbox","indent-left","indent-right","info-sign","italic","leaf","link","list","list-alt","lock","log-in","log-out","magnet","map-marker","minus","minus-sign","move","music","new-window","off","ok","ok-circle","ok-sign","open","paperclip","pause","pencil","phone","phone-alt","picture","plane","play","play-circle","plus","plus-sign","print","pushpin","qrcode","question-sign","random","record","refresh","registration-mark","remove","remove-circle","remove-sign","repeat","resize-full","resize-horizontal","resize-small","resize-vertical","retweet","road","save","saved","screenshot","sd-video","search","send","share","share-alt","shopping-cart","signal","sort","sort-by-alphabet","sort-by-alphabet-alt","sort-by-attributes","sort-by-attributes-alt","sort-by-order","sort-by-order-alt","sound-5-1","sound-6-1","sound-7-1","sound-dolby","sound-stereo","star","star-empty","stats","step-backward","step-forward","stop","subtitles","tag","tags","tasks","text-height","text-width","th","th-large","th-list","thumbs-down","thumbs-up","time","tint","tower","transfer","trash","tree-conifer","tree-deciduous","unchecked","upload","usd","user","volume-down","volume-off","volume-up","warning-sign","wrench","zoom-in","zoom-out");
        var settings = $.extend({
        	
        }, options);
        return this.each( function() {
                element=this;
            if(!settings.buttonOnly && $(this).data("iconPickerCustom")==undefined ){
                $this=$(this); //.addClass("form-control");
//                $wraper=$("<div/>",{class:"input-group"});
//                $this.wrap($wraper);

//                $button=$("<span class=\"input-group-addon pointer\"><i class=\"glyphicon  glyphicon-picture\"></i></span>");
                $button=$(".icon-picker > a > span");
//                $this.after($button);
                (function(ele){
                        $button.click(function(){
                                        createUI(ele);
                                        showList(ele,icons);
                        });
                    })($this);

                $(this).data("iconPickerCustom",{attached:true});
            }
        
                function createUI($element){
                        $popup=$('<div/>',{
                                css: {
                                        'top':$element.offset().top+$element.outerHeight()+6,
                                        'left':$element.offset().left
                                },
                                class:'icon-popup'
                        })

                        $popup.html('<div class="ip-control"> \
                                                          <ul> \
                                                            <li><a href="javascript:;" class="btn" data-dir="-1"><span class="glyphicon  glyphicon-fast-backward"></span></a></li> \
                                                            <li><input type="text" class="ip-search glyphicon  glyphicon-search" placeholder="Search" /></li> \
                                                            <li><a href="javascript:;"  class="btn" data-dir="1"><span class="glyphicon  glyphicon-fast-forward"></span></a></li> \
                                                          </ul> \
                                                      </div> \
                                                     <div class="icon-list"> </div> \
                                                 ').appendTo("body");
	        	
	        	
                        $popup.addClass('dropdown-menu').show();
                                $popup.mouseenter(function() {  mouseOver=true;  }).mouseleave(function() { mouseOver=false;  });

                        var lastVal="", start_index=0,per_page=30,end_index=start_index+per_page;
                        $(".ip-control .btn",$popup).click(function(e){
                        e.stopPropagation();
                        var dir=$(this).attr("data-dir");
                        start_index=start_index+per_page*dir;
                        start_index=start_index<0?0:start_index;
                        if(start_index+per_page<=210){
                          $.each($(".icon-list>ul li"),function(i){
                              if(i>=start_index && i<start_index+per_page){
                                 $(this).show();
                              }else{
                                $(this).hide();
                              }
                          });
                        }else{
                          start_index=180;
                        }
                    });
	        	
                        $('.ip-control .ip-search',$popup).on("keyup",function(e){
                        if(lastVal!=$(this).val()){
                            lastVal=$(this).val();
                            if(lastVal==""){
                                showList(icons);
                            }else{
                                showList($element, $(icons)
                                                                .map(function(i,v){ 
                                                                            if(v.toLowerCase().indexOf(lastVal.toLowerCase())!=-1){return v} 
                                                                        }).get());
                                                }
	                    
                        }
                    });  
                        $(document).mouseup(function (e){
                                    if (!$popup.is(e.target) && $popup.has(e.target).length === 0) {
                                        removeInstance();
                                    }
                                });

                }
                function removeInstance(){
                        $(".icon-popup").remove();
                }
                function showList($element,arrLis){
                        $ul=$("<ul>");
	        	
                        for (var i in arrLis) {
                                $ul.append("<li><a href=\"#\" title="+arrLis[i]+"><span class=\"glyphicon  glyphicon-"+arrLis[i]+"\"></span></a></li>");
                        };

                        $(".icon-list",$popup).html($ul);
                        $(".icon-list li a",$popup).click(function(e){
                                e.preventDefault();
                                var title=$(this).attr("title");
//                                $element.val("glyphicon glyphicon-"+title);
                                $('.select-icon > a').find('.selected-icon').html('<i class="'+"glyphicon glyphicon-"+title+'"></i>');
                                $('input[name="icon"]').val("glyphicon glyphicon-"+title);
                                removeInstance();
                        });
                }

        });
    }

}(jQuery));
</script>



