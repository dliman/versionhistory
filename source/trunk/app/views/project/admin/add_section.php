    <section id="main-content">
        
        <section class="wrapper">
            <div class="container">
                <!-- page start-->
                
                <div class="row">
                    <div class="col-md-12">

                        <h4><a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project->slug)); ?>"><i class="fa fa-chevron-left"></i> <?php echo $project->title; ?> </a></h4>
                        <?php /* if ($errors->any()) : ?>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <?php echo implode('', $errors->all(':message<br>')); ?>
                                </div>
                            </div>
                        </div>
                        <?php endif */ ?>
                        <div class="panel add-release-notes">
                            <div class="panel-body">
                                <?php echo Form::open(array(
                                    'action'=>'ProjectController@addSection', 
                                    'method'=>'post', 
                                    'id' => 'fileupload',
                                    'enctype' => "multipart/form-data")); ?>
                                
                                    <div class="row form-group">

                                        <div class="col-sm-12 col-xs-12 col-md-8">
                                            <p>Version Name</p>
                                            <?php echo Form::text(
                                                    'title', 
                                                    Input::old('title')?Input::old('title'):'', 
                                                    array(
                                                        'class'=>'form-control title', 
                                                        'placeholder'=>'Version Name'
                                                        )); ?>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 col-md-4">
                                            <p>Version Date</p>
                                            <?php echo Form::text(
                                                    'released_at', 
                                                    Input::old('released_at')?Input::old('released_at'):date('m-d-Y'), 
                                                    array(
                                                        'class'=>'form-control default-date-picker', 
                                                        'placeholder'=>'Version Date'
                                                        )); ?>
                                        </div>

                                        
                                    </div>
                                    <p>Version Description (optional)</p>
                                    <div class="form-group">
                                        <?php echo Form::textarea(
                                                'editor1',
                                                Input::old('description')?Input::old('description'):'',
                                                array('class'=>'form-control ckeditor', 'rows'=>'6' )) ?>
                                        
                                    </div>
                                    <div class="trakingRedactor" style='display:none'></div>
                                    <div class="row fileupload-buttonbar form-button-block">
                                        <div class="col-lg-12">
                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                            <span class="btn btn-success fileinput-button attach-file">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                <span>Add Attachment</span>
                                                <input type="file" name="files[]" multiple>
                                            </span>


                                        </div>
                                        <!-- The global progress state -->
                                        <div class="col-lg-12 fileupload-progress fade">
                                            <!-- The global progress bar -->
                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                            </div>
                                            <!-- The extended global progress state -->
                                            <div class="progress-extended">&nbsp;</div>
                                        </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-upload-files"><tbody class="files"></tbody></table>
                                </div>
                                <div class="clearfix"></div>
                                    <div class="col-mg-12 fileupload-buttonbar form-button-block">
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <?php echo Form::submit('Save',array('class'=>"start btn btn-success")); ?>
                                        <?php // echo Form::button('Cancel',array('class'=>"cancel btn btn-default", 'id'=>'cancel_form',  'type'=>'reset')); ?>
                                        <a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project->slug)); ?>" class="btn btn-default">Cancel</a>
                                        <?php echo Form::hidden('project_id', $project->id, array('class'=>'project_id'));?>
                                        <?php echo Form::hidden('slug', $slug, array('class'=>'slug'));?>
                                        <?php echo Form::hidden('project', $project->slug, array('class'=>'project'));?>
                                        <?php echo Form::hidden('company_id', $project->company_id, array('class'=>'company_id'));?>
                                        
                                        <?php echo Form::token();?>
                                    </div>
                                <?php echo Form::close(); ?>
                            </div>
                        </div>


                    </div>
                </div>

                
            </div>
        </section>
    </section>
    
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <p class="name">{%=file.name%}</p>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}

    <i class="glyphicon glyphicon-upload start"></i>


    {% } %}
    {% if (!i) { %}

    <i class="glyphicon glyphicon-remove-circle cancel"></i>

    {% } %}
    </td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <p class="name">{%=file.name%}</p>
    </td>
    
    <td>
    
    <i class="glyphicon glyphicon-remove-circle cancel"></i>
    
    </td>
    </tr>
    {% } %}

</script>
<script type="text/javascript">
     jQuery( document ).ready(function() {
        jQuery('.default-date-picker').bootstrapDP({
            format: 'mm-dd-yyyy'
        }); 
        
        CKEDITOR.replace('editor1');
        var editor_text;
        function updateTrakingRedactor(){
            editor_text = CKEDITOR.instances.editor1.getData();
            jQuery('.trakingRedactor').html(editor_text);
        }
        timer = setInterval( function(){ 
            updateTrakingRedactor();
        },100);
        'use strict';

        // Initialize the jQuery File Upload widget:
        jQuery('#fileupload').fileupload();

        // Enable iframe cross-domain access via redirect option:
        jQuery('#fileupload').fileupload(
            'option',
            {
                url:'/section',
                complete: function (e, data) {
                    if(data == 'error' || data == 'parsererror') {
                        window.location.href = window.location.origin+'/admin/'+jQuery('.slug').val()+'/'+jQuery('.project').val();
                    } else {
                        setTimeout(function(){jQuery('.form-button-block').find('.start').click()}, 500);
                    }
                }
            }
        ).on('fileuploadsubmit', function (e, data) {
            data.formData = {
                'title': jQuery('.title').val(),
                'editor1': jQuery('.trakingRedactor').html(),
                'project_id': jQuery('.project_id').val(),
                'slug': jQuery('.slug').val(),
                'released_at': jQuery('input[name="released_at"]').val(),
                'company_id':jQuery('.company_id').val(),
                '_token' : jQuery('.fileupload-buttonbar').find('input[name="_token"]').val()
            };
        });

        jQuery('.form-button-block').find('.start').click(function(){

            var table_files_upload = jQuery('.table-upload-files').find('.template-upload').size();
            var table_files_download = jQuery('.table-upload-files').find('.template-download').size();
            if( (table_files_upload == 0) || (table_files_download > 0) ){
                jQuery('#fileupload').submit();
            }
        });           

        jQuery('.table-upload-files').on('click', '.template-download .cancel', function(){
            var f_name = jQuery(this).closest('.template-download').find('.name').text();
            var token = jQuery('.fileupload-buttonbar').find('input[name="_token"]').val();
            var note_id = jQuery('.fileupload-buttonbar').find('input[name="note_id"]').val();
            var flag = 1;
            jQuery.post('/delete-note-attachment',{flag: flag, note_id: note_id, f_name: f_name, _token: token },function(data){

            });
        });

        jQuery('.table-attached-files').on('click', '.cancel', function(){
            var attach = jQuery(this).data('attach');
            var f_name = jQuery('p[data-attach="'+attach+'"]').text();
            var token = jQuery('.fileupload-buttonbar').find('input[name="_token"]').val();
            var flag = 2;
            jQuery.post('/delete-note-attachment',{flag: flag, attach: attach, f_name: f_name, _token: token },function(data){
                jQuery('.table-attached-files').find('.cancel').each(function(){
                    if(jQuery(this).attr('data-attach') == data){
                        jQuery(this).closest('tr').remove();
                    }
                });
            });

        });
      

    });
</script>