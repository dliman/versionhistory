<section id="main-content">
          <section class="wrapper">
            <div class="container">
			  <!-- page start-->
			  
			  <div class="row">
				
			  	<div class="col-md-12">
                                    <h4><a href="<?php echo action('ProjectController@project',array($slug, $project)); ?>"><i class="fa fa-chevron-left"></i> <?php echo $project_title;?></a></h4>

                                    <h2>Feature Requests</h2>
                                    <?php if(isset($notes[0]) && !empty($notes[0]->id) || true): ?>
                                    <div class="sort-by row">
                                            <div class="col-sm-4">
                                                <?php echo Form::open(array(
                                                    'action'=>'ProjectController@postFeatureRequest', 
                                                    'method'=>'post', 
                                                    'class'=>'sorting-form'
                                                    )); ?>
                                                <?php echo Form::select('select-sort', array(
                                                    '1'=>'Sort by Submit Date (Newest to Oldest)',
                                                    '2'=>'Sort by Submit Date (Oldest to Newest)',
                                                    '3'=>'Sort by Type',
                                                    '4'=>'Show Hidden Requests'
                                                    ), $select_sort, array('class'=>'form-control select-sort') ); ?>
                                                    
                                                
                                                <?php echo Form::hidden('slug', $slug); ?>
                                                <?php echo Form::hidden('project', $project); ?>
                                                <?php echo Form::token(); ?>
                                                <?php echo Form::close(); ?>
                                            </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="row result-notes-features">
                                        <div class="col-md-12">
                                          
                                            <?php if(isset($notes[0]) && !empty($notes[0]->id)): ?>
                                                
                                                <?php foreach ($notes as $note) : 
                                                    $note->type = isset($settings['feature_type'][$note->type_id])?$settings['feature_type'][$note->type_id]:$settings['feature_type'][0];
                                                ?>

                                                <div class="panel search-result">
                                                        <div class="panel-body">
                                                                <h3 style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>">
                                                                    <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                                                        <i class="<?php echo $note->type->icon; ?>"></i>
                                                                    <?php elseif (isset($note->type->image) && !empty($note->type->image)): ?>
                                                                        <img src="/files/users/<?php echo Auth::id(); ?>/<?php echo $note->type->image; ?>" width='13' height='15'>
                                                                    <?php endif; ?>
                                                                    &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                                                </h3>
                                                            <p><?php echo strip_tags($note->email); ?></p>
                                                            <p><?php echo strip_tags($note->description); ?></p>
                                                            <div class="row">
                                                                <div class="updated text-primary col-sm-10"><i class="fa fa-calendar"></i> Submitted <?php echo date('M j, Y', strtotime($note->updated_at)); ?></div>
                                                                <div class="updated text-primary col-sm-2">
                                                                    <?php if(empty($note->hidden)) : ?>
                                                                    <a href="javascript:;" class="custom-request-hide pull-right" data-id="<?php echo $note->id;?>"> Hide </a>
                                                                    <?php else : ?>
                                                                    <span class="pull-right"> Hidden </span>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>


                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <div class="panel search-result">
                                                        <div class="panel-body">
                                                                    <p>No features found</p>
                                                        </div>
                                                </div> 
                                            <?php endif; ?>
                                        </div>
                                    </div>
				</div>
			  </div>
			  
              
                </div>
          </section>
      </section>
     
<script type="text/javascript">
    jQuery( document ).ready(function() {
    
        jQuery('.select-sort').change(function(){
            jQuery('.sorting-form').submit();
        });

        jQuery('.custom-request-hide').click(function(event){
            event.preventDefault();
            var requestId = jQuery(this).attr('data-id');
            var block = jQuery(this).parents('.panel.search-result');
            jQuery.ajax({
                type: "POST",
                url: '/feature-request-hide',
                data: { _token: "<?php echo Session::token();?>",request: requestId },
                success: function( ) {
                    block.hide(); 
                }
            }).error(function( msg ) {
                alert( msg );
            });
        });

    });
</script>

    