<section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->

                <div class="row">
                    <div class="col-md-12 projects">

                        <div class="panel add-new">
                            <div class="panel-body">
                                <?php if($addProject) : ?>
                                <?php echo Form::open(array('action' => 'ProjectController@addProject', 'method' => 'post', 'class' => 'form-horizontal')); ?>


                                <div class="form-group">
                                    <?php echo Form::label('title', 'Project Name', array('class' => 'col-sm-3 control-label')); ?>
                                    <?php // echo $errors->has('title') ? $errors->first('title') : ''; ?> 

                                    <div class="col-sm-6">
                                        <?php echo Form::text('title', '', array('placeholder' => '', 'class' => 'form-control')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php
                                        echo Form::select('status', array(
                                            'public' => 'Public',
                                            'private' => 'Private'
                                                ), 'public', array('class' => 'form-control'));
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo Form::label('description', 'Description (optional)', array('class' => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php echo Form::text('description', '', array('class' => 'form-control')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo Form::submit('Save', array('class' => 'btn btn-primary btn-shadow btn-block')); ?>
                                        <?php echo Form::token(); ?>
                                    </div>
                                </div>

                                <?php echo Form::close(); ?>
                                <?php else : ?>
                                You have reached the limit of the pricing plan
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if ($projects): ?>
                            <?php foreach ($projects as $project): ?>
                                <div class="panel project-list <?php if(is_null($project->inactived_at)) { echo 'active'; } ?>" id="projects_<?php echo $project->id;?>">
                                    <div class="panel-body">

                                        <div class="project">
                                            <h3>
                                                <a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project->slug)); ?>"><?php echo $project->title; ?></a>
                                                <?php if(!is_null($project->inactived_at)) {
                                                    echo ' <span class="label label-warning">inactive</span>';
                                                } else { ?>
                                                <?php
                                                    echo $project->status == 'public' ?
                                                            '<span class="label label-success">public</span>' :
                                                            '<span class="label label-danger">private</span>';
                                                }
                                                ?>

                                            </h3>
                                            <p><?php echo $project->description; ?></p>
                                            <?php $updated_at = strtotime($project->updated_at); ?>
                                            <div class="updated ">
                                                <i class="fa fa-calendar"></i> Last updated <?php 
                                                $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'M j, Y';
                                                echo date($date_format, $updated_at); ?></div>
                                        </div>





                                    </div>
                                </div>
                            <?php endforeach; ?> 
                        <?php endif; ?>
                    </div>
                </div>

               
            </div>
        </section>
    </section>

<script type="text/javascript">
    jQuery.noConflict();

    var DraggableProjects = function () {

        return {

            init: function () {

                if (!jQuery().sortable) {
                    return;
                }

                jQuery(".projects").sortable({
                    connectWith: ".project-list.active",
                    items: ".project-list.active",
                    opacity: 0.8,
                    coneHelperSize: true,
                    placeholder: 'sortable-box-placeholder round-all',
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    update: function( event, ui ) { DraggableProjects.save(event.target) }
                });

                jQuery(".project-list").disableSelection();

            },
            save: function (target) {
                var sortedIDs = jQuery( target ).sortable('toArray', {attribute: 'id'}).map(function(x){return x.replace(/projects\_/g, '');});
                jQuery.ajax({
                    type: "POST",
                    url: '/order-project',
                    data: { _token: "<?php echo Session::token();?>",order:sortedIDs }
                }).error(function( msg ) {
                    alert( msg );
                });
            }

        };

    }();

    jQuery(document).ready(function($) {
            DraggableProjects.init();
    });

</script>