    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel project-list">
                            <div class="panel-body">
                                <h3>
                                    <a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project->slug)); ?>"><?php echo $project->title; ?></a>
                                                <?php if(!is_null($project->inactived_at)) {
                                                    echo ' <span class="label label-warning">inactive</span>';
                                                } else { ?>                                    
                                    <?php echo $project->status == 'public' ?
                                            '<span class="label label-success">public</span>' :
                                            '<span class="label label-danger">private</span>'; 
                                                }
                                    ?>
                                    
                                </h3>


                                <p class="desc"><?php echo $project->description; ?></p>
                                <div align="right">
                                    <?php if(is_null($project->inactived_at)) : ?>
                                    <a href="<?php echo action('ProjectController@getEditProject', array('slug'=>$slug, 'project' => $project->slug)); ?>"><i class="fa fa-pencil"></i> edit</a> &nbsp;
                                    <?php endif; ?>
                                    <a href="#myModal"  class="text-danger" data-toggle="modal"><i class="fa fa-times"></i> delete</a>
                                </div>
                                <div class="actions">

                                </div>
                            </div>
                        </div>

                        <div class="panel project-info">
                            <div class="panel-body">
                                <?php if(is_null($project->inactived_at)) : ?>
                                <a href="<?php echo action('ProjectController@getAddSection', array('slug'=>$slug, 'project' => $project->slug));?>" class="btn btn-primary">
                                    <i class="fa fa-plus"></i> Add New Version</a>
                                    <?php if($project->status == 'public') : ?>
                                <a href="<?php echo !empty($settings['project_setting']->custom_domain)? 
                                'http://'. $settings['project_setting']->custom_domain . '/' . $project->slug
                                 : url("/{$slug}/{$project->slug}", array(), false); /*action('ClientController@project', array('slug'=>$slug, 'project' => $project->slug));*/ 
                                            ?>" class="btn btn-primary" target="_blank"><i class="fa fa-eye"></i> View Live Site </a>
                                    <?php endif; ?>
                                
                                <div style='display: inline-block'>
                                    <?php echo Form::open(array('action'=>'ProjectController@changeProjectStatus','method'=>'post')); ?>
                                    <?php echo Form::hidden('slug',$slug);?>
                                    <?php echo Form::hidden('project',$project->slug);?>
                                    <?php echo Form::token();?>
                                    <button type='submit' class="btn btn-primary">
                                        <i class="fa fa-check"></i> <?php echo $project->status == 'private'? 'Publish':'Unpublish'?>
                                    </button>
                                    <?php echo Form::close();?>
                                </div>
                                <?php endif; ?>

                                <span class="pull-right">
                                <?php if($countArchived) : ?>
                                <a href="<?php echo action(($archived == 'no')?'ProjectController@projectArchivedSections':'ProjectController@project', array('slug'=>$slug, 'project' => $project->slug));?>" class="btn btn-default ">
                                    <i class="fa fa-book"></i> <?php echo ($archived == 'yes')?'Show active':'Show archived'; ?></a>
                                <?php endif; ?>

                                <a href="<?php echo action('ProjectController@featureRequest', array('slug'=>$slug, 'project' => $project->slug));?>" class="btn btn-default">
                                    <i class="fa fa-flag"></i> View Feature Requests</a>
                                </span>
                            </div>
                        </div>
                       
                        <?php if($project->section): ?>
                        <div class="versions">
                        <?php foreach ($project->section as $section): ?>
                        
                        <div class="panel version"  id="versions_<?php echo $section->id;?>">
                            <div class="panel-body">
                                <h3><?php echo $section->title; ?> 
                                    <small><?php 
                                $date_format = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'M j, Y';
                                echo date($date_format, strtotime($section->released_at )); ?>
                                <?php if($section->archived == 'yes') : ?> <span class="label label-default">archived</span> <?php endif; ?>
                                    </small>
                                    <?php if(is_null($project->inactived_at)) : ?>
                                    <span class="section-edit-delete pull-right">
                                    <a href="<?php echo action('ProjectController@getEditSection', array('slug'=>$slug, 'project' => $project->slug, 'section'=>$section->slug)); ?>"><i class="fa fa-pencil"></i> edit</a> &nbsp;
                                    <a href="delete-section" class="text-danger section-delete" data-section="<?php echo $section->id; ?>" ><i class="fa fa-times"></i> delete</a>
                                    </span>
                                    <?php endif; ?>
                                </h3>
                                <p><?php echo strip_tags($section->description); ?></p>
                                <?php if($section->note): ?>
                                <ul class="changes">
                                    <?php foreach ($section->note as $note): ?>
                                   
                                    <li class="notes" style="<?php if(isset($note->type->color) ) echo 'color:' .$note->type->color . ';'; ?>" id="notes_<?php echo $note->id;?>">
                                        <?php if(isset($note->type->icon) && !empty($note->type->icon)): ?>
                                        <i class="<?php echo $note->type->icon;?>"></i>
                                        <?php elseif (isset($note->type->image) && !empty($note->type->image)) : ?>
                                        <img src="/files/users/<?php echo Auth::id(); ?>/<?php echo $note->type->image; ?>" width='13' height='15'>
                                        <?php endif; ?>
                                        &nbsp;<?php echo (isset($note->type->show_label) && $note->type->show_label=='show' && isset($note->type->title))?$note->type->title.':':'';  ?>&nbsp;
                                        <?php echo strip_tags($note->title); ?>&nbsp;
                                        <?php 
                                        $date_format1 = (isset($settings['localization']->date_format) && !empty($settings['localization']->date_format)) ? $settings['localization']->date_format : 'm-d-Y';
                                        // echo '(' . date($date_format1, strtotime($note->updated_at)) . ')';?>&nbsp;
                                        <?php if(is_null($project->inactived_at)) : ?>
                                        <span class="actions">
                                            <a href="<?php echo action('ProjectController@getEditNote', array('slug'=>$slug, 'project' => $project->slug, 'section'=>$section->slug, 'note'=>$note->slug)); ?>"><i class="fa fa-pencil"></i> edit</a>
                                            <a href="<?php echo action('ProjectController@deleteNote'); ?>" data-note="<?php echo $note->id; ?>" class="text-danger note-delete"><i class="fa fa-times"></i> delete</a>
                                        </span>
                                        <?php endif; ?>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                                <?php endif; ?>
                                <?php if(is_null($project->inactived_at)) : ?>
                                <a href="<?php echo action('ProjectController@getAddNote', array('slug'=>$slug, 'project' => $project->slug, 'section'=>$section->slug)); ?>" class="btn btn-primary">
                                    <i class="fa fa-plus"></i> Add  Note</a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        </div>
                       <?php endif; ?>

                    </div>
                </div>

                
             </div>
        </section>
    </section>


    
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>



            <?php echo Form::open(array('action' => 'ProjectController@deleteProject', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your project. Do you really want to do this?</p>
                <?php echo Form::hidden('project_id', $project->id); ?>
                <?php echo Form::hidden('slug', $slug); ?>
                <?php echo Form::hidden('project', $project->slug); ?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="ModalSectionDelete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'ProjectController@deleteSection', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your version. Do you really want to do this?</p>
                <?php echo Form::hidden('section_id', '', array('class'=>'section-hidden')); ?>
                <?php echo Form::hidden('project_id', $project->id); ?>
                <?php echo Form::hidden('slug', $slug); ?>
                <?php echo Form::hidden('project', $project->slug); ?>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="ModalNoteDelete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <?php echo Form::open(array('action' => 'ProjectController@deleteNote', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your note. Do you really want to do this?</p>
                <?php echo Form::hidden('note_id', '', array('class'=>'note-hidden')); ?>
                <?php echo Form::hidden('project_id', $project->id); ?>
                <?php echo Form::hidden('slug', $slug); ?>
                <?php echo Form::hidden('project', $project->slug); ?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<script type="text/javascript">
    jQuery.noConflict();

    var DraggableNotes = function () {

        return {
            //main function to initiate the module
            init: function () {

                if (!jQuery().sortable) {
                    return;
                }

                jQuery(".changes").sortable({
                    connectWith: ".notes",
                    items: ".notes",
                    opacity: 0.8,
                    coneHelperSize: true,
                    placeholder: 'sortable-box-placeholder round-all',
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    update: function( event, ui ) { DraggableNotes.save(event.target) }
                });

                jQuery(".changes").disableSelection();

            },
            save: function (target) {
                var sortedIDs = jQuery( target ).sortable('toArray', {attribute: 'id'}).map(function(x){return x.replace(/notes\_/g, '');});
                jQuery.ajax({
                    type: "POST",
                    url: '/order-note',
                    data: { _token: "<?php echo Session::token();?>",order:sortedIDs }
                }).error(function( msg ) {
                    alert( msg );
                });
            }

        };

    }();

    var DraggableVersions = function () {

        return {
            //main function to initiate the module
            init: function () {

                if (!jQuery().sortable) {
                    return;
                }

                jQuery(".versions").sortable({
                    connectWith: ".version",
                    items: ".version",
                    opacity: 0.8,
                    coneHelperSize: true,
                    placeholder: 'sortable-box-placeholder round-all',
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    update: function( event, ui ) { DraggableVersions.save(event.target) }
                });

                jQuery(".versions").disableSelection();

            },
            save: function (target) {
                var sortedIDs = jQuery( target ).sortable('toArray', {attribute: 'id'}).map(function(x){return x.replace(/versions\_/g, '');});
                jQuery.ajax({
                    type: "POST",
                    url: '/order-version',
                    data: { _token: "<?php echo Session::token();?>",order:sortedIDs }
                }).error(function( msg ) {
                    alert( msg );
                });
            }

        };

    }();

    jQuery(document).ready(function($) {
        $('.section-delete').click(function(e){
            e.preventDefault();
            var section = $(this).data('section');
            $('.section-hidden').val(section);
            $('#ModalSectionDelete').modal();
        });
        
        $('.note-delete').click(function(e){
            e.preventDefault();
            var note = $(this).data('note');
            $('.note-hidden').val(note);
            $('#ModalNoteDelete').modal();
        });
        
        DraggableNotes.init();
        
        DraggableVersions.init();
        
        $('ul.changes li.notes').hover(function() {
                        $(this).addClass("hovered"); 
        },function() {
                        $(this).removeClass("hovered"); 
        }
        ); 
    });


</script>