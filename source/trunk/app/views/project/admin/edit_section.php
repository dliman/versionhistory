    <section id="main-content">
        
        <section class="wrapper">
            <div class="container">
                <!-- page start-->
                
                <div class="row">
                    <div class="col-md-12">

                        <h4><a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project_slug)); ?>"><i class="fa fa-chevron-left"></i> <?php echo $project->project->title; ?> </a></h4>
                        
                        <div class="panel add-release-notes">
                            <div class="panel-body">
                                <?php echo Form::open(array(
                                    'action'=>'ProjectController@postEditSection', 
                                    'method'=>'post', 
                                    'id' => 'fileupload',
                                    'enctype' => "multipart/form-data")); ?>
                                
                                    <div class="row">

                                        <div class="col-sm-12 col-xs-12 col-md-8">
                                            <p>Version Name</p>
                                            <?php echo Form::text(
                                                    'title', 
                                                    Input::old('title')?Input::old('title'):$project->title, 
                                                    array(
                                                        'class'=>'form-control title', 
                                                        'placeholder'=>'Version Name'
                                                        )); ?>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 col-md-4">
                                            <p>Version Date</p>
                                            <?php echo Form::text(
                                                    'released_at', 
                                                    Input::old('released_at')?Input::old('released_at'): date('m-d-Y', strtotime($project->released_at)), 
                                                    array(
                                                        'class'=>'form-control default-date-picker', 
                                                        'placeholder'=>'Version Date'
                                                        )); ?>
                                            <div class="checkbox">
                                                <label>
                                                    <?php echo Form::checkbox('archived', 'yes', 
                                                            Input::old('archived') ? 
                                                                ( Input::old('archived') == 'yes' ? true : false ) : 
                                                            (isset($project->archived) ? 
                                                                ($project->archived=='yes' ? true : false ): false )); ?>&nbsp;Archive
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <p>
                                        Version Description (optional)
                                    </p>
                                    <div class="form-group">
                                        <?php echo Form::textarea(
                                                'editor1',
                                                Input::old('description')?Input::old('description'):$project->description,
                                                array('class'=>'form-control ckeditor', 'rows'=>'6' )) ?>
                                        
                                    </div>
                                    <div class="trakingRedactor" style='display:none;'></div>
                                    <div class="row fileupload-buttonbar form-button-block">
                                        <div class="col-lg-12">
                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                            <span class="btn btn-success fileinput-button attach-file">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                <span>Add Attachment</span>
                                                <input type="file" name="files[]" multiple>
                                            </span>


                                        </div>
                                        <!-- The global progress state -->
                                        <div class="col-lg-12 fileupload-progress fade">
                                            <!-- The global progress bar -->
                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                            </div>
                                            <!-- The extended global progress state -->
                                            <div class="progress-extended">&nbsp;</div>
                                        </div>
                                </div>
                                <div class="col-md-6">
                                    <table role="presentation" class="table table-attached-files">
                                    <tbody class="attached-files">
                                        <?php if($project->attachment):?>
                                        <?php foreach ($project->attachment as $attachment) :?>
                                        <tr>
                                            <td><p data-attach="<?php echo $attachment->id ?>" class="name"><?php echo $attachment->attachment; ?></p></td>
                                            <td><i class="glyphicon glyphicon-remove-circle cancel" data-attach="<?php echo $attachment->id ?>"></i></td>
                                        </tr>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </tbody></table>
                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-upload-files"><tbody class="files"></tbody></table>
                                </div>
                                <div class="clearfix"></div>
                                    <div class="col-mg-12 fileupload-buttonbar form-button-block">
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                        <?php echo Form::submit('Save',array('class'=>"start btn btn-success")); ?>
                                        <?php // echo Form::button('Cancel',array('class'=>"cancel btn btn-default", 'id'=>'cancel_form',  'type'=>'reset')); ?>
                                        <a href="<?php echo action('ProjectController@project', array('slug'=>$slug, 'project' => $project_slug)); ?>" class="btn btn-default">Cancel</a>
                                        <?php echo Form::button('Delete',array('class'=>"delete btn btn-danger pull-right")); ?>
                                        <?php echo Form::hidden('project_id', $project->project->id, array('class'=>'project_id'));?>
                                        <?php echo Form::hidden('section_id', $project->id, array('class'=>'section_id'));?>
                                        <?php echo Form::hidden('slug', $slug, array('class'=>'slug'));?>
                                        <?php echo Form::hidden('project', $project_slug, array('class'=>'project'));?>
                                        
                                        <?php echo Form::token();?>
                                    </div>
                                <?php echo Form::close(); ?>
                            </div>
                        </div>


                    </div>
                </div>

               
            </div>
        </section>
    </section>
    
<!-- Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>



            <?php echo Form::open(array('action' => 'ProjectController@deleteSection', 'method' => "POST")); ?>
            <div class="modal-body">
                <p>This action will delete your section. Do you really want to do this?</p>
                <?php echo Form::hidden('section_id', $project->id); ?>
                <?php echo Form::hidden('project_id', $project->project->id); ?> 
                <?php echo Form::hidden('slug', $slug, array('class'=>'slug'));?>
                <?php echo Form::hidden('project', $project_slug, array('class'=>'project'));?>
                
            </div>
            <div class="modal-footer">
                <?php echo Form::button('Cancel', array('data-dismiss' => "modal", 'class' => "btn btn-default")); ?>
                <?php echo Form::submit('Delete', array('class' => "btn btn-success")); ?>
                <?php echo Form::token(); ?>
            </div>
            <?php echo Form::close(); ?>
        </div>
    </div>
</div>
<!-- modal -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <p class="name">{%=file.name%}</p>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}

    <i class="glyphicon glyphicon-upload start"></i>


    {% } %}
    {% if (!i) { %}

    <i class="glyphicon glyphicon-remove-circle cancel"></i>

    {% } %}
    </td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <p class="name">{%=file.name%}</p>
    </td>
    
    <td>
    
    <i class="glyphicon glyphicon-remove-circle cancel"></i>
    
    </td>
    </tr>
    {% } %}

</script>
<script type="text/javascript">
     jQuery( document ).ready(function($) {
        jQuery('.default-date-picker').bootstrapDP({
            format: 'mm-dd-yyyy'
        }); 
        
        CKEDITOR.replace('editor1');
        var editor_text;
        function updateTrakingRedactor(){
            editor_text = CKEDITOR.instances.editor1.getData();
            $('.trakingRedactor').html(editor_text);
        }
        timer = setInterval( function(){ 
            updateTrakingRedactor();
        },100);
        'use strict';

        // Initialize the $ File Upload widget:
        $('#fileupload').fileupload();

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            {
                url:'/edit-section',
                complete: function (e, data) {
                    window.location.href = window.location.origin+'/admin/'+jQuery('.slug').val()+'/'+jQuery('.project').val();
                }
            }
        ).on('fileuploadsubmit', function (e, data) {
            data.formData = {
                'title': $('.title').val(),
                'editor1': $('.trakingRedactor').html(),
                'project_id': $('.project_id').val(),
                'section_id': $('.section_id').val(),
                'slug': jQuery('.fileupload-buttonbar .slug').val(),
                'project':jQuery('.fileupload-buttonbar .project').val(),
                'released_at': jQuery('input[name="released_at"]').val(),
                '_token' : $('.fileupload-buttonbar').find('input[name="_token"]').val()
            };
        });

        $('.form-button-block').find('.start').click(function(){

            var table_files_upload = $('.table-upload-files').find('.template-upload').size();
            var table_files_download = $('.table-upload-files').find('.template-download').size();
            if( (table_files_upload == 0) || (table_files_download > 0) ){
                $('#fileupload').submit();
            }
        });           

        $('.table-upload-files').on('click', '.template-download .cancel', function(){
            var f_name = $(this).closest('.template-download').find('.name').text();
            var token = $('.fileupload-buttonbar').find('input[name="_token"]').val();
            var section_id = $('.fileupload-buttonbar').find('input[name="section_id"]').val();
            var flag = 1;
            $.post('/delete-section-attachment',{flag: flag, section_id: section_id, f_name: f_name, _token: token },function(data){

            });
        });

        $('.table-attached-files').on('click', '.cancel', function(){
            var attach = $(this).data('attach');
            var f_name = $('p[data-attach="'+attach+'"]').text();
            var token = $('.fileupload-buttonbar').find('input[name="_token"]').val();
            var flag = 2;
            $.post('/delete-section-attachment',{flag: flag, attach: attach, f_name: f_name, _token: token },function(data){
                $('.table-attached-files').find('.cancel').each(function(){
                    if($(this).attr('data-attach') == data){
                        $(this).closest('tr').remove();
                    }
                });
            });

        });
        
        $('.delete').click(function(){
             $('#myModal').modal();
        });
    });
</script>