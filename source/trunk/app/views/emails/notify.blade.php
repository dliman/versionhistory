<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2><?php echo $title; ?></h2>

		<div>
			 
                    <p>
                    We hope you've found VersionHistory.io useful. If you'd like to keep
                    your account active, remember to add your billing information before
                    your free trial is over.
                    </p>
                    <p>
                        To do that, simply log into your <a href="http://versionhistory.io/login"><?php echo $company; ?> account</a>, click the
                    Account link at the top and then scroll down to the Billing
                    Information section.
                    </p>
                    <p>
                    Of course, if you have any questions or feedback, please don't
                    hesitate to contact us at info@versionhistory.io
                    </p>
                    Thanks!<br>
                    -<strong>The VersionHistory.io Team</strong>
		</div>
	</body>
</html>