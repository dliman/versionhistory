<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
                    <h2>Hi Admin!</h2>
                    <p>An account cancelled:</p>
        <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
            <tr >
                <td> Email address: </td><td><b> {{ $user->email }} </b></td>
            </tr>
            <tr >
                <td> Company name: </td><td><b> {{ $user->company->name }} </b></td>
            </tr>
            <tr >
                <td> Comments: </td><td><b> {{{ $comment }}} </b></td>
            </tr>
            <?php if(isset($users)) : ?>
            <tr >
                <td> User Accounts cancel too: </td>
                <td>
                    <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0">
                    <?php foreach ($users as $userAccount) { ?>
                        <tr >
                            <td><b> {{ $userAccount->email }} </b></td>
                        </tr>
                    <?php } ?>
                    </table>
                </td>
            </tr>
            <?php endif; ?>
        </table>
                    <p>For more information, visit us at: https://versionhistory.io/vhadmin</p>
		</div>
	</body>
</html>