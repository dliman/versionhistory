<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
                    <p>Check out <?php echo $companyName; ?>&#39;s latest updates for <?php echo $projectName; ?>:</p>

                    <?php if (isset($sections)) { ?>
                    <?php foreach ($sections as $section) { ?>
                    <h4><?php echo $section->title;?></h4>
                    <?php if (isset($section->note)) { ?>
                    <?php   foreach ($section->note as $note) { ?>
                    <?php       echo $note->title; ?><br/>
                    <?php   } ?>
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>

                    <p>For more information, visit us at: http://<?php echo $customdomainURL; ?></p>

Thanks!<br/>
- <?php echo $companyName; ?>

                    <p>
                        If you no longer want to receive these notifications, <a href="http://<?php echo $customdomainURL . '/unsubscribe-updates/'. $token; ?>">click here to unsubscribe</a> .
                    </p>
		</div>
	</body>
</html>