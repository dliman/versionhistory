<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
                    <h2>Hi!</h2>
                    <p>Thanks for signing up with VersionHistory.io, the web-based application that helps you create a better version history for your product and customers.</p>
                    <p>Here are your login details:</p>
        <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
            <tr >
                <td> Email address : </td><td><b> {{ $user->email }} </b></td>
            </tr>
            <tr >
                <td> Password : </td><td><b> {{ $password }} </b></td>
            </tr>
        </table>
                    <p>Your current Website address is http://versionhistory.io/{{ $company->slug }} but of course you can replace that with your own domain, such as: http://version.{{ $company->slug }}.com.</p>
                    <p>If you have any questions at anytime, just let us know at support@versionhistory.io</p>
                    <p>Thanks!<br/>-The VersionHistory.io team</p>
		</div>
	</body>
</html>