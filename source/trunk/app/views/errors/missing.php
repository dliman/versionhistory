<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description"
              content="VersionHistory.io provides better communication to your customres about your application's version history">
        <meta name="author" content="VersionHistory.io">
        <meta name="keywords"
              content="VersionHistory.io, version history, release notes, change log, versionhistory, updates, software, web application, saas, web app">
        <link rel="shortcut icon" href="img/favicon.png">
        <title>VersionHistory.io - 404</title>
        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="/js/html5shiv.js"></script>
        <script src="/js/respond.min.js"></script>
        <![endif]-->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-53855658-1', 'auto');
          ga('send', 'pageview');

      </script>
    </head>
    <body class="login-body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron" style="background:#428bca;">
                        <div class="container">
                            <div class="text-center"><p class="bg-primary"><?php echo $message; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
