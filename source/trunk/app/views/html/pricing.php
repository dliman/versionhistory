
	<section id="content">
	  <div class="bg-dark lt">
      <div class="container">
        <div class="m-b-lg m-t-lg">
          <h3 class="m-b-none">Pricing</h3>
        </div>
      </div>
    </div>
    <div class="bg-white b-b b-light">
      <div class="container">      
        <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
          <li><a href="/">Home</a></li>
          <li class="active">Pricing</li>
        </ul>
      </div>
    </div>
    <div class="container">
      <div class="m-t-xl m-b-xl text-center">
        <h2>VersionHistory.io Pricing Plans </h2>
      </div>
      <div class="clearfix">
        <div class="row m-b-xl">
          <div class="col-sm-4 animated777 fadeInLeftBig">
            <section class="panel b-light text-center m-t">
              <header class="panel-heading bg-white b-light">
                <h3 class="m-t-sm">Startup</h3>
                <p>For individuals and startups</p>
              </header>
              <ul class="list-group">
                <li class="list-group-item text-center bg-light lt">
                  <span class="text-danger font-bold h1">$10</span> / month
                </li>
                <li class="list-group-item">
                  Up to 2 projects
                </li>
                <li class="list-group-item">
                  Up to 2 team members
                </li>
                <li class="list-group-item">
                  Unlimited versions 
                </li>
                <li class="list-group-item">
                  Unlimited notes
                </li>
                <li class="list-group-item">
                  Customize the look &amp; feel
                </li>
                  <li class="list-group-item">
                  Auto notify users of updates
                </li>
                   <li class="list-group-item">
                  Receive product feedback
                </li>
                <li class="list-group-item">
                  Knowledge base &amp; Email support
                </li>
              </ul>
              <footer class="panel-footer">
                <a href="/login" class="btn btn-primary m-t m-b">Try it Free</a>
              </footer>
            </section>
          </div>
          <div class="col-sm-4 animated777 fadeInUp">
            <section class="panel b-primary text-center">
              <header class="panel-heading bg-primary">
                <h3 class="m-t-sm">Business</h3>
                <p>For most companies because it's the middle option and highlighted in a bright color. Only kidding.</p>
              </header>
              <ul class="list-group">
                <li class="list-group-item text-center bg-light lt">
                  <div class="padder-v">
                    <span class="text-danger font-bold h1">$20</span> / month
                  </div>
                </li>
                <li class="list-group-item">
                  Up to 10 projects
                </li>
                <li class="list-group-item">
                  Up to 10 team members
                </li>
                <li class="list-group-item">
                  Unlimited versions 
                </li>
                <li class="list-group-item">
                  Unlimited notes
                </li>
                <li class="list-group-item">
                  Customize the look &amp; feel
                </li>
                 <li class="list-group-item">
                  Auto notify users of updates
                </li>
                   <li class="list-group-item">
                  Receive product feedback
                </li>
                <li class="list-group-item">
                  Knowledge base &amp; Email support
                </li>
              </ul>
              <footer class="panel-footer">
                <a href="/login" class="btn btn-primary m-t m-b">Try it Free</a>
              </footer>
            </section>
          </div>
          <div class="col-sm-4 animated777 fadeInRightBig">
            <section class="panel b-light text-center m-t">
              <header class="panel-heading bg-white b-light">
                <h3 class="m-t-sm">Unlimited</h3>
                <p>For large enterprises (as in companies, not Star Trek)</p>
              </header>
              <ul class="list-group">
                <li class="list-group-item text-center bg-light lt">
                  <span class="text-danger font-bold h1">$40</span> / month
                </li>
                <li class="list-group-item">
                  Unlimited projects
                </li>
                <li class="list-group-item">
                  Unlimited team members
                </li>
                <li class="list-group-item">
                  Unlimited versions 
                </li>
                <li class="list-group-item">
                  Unlimited notes
                </li>
                <li class="list-group-item">
                  Customize the look &amp; feel
                </li>
                                 <li class="list-group-item">
                  Auto notify users of updates
                </li>
                   <li class="list-group-item">
                  Receive product feedback
                </li>
                <li class="list-group-item">
                  Knowledge base &amp; Email support
                </li>
              </ul>
              <footer class="panel-footer">
                <a href="/login" class="btn btn-primary m-t m-b">Try it Free</a>
              </footer>
            </section>
          </div>
        </div>
      </div>      
    </div>
	</section>
