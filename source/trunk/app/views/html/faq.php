	<section id="content">
	  <div class="bg-dark lt">
      <div class="container">
        <div class="m-b-lg m-t-lg">
          <h3 class="m-b-none">FAQ</h3>
        </div>
      </div>
    </div>
    <div class="bg-white b-b b-light">
      <div class="container">      
        <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
          <li><a href="/">Home</a></li>
          <li class="active">FAQ</li>
        </ul>
      </div>
    </div>
      <div class="container">
        <div class="faq m-t-xl m-b-xl">
        	<h3>1. What is VersionHistory.io?</h3> 
        	<p>VersionHistory.io is a Web-based application (Software-as-a-Service) that allows you to create and share updates to your projects. For example, let’s say you’re creating an awesome new software product. You can use VersionHistory.io to communicate to your customers any new features or changes to the product. </p>
        	<h3>2. How is this different than a blog or just sending an email?</h3> 
        	<p>Because that's so 2013. Blogs usually talk about the big features, take a long time to write and edit and include a mix of topics. VersionHistory.io is for tracking all of the product’s changes, big or small and sharing it with your customers. It lets you keep a running list of updates, makes it fast to input and easy for customers to read. Most customers don’t want to spend a lot of time reading through blog posts – they want to quickly see what’s new or changed and get on with their work. </p>
        	<h3>3. How much does it cost?</h3> 
        	<p>Please visit our <a href="pricing">pricing page</a>.</p>
        	<h3>4. Are there any limits?</h3> 
        	<p>Depends on the plan you sign up with. We do limit the number of projects and team members (contributors). But within each project, you can create an unlimited number of versions and notes.</p> 
        	<h3>5. What if I don’t want to make my projects public?</h3> 
        	<p>No problem at all – you can mark a project as private and only you and your team members can see it. </p>
        	<h3>6. Can I update the design?</h3>
        	<p>Yes, you can customize the look & feel to match your website or brand. You add your logo, favicon, header and background graphics. Or if you’re familiar with CSS, you can apply custom styles and do even more enhanced customizations. </p>
            <h3>7. Can I use my own subdomain?</h3>
        	<p>Yes, you can replace your VersionHistory.io URL (for example, http://versionhistory.io/your-company) can use a custom subdomain like: http://version.your-company.com</p>
            <h3>8. Can I get a list of my customers who have subscribed to updates?</h3>
        	<p>Yes, it's your data. You can export the email addresses at anytime.</p>
          	<h3>9. Are there any contracts or commitments?</h3>
        	<p>Nope. VersionHistory.io is a month-to-month service. If you're not happy with it, you can cancel anytime. You'll be billed for the current month but never again.</p>
        	<h3>10. What if I have more questions, suggestions or just want to talk about the mole on my back?</h3>
        	<p>
        		No problem! Just email us at <a href="mailto:info@versionhistory.io">info@versionhistory.io</a> and we'll get back to you in a jiffy. Or giffy. Whichever you prefer. 
        	</p>
        </div> 
      </div>
	</section>
  
