
	<section id="content">
	  <div class="bg-primary dk">
      <div class="text-center wrapper">
        <div class="m-t-xl m-b-xl">
          <div class="text-uc h1 font-bold inline">
            <div class="pull-left m-r-sm text-white">A Better Version History</div>
          </div>
          <div class="h4 text-muted m-t-sm">Create and share your product's updates and changes with customers</div>
        </div>
        <p class="text-center m-b-xl">
          <!-- <a href="#" target="_blank" class="btn btn-lg btn-dark m-sm">View the Demo</a> -->
          <a href="/login" class="btn btn-lg btn-warning b-white bg-empty m-sm">Try it Free for 30 Days!</a>
        </p>
      </div>
      <div class="padder">
        <div class="hbox">
          <aside class="col-md-3 v-bottom text-right">
            <div class="hidden-sm hidden-xs">
              <section class="panel bg-dark inline m-b-n-lg m-r-n-lg aside-sm no-border device phone animated777 fadeInLeftBig">
                <header class="panel-heading text-center">
                  <i class="fa fa-minus fa-2x m-b-n-xs block"></i>
                </header>
                <div>
                  <div class="m-l-xs m-r-xs">
                    <img src="images/phone-2.png" alt="" class="img-full" >
                  </div>
                </div>
              </section>
            </div>
          </aside>
          <aside class="col-sm-10 col-md-6">
            <section class="panel bg-dark m-b-n-lg no-border device animated777 fadeInUp">
              <header class="panel-heading text-left">
                <i class="fa fa-circle fa-fw"></i>
                <i class="fa fa-circle fa-fw"></i>
                <i class="fa fa-circle fa-fw"></i>
              </header>
              <img src="images/main.png" alt="" class="img-full" >
            </section>
          </aside>
          <aside class="col-md-3 v-bottom">
            <div class="hidden-sm hidden-xs">
              <section class="panel bg-light m-b-n-lg m-l-n-lg aside no-border device phone animated777 fadeInRightBig">
                <header class="panel-heading text-center">
                  <i class="fa fa-minus fa-2x text-white m-b-n-xs block"></i>
                </header>
                <div class="">
                  <div class="m-l-xs m-r-xs">
                    <img src="images/phone-1.png" alt="" class="img-full" >
                  </div>
                </div>
              </section>
            </div>
          </aside>
        </div>
      </div>
      <div class="dker pos-rlt">
        <div class="container wrapper">
          <div class="m-t-lg m-b-lg text-center">
                      VersionHistory.io is perfect for software products, mobile apps, Web applications or even physical products. 
          </div>
        </div>
      </div>
    </div>
    <div id="about">
      <div class="container">
        <div class="m-t-xl m-b-xl text-center wrapper">
          <h3>VersionHistory.io makes it easy to create, manage and share your product’s release notes</h3>
          <p class="text-muted">Your customers can subscribe to updates, submit suggestions for upcoming versions and more!</p>
        </div>
        <div class="row m-t-xl m-b-xl text-center">
          <div class="col-sm-4" data-ride="animated777" data-animation="fadeInLeft" data-delay="300">
            <p class="h3 m-b-lg">
              <i class="fa fa-paper-plane fa-3x text-info"></i>
            </p>
            <div class="">
              <h4 class="m-t-none">Keeping It Simple</h4>
              <p class="text-muted m-t-lg">You probably don't want to write lengthy blog posts or emails about every little update (and your customers don’t have time to read them). Use VersionHistory.io and make everybody happy.</p>
            </div>
          </div>
          <div class="col-sm-4" data-ride="animated777" data-animation="fadeInLeft" data-delay="600">
            <p class="h3 m-b-lg">
              <i class="fa fa-wrench fa-3x text-info"></i>
            </p>
            <div class="">
              <h4 class="m-t-none">Full Control</h4>
              <p class="text-muted m-t-lg">Manage everything through your dashboard. Add team members so they can add their updates. Change the look and feel to match your company’s brand. Customize it to your heart’s content.</p>
            </div>
          </div>
          <div class="col-sm-4" data-ride="animated777" data-animation="fadeInLeft" data-delay="900">
            <p class="h3 m-b-lg">
              <i class="fa fa-group fa-3x text-info"></i>
            </p>
            <div class="">
              <h4 class="m-t-none">Power to the People</h4>
              <p class="text-muted m-t-lg">Customers can subscribe to updates and be notified of changes automatically. You can also let them submit requests for future versions and create printer-friendly release notes to impress their friends.</p>
            </div>
          </div>
        </div>
        <!-- <div class="m-t-xl m-b-xl text-center wrapper">
          <p class="h5">You can use it to build your 
            <span class="text-primary">Content manage system</span>, 
            <span class="text-primary">Travel app</span>, 
            <span class="text-primary">Medical app</span>...
          </p>
        </div> -->
      </div>
    </div>
    <!-- <div id="responsive" class="bg-dark"> -->
      <div class="bg-dark">
      <div class="text-center">
        <div class="container">
          <div class="m-t-lg wrapper">
            <h3 class="text-white">VersionHistory.io works no matter what device you use</h3>
          </div>
          <div class="row">
            <div class="col-sm-4 wrapper-xl" data-ride="animated777" data-animation="fadeInLeft" data-delay="300">
              <p class="text-center h2 m-b-lg m-t-lg">
                <span class="fa-stack fa-2x">
                  <i class="fa fa-circle fa-stack-2x text-dark"></i>
                  <i class="fa fa-mobile fa-stack-1x text-muted"></i>
                </span>
              </p>
              <p>Mobile</p>
            </div>
            <div class="col-sm-4 wrapper-xl" data-ride="animated777" data-animation="fadeInLeft" data-delay="600">
              <p class="text-center h1 m-b-lg">
                <span class="fa-stack fa-2x">
                  <i class="fa fa-circle fa-stack-2x text-dark"></i>
                  <i class="fa fa-tablet fa-stack-1x text-muted"></i>
                </span>
              </p>
              <p>Tablet</p>
            </div>
            <div class="col-sm-4 wrapper-xl" data-ride="animated777" data-animation="fadeInLeft" data-delay="900">
              <p class="text-center h2 m-b-lg m-t-lg">
                <span class="fa-stack fa-2x">
                  <i class="fa fa-circle fa-stack-2x text-dark"></i>
                  <i class="fa fa-desktop fa-stack-1x text-muted text-md"></i>
                </span>
              </p>
              <p>Desktop</p>
            </div>
          </div>
        </div>
      </div>
    </div> 
    <div id="newsletter" class="bg-white clearfix wrapper-lg">
      <div class="container text-center m-t-xl m-b-xl" data-ride="animated777" data-animation="fadeIn">        
        <h2>Demo time!</h2>
        <p>Check out the version history of VersionHistory.io</p>
		<a href="http://meta.versionhistory.io/versionhistory" target="_blank" class="btn btn-lg btn-dark m-sm">Our Version History</a>
      </div>
    </div>
	</section>
