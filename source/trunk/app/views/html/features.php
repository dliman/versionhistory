
	<section id="content">
	  <div class="bg-dark lt">
      <div class="container">
        <div class="m-b-lg m-t-lg">
          <h3 class="m-b-none">Features</h3>
        </div>
      </div>
    </div>
    <div class="bg-white b-b b-light">
      <div class="container">      
        <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
          <li><a href="/">Home</a></li>
          <li class="active">Features</li>
        </ul>
      </div>
    </div>
    <div>
      <div class="container m-t-xl">
        <div class="row">
          <div class="col-sm-7">
            <h2 class="font-thin m-b-lg">Better Communication of your Product Changes</h2>
            <p class="h4 m-b-lg l-h-1x">Unless you were an English major, you probably don't enjoy writing long blog posts or emails about changes to your product. And the next thing you know, you've added or changed a couple dozen things and your customers have no idea. And they don't have the time to search your application for changes. It's a vicious cycle. Much like laundry, taking out the trash or changing diapers. Instead, you can use VersionHistory.io to help you easily create, manage and share your product's updates (sorry, we off diaper duty so can't help there). 
            </p>
           <!-- <div class="row m-b-xl">
              <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Have Any Number of Columns</div>
              <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Scales to Any Width</div>
              <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>It's Smart and Easy</div>
              <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>It Fits In with You</div>
              <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Take What You Need</div>
              <div class="col-sm-6"><i class="fa fa-fw fa-angle-right"></i>Flexible way to create a responsive layout</div>
            </div> -->
            <p class="clearfix">&nbsp;</p>
            <div class="row m-b-xl">
              <div class="col-xs-2"><i class="fa fa-cloud fa-4x icon-muted"></i></div>
              <div class="col-xs-2"><i class="fa fa-plus icon-muted m-t"></i></div>
              <div class="col-xs-2"><i class="fa fa-cogs fa-4x icon-muted"></i></div>
              <div class="col-xs-2"><i class="fa fa-plus icon-muted m-t"></i></div>
              <div class="col-xs-2"><i class="fa fa-wrench fa-4x icon-muted"></i></div>
            </div>
          </div>
          <div class="col-sm-5 text-center">
            <section class="panel bg-dark inline aside-md no-border device phone animated777 fadeInRightBig">
              <header class="panel-heading text-center">
                <i class="fa fa-minus fa-2x icon-muted m-b-n-xs block"></i>
              </header>
              <div class="m-l-xs m-r-xs">
                <div class="carousel auto slide" id="c-fade" data-interval="3000">
                    <div class="carousel-inner">
                      <div class="item active text-center">
                        <img src="images/phone-2.png" alt="" class="img-full" >
                      </div>
                      <div class="item text-center">
                        <img src="images/phone-1.png" alt="" class="img-full" >
                      </div>
                    </div>
                </div>
              </div>
              <footer class="bg-dark text-center panel-footer no-border">
                <i class="fa fa-circle icon-muted fa-lg"></i>
              </footer>
            </section>
          </div>
        </div>        
      </div>
    </div>
    <div class="bg-white b-t b-light">
      <div class="container">
        <div class="row m-t-xl m-b-xl">
          <div class="col-sm-5 text-center clearfix m-t-xl" data-ride="animated777" data-animation="fadeInLeftBig">
            <i class="ico-pen icon-large icon-muted"></i>
          </div>
          <div class="col-sm-7">
            <h2 class="font-thin m-b-lg">Unlimited Versions and Notes</h2>
            <p class="h4 m-b-lg l-h-1x">We don't limit the number of versions or notes you can add. So as your product grows, we'll grow with you. Plus we're always adding new features all the time!
            </p>
           <!-- <p class="m-b-xl">We make the less files as Bootstrap way. you can config the variables in one place, like colors: @brand-primary, @brand-success, @border-color. and you can get the unlimited colors for your application.</p>
            <p class="m-t-xl m-b-xl h4"><i class="fa fa-quote-left fa-fw fa-1x icon-muted"></i> Less is More</p> --> 
          </div>
        </div>
      </div>
    </div>
    <div class="b-t b-light">
      <div class="container m-t-xl">
        <div class="row">
          <div class="col-sm-7">
            <h2 class="font-thin m-b-lg">Keep it Simple or Go Detailed</h2>
            <p class="h4 m-b-lg l-h-1x">If you're like us, we keep it simple. Short and sweet. Get in, get out. If only the DMV was that easy. We digress. You can make your Version History as succint or elaborate as you want. Only want to bullet point your updates? No problem! Want to write the next War &amp; Peace about each change? No problem!
            </p>
            <p class="m-b-xl">
              <a href="/login" class="btn btn-sm btn-primary font-bold">Try it now</a>
            </p>
          </div>
          <div class="col-sm-5 text-center"  data-ride="animated777" data-animation="fadeInUp" >
            <section class="panel bg-dark m-t-lg m-r-n-lg m-b-n-lg no-border device animated777 fadeInUp">
              <header class="panel-heading text-left">
                <i class="fa fa-circle fa-fw icon-muted"></i>
                <i class="fa fa-circle fa-fw icon-muted"></i>
                <i class="fa fa-circle fa-fw icon-muted"></i>
              </header>
              <img src="images/main.png" alt="" class="img-full" >
            </section>
          </div>
        </div>        
      </div>
    </div>
    <div class="bg-white b-t b-light pos-rlt">
      <div class="container">
        <div class="row m-t-xl m-b-xl">
			<div class="col-sm-5 text-center clearfix m-t" data-ride="animated777" data-animation="fadeInLeftBig">
            <i class="ico-lock icon-large icon-muted"></i>
          </div>
          <div class="col-sm-7">
            <h2 class="font-thin m-b-lg">Make it Public or Keep it Private</h2>
            <p class="h4 m-b-lg l-h-1x">We support both internal and external projects. So you can set your projects as public or private and share it with the outside world or not. 
            </p>
          <!--  <ul class="m-b-xl fa-ul">
              <li><i class="fa fa-li fa-check text-muted"></i>369 Fontawesome icons</li>
              <li><i class="fa fa-li fa-check text-muted"></i>+30 Jquery Components</li>
              <li><i class="fa fa-li fa-check text-muted"></i>Application pages</li>
            </ul> -->
          </div>
        </div>
      </div>
    </div>
    <div class="b-t b-light">
      <div class="container m-t-xl">
        <div class="row">
          <div class="col-sm-7">
            <h2 class="font-thin m-b-lg">For Your Customers</h2>
            <p class="h4 m-b-lg l-h-1x">
              VersionHistory.io makes it easy for your customers to explore your updates, print them out, search them, submit suggestions and feedback for current and future versions and subscribe so they're automatically notified of new updates. It will give your customers the warm and fuzzies. 
            </p>
            <p class="m-b-xl">
              <a href="/login" class="btn btn-dark font-bold">Try it now</a>
            </p>
          </div>
          <div class="col-sm-5 text-center" data-ride="animated777" data-animation="fadeInRightBig" >
            <section class="panel bg-light m-t-lg m-r-n-lg m-b-n-lg no-border device animated777 fadeInUp">
              <header class="panel-heading text-left">
                <i class="fa fa-circle fa-fw icon-muted"></i>
                <i class="fa fa-circle fa-fw icon-muted"></i>
                <i class="fa fa-circle fa-fw icon-muted"></i>
              </header>
              <img src="images/app.png" alt="" class="img-full" >
            </section>
          </div>
        </div>        
      </div>
    </div>
    <div class="bg-white b-t b-light pos-rlt">
      <div class="container">
        <div class="row m-t-xl m-b-xl">
			<div class="col-sm-5 text-center clearfix m-t" data-ride="animated777" data-animation="fadeInLeftBig">
            <i class="ico-params icon-large icon-muted"></i>
          </div>
          <div class="col-sm-7">
            <h2 class="font-thin m-b-lg">Customizations Galore</h2>
            <p class="h4 m-b-lg l-h-1x">Of course we give you a bunch of tools to customize your VersionHistory.io site as well. You can use your own subdomain (e.g., version.company.com), match your brand design, add your logo, change the background, insert your own styles and lots more. It's your VersionHistory after all.
            </p>
         <!--   <ul class="m-b-xl fa-ul">
              <li><i class="fa fa-li fa-check text-muted"></i>369 Fontawesome icons</li>
              <li><i class="fa fa-li fa-check text-muted"></i>+30 Jquery Components</li>
              <li><i class="fa fa-li fa-check text-muted"></i>Application pages</li>
            </ul> -->
          </div>
        </div>
      </div>
    </div>
	</section>
