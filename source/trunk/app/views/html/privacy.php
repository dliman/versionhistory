
	<section id="content">
	  <div class="bg-dark lt">
      <div class="container">
        <div class="m-b-lg m-t-lg">
          <h3 class="m-b-none">Privacy Policy</h3>
        </div>
      </div>
    </div>
    <div class="bg-white b-b b-light">
      <div class="container">      
        <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
          <li><a href="/">Home</a></li>
          <li class="active">Privacy Policy</li>
        </ul>
      </div>
    </div>
      <div class="container">
        <div class="faq m-t-xl m-b-xl">
        
        	<p>Website Visitors<br />
Like most website operators, VersionHistory.io collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. VersionHistory.io’s purpose in collecting non-personally identifying information is to better understand how VersionHistory.io’s visitors use its website. From time to time, VersionHistory.io may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.
</p><p>
VersionHistory.io also collects potentially personally-identifying information like Internet Protocol (IP) addresses. VersionHistory.io does not use such information to identify its visitors, however, and does not disclose such information, other than under the same circumstances that it uses and discloses personally-identifying information, as described below.
</p>

<p>Gathering of Personally-Identifying Information<br />
Certain visitors to VersionHistory.io’s websites choose to interact with VersionHistory.io in ways that require VersionHistory.io to gather personally-identifying information. The amount and type of information that VersionHistory.io gathers depends on the nature of the interaction. For example, we ask visitors who create an account to provide an email address. In each case, VersionHistory.io collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor’s interaction with VersionHistory.io. VersionHistory.io does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.</p>

<p>Aggregated Statistics<br />
VersionHistory.io may collect statistics about the behavior of visitors to its websites.However, VersionHistory.io does not disclose personally-identifying information other than as described below.</p>

<p>Protection of Certain Personally-Identifying Information <br />
VersionHistory.io discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors, and affiliated organizations that (i) need to know that information in order to process it on VersionHistory.io’s behalf or to provide services available at VersionHistory.io’s websites, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using VersionHistory.io’s websites, you consent to the transfer of such information to them. VersionHistory.io will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors, and affiliated organizations, as described above, VersionHistory.io discloses potentially personally-identifying and personally-identifying information only when required to do so by law, or when VersionHistory.io believes in good faith that disclosure is reasonably necessary to protect the property or rights of VersionHistory.io, third parties, or the public at large. If you are a registered user of a VersionHistory.io website and have supplied your email address, VersionHistory.io may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what’s going on with VersionHistory.io and our products. We primarily use our blog to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. VersionHistory.io takes all measures reasonably necessary to protect against the unauthorized access, use, alteration, or destruction of potentially personally-identifying and personally-identifying information.
</p>

<p>Cookies<br />
A cookie is a string of information that a website stores on a visitor’s computer, and that the visitor’s browser provides to the website each time the visitor returns. VersionHistory.io uses cookies to help VersionHistory.io identify and track visitors, their usage of VersionHistory.io website, and their website access preferences. VersionHistory.io visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using VersionHistory.io’s websites, with the drawback that certain features of VersionHistory.io’s websites may not function properly without the aid of cookies.</p>

<p>Privacy Policy Changes <br />
Although most changes are likely to be minor, VersionHistory.io may change its Privacy Policy from time to time, and in VersionHistory.io’s sole discretion. VersionHistory.io encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.
        	</p>
           <p> Thanks to <a href="http://wordpress.org">WordPress.org</a> and <a href="http://creativecommons.org/licenses/by-sa/2.5/">Creative Commons</a></p>
        </div> 
      </div>
	</section>
