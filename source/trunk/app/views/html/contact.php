
	<section id="content">
	  <div class="bg-dark lt">
      <div class="container">
        <div class="m-b-lg m-t-lg">
          <h3 class="m-b-none">Contact</h3>
        </div>
      </div>
    </div>
    <div class="bg-white b-b b-light">
      <div class="container">      
        <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
          <li><a href="/">Home</a></li>
          <li class="active">Contact</li>
        </ul>
      </div>
    </div>
    <div class="container m-t m-b-xl">
		<h2>Contact form</h2>
		<div class="row">
			<div class="col-sm-8 m-t">
				<!--<form action="/form-contact" method="post" class="contact">-->
                                <?php echo Form::open(array('action' => 'ClientController@postFormContact', 'method' => "POST", 'class'=>"contact")); ?>
					<div class="row form-group">
						<div class="col-sm-6">
							<label for="i1">Name:</label><input name="name" id="i1" type="text" class="form-control" />
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-6">
							<label for="i2">E-mail:</label><input name="email" id="i2" type="email" class="form-control" />
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-6">
							<label for="i3">Subject:</label><input name="subject" id="i3" type="text" class="form-control" />
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label for="i4">Message:</label><textarea name="message" id="i4" cols="30" rows="10" class="form-control"></textarea>
						</div>
					</div>
					<button type="submit" name="submit" class="btn btn-lg btn-dark"><i class="fa fa-paper-plane dark"></i> &nbsp;<span>Send</span></button>
                                <?php echo Form::close(); ?>
				<!--</form>-->
			</div>
			<div class="col-sm-4 m-t">
				<div class="contact-info">
					<h4><i class="fa fa-envelope"></i> Contact info</h4>
					<p>
						General questions or information: <br>
						<a href="mailto:info@versionhistory.io">info@versionhistory.io</a>
					</p>
					<br>
					<p>
						Support questions: <br>
						<a href="mailto:support@versionhistory.io">support@versionhistory.io</a>
					</p>
				</div>
			</div>
		</div>
    </div>
	</section>

