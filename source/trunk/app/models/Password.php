<?php

class Password extends Eloquent {
    
	protected $table = 'password_reminders';
    
	protected $hidden = array();  
        
        protected $fillable = array(
            'email'
        );
}