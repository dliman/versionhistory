<?php

class UserAdmin extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_admin';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array(
        'password',
    );

    protected $fillable = array(
        'password',
        'username',
    );

    public static function isAdmin() {
        return Session::has('superAdmin');
    }
    
    public static function adminId() {
        if(Session::has('superAdmin')) {
            return Session::get('superAdmin',0);
        } else {
            return 0;
        }
    }
    
    public static function attempt($credentials = array()) {
        
        if(!isset($credentials['username']) || !isset($credentials['password'])) {
            return false;
        }
        
        $user = UserAdmin::whereUsername($credentials['username'])->first();
        
        if(!empty($user->password) && password_verify($credentials['password'], $user->password)) {
            Session::set('superAdmin', $user->id);
            return true;
        } else {
            return false;
        }
    }
    
    public static function logout() {
        Session::remove('superAdmin');
    }
    
    private function getRules() {            
        
        return array(
            'username' => 'required',
            'password' => 'required'
        );
    }
        


    private function setCredentials() {
        return array( 
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );
    }
        
        

       

    public static function validate($input) {
        
        return Validator::make($input, $this->getRules() );
    }
}
