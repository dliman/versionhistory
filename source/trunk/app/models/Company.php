<?php

class Company extends \Eloquent {
	protected $table = 'companies';
    
	protected $hidden = array();  
        
        protected $fillable = array(
            'name',
            'slug',
           
        );
        
        public $company_id = null;




        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(
                    'custom_unique' => "The Company URL has already been taken.",
                    'name.required' => "Company Name field is required."
                ),
                'edit'=> array(
                        'custom_unique' => "The Company URL has already been taken."                        
                ),
            );
            return $messages[$key];
        }


        private function getRules($key) {
            $rules = array(                
                'create' => array(
                    'name'=> 'required|max:255|unique:companies,name|custom_unique:companies,slug',
                    'slug'=> 'max:255|regex:/^[a-z0-9_-]+$/'
                ),
                'edit' => array(
                    'name'=> "required|max:255|unique:companies,name,{$this->company_id},id|custom_unique:companies,slug,{$this->company_id},id",
                    'slug'=> 'required|max:255|regex:/^[a-z0-9_-]+$/',
                    'password'              => 'required|min:6|max:64',
                    'password_confirmation' => 'required|min:6|max:64|same:password',
                )
            );
            return $rules[$key];
        }
        
        public function setEditCredentials() {
            $username = Input::get('p_username');
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $username));
            return array(
                'name'=>$username,
                'slug'=>$slug,
                'password' => Input::get('p_password'),
                'password_confirmation' => Input::get('p_password_confirmation')
            );            
        }
        
        public function setCredentials(){
            $username = Input::get('username_signup');
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $username));
            return array(
                'name'=>$username, 
                'slug'=>$slug
            );
        }

        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages);
        }
        
        public function user() {
            return $this->hasMany('User', 'company_id');
        }
        
        public function project() {
            return $this->hasMany('Project', 'company_id');
        }
        
        static public function countPlanProjects($company_id) {
            $userAdmin = User::whereCompany_id($company_id)->whereRaw('id = parent_id')->first();
            $pricePlans = Config::get('app.plan', array());
            return isset($pricePlans[$userAdmin->pricing_plan]['projects'])?$pricePlans[$userAdmin->pricing_plan]['projects']: 10;

        }
        
}