<?php

class FeatureSetting extends Eloquent {
	
        protected $table = 'feature_settings';
    
	protected $hidden = array(
            'project_id',
            'user_id'
        );  
        
        protected $fillable = array(
            'public_private',
            'show_search_box',
            'show_feature_request_box',
            'show_download_pdf',
            'show_signup_email',
            'project_id',
            'user_id',
        );
        
        private function getCustomValidateMessages($key) {
            $messages = array();
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array();
            return $rules[$key];   
        }
        
        public function setCredentials() {
            return array( 
                'public_private'=> 'public',//Input::get('public_private'),
                'show_search_box'=> Input::get('show_search_box') == 'yes' ? 'yes' : 'no' ,
                'show_feature_request_box'=> Input::get('show_feature_request_box') == 'yes' ? 'yes' : 'no',
                'show_download_pdf'=> Input::get('show_download_pdf') == 'yes' ? 'yes' : 'no',
                'show_signup_email'=> Input::get('show_signup_email') == 'yes' ? 'yes' : 'no',
                'project_id'=> Input::get('project_id') ? Input::get('project_id') : 0 ,
                'user_id'=>Auth::user()->parent_id
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
}