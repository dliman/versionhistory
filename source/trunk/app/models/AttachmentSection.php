<?php

class AttachmentSection extends \Eloquent {
	protected $table = 'attachment_sections';
    
	protected $hidden = array();  
        
        protected $fillable = array(
            'section_id',
            'attachment',
        );
        
        public function section() {
            return $this->belongsTo('Section', 'section_id');
        }
}