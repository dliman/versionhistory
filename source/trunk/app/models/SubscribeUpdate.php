<?php

class SubscribeUpdate extends Eloquent {
    
	protected $table = 'subscribe_updates';
    
	protected $hidden = array(); 
        
        protected $fillable = array(
            'email',
            'project_id',
            'user_id',
            'company_id',
            'token'
        );
        
        public function project() {
            return $this->belongsTo('Project', 'project_id');
        }
        
        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(),
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'email' => 'required|email|min:6|max:64',
                    'project_id' => 'required|integer',
                    'token'      => 'unique:subscribe_updates'     
                ), 
                
            );
            return $rules[$key];   
        }
        
        public function setCredentials() {
            return array( 
                'email'         => Input::get('email'),
                'project_id'    => Input::get('project_id'),
                'user_id'       => Auth::id(),
                'company_id'    => Auth::user()->company_id,
                'token'         => Illuminate\Support\Str::random(60),
            );
        }
        
        public function setClientCredentials() {
            return array(
                'email'         => Input::get('my_email'),
                'project_id'    => Input::get('project_id'),
                'user_id'       => Input::get('user_id'),
                'token'         => Illuminate\Support\Str::random(60)
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
       
}