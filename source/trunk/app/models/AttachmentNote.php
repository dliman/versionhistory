<?php

class AttachmentNote extends Eloquent {
	
    protected $table = 'attachment_notes';
    
	protected $hidden = array();  
        
        protected $fillable = array(
            'note_id',
            'attachment',
        );
        
        public function note() {
            return $this->belongsTo('Note', 'note_id');
        }
}

