<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Laravel\Cashier\BillableTrait;
use Laravel\Cashier\BillableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, BillableInterface {

	use UserTrait, RemindableTrait, BillableTrait;
        use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
        
        protected $cardUpFront = false;
        
        protected $dates = ['trial_ends_at', 'subscription_ends_at', 'deleted_at'];
        
        

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array(
            'password', 
            'parent_id', 
            'remember_token'
        );  
        
        protected $fillable = array(
            'email', 
            'company_id', 
            'password', 
            'first_name', 
            'last_name', 
            'phone', 
            'resive', 
            'parent_id',
            'pricing_plan',
            'free'
        );
        
        private function getRules($key) {
            $uid = Auth::id();
            $rules = array(
                'auth' => array(
                    'email'     => 'required|min:5|max:64|email',
                    'password'  => 'required'
                ),
                'create' => array(
                    'email'                 => 'required|min:6|max:64|email|unique:users',
                    'company_id'            => 'required|integer',
                    'password'              => 'required|min:6|max:64',
                    'password_confirmation' => 'required|min:6|max:64|same:password',
                    'pricing_plan'          => 'required|alpha_num',
                ),
                'edit' => array(
                    'email'                 => "required|min:6|max:64|email|unique:users,email,{$uid},id",
                    'password'              => 'required|min:6|max:64',
                    'password_confirmation' => 'required|min:6|max:64|same:password',
                    'first_name'            => 'alpha|max:255',
                    'last_name'             => 'alpha|max:255'
                ),
                'edit-account'  => array(
                    'email'    => 'required|min:6|max:64|email|unique:users',
                    'password' => 'required|min:6|max:64',
                ),
                'edit-account-modal' => array(
                    'password'              => 'required|min:6|max:64',
                    'password_confirmation' => 'required|min:6|max:64|same:password',
                ),
                'trial' => array(
                    'trial_ends_at' => 'required|date',
                    'user_id'       => 'required|integer'
                ),
                'free' => array(
                    'free' => 'required|in:yes,no',
                    'user_id' => 'required|integer'
                ),
            );
            return $rules[$key];
        }
        
        public function setFreeCredentials() {
            return array(
                'free' => Input::get('free'),
                'user_id' => Input::get('user_id')
            );
        }
        
        public function setTrialCredentials() {
            return array(
                'trial_ends_at' => Input::get('date_trial'),
                'user_id' => Input::get('user_id')
            );
        }
        
        public function setEditAccountCredentialsModal() {
            return array( 
                'email'                 => Input::get('m_email'),
                'password'              => Input::get('m_password'), 
                'password_confirmation' => Input::get('m_password_confirmation'),
                'first_name'            => Input::get('m_fname'), 
                'last_name'             => Input::get('m_lname'), 
                'phone'                 => Input::get('m_phone')                
            );
        }
        
        
        public function setEditAccountCredentials($company_id) {
            
            return array(
                'email'          => Input::get('iuaEmailAddress'),
		'company_id'     => $company_id,
                'password'       => Input::get('iuaPassword'), 
                'parent_id'      => Auth::id()
            );
        }


        public function setAuthCredentials() {
            return array( 
                'email' => Input::get('email_signin'),
                'password' => Input::get('password_signin')
            );
        }
        
        public function setCreateCredentials($company_id) {
            return array( 
                    'email'                 => Input::get('email_signup'),
                    'company_id'            => $company_id,
                    'password'              => Input::get('password_signup'), 
                    'password_confirmation' => Input::get('password_confirmation'),
                    'pricing_plan'          => Input::get('pricing_plan'),
                );
        }
        
        public function setEditCredentials(){
            return array(
                'first_name'=> Input::get('p_fname'), 
                'last_name'=> Input::get('p_lname'), 
                'phone'=> Input::get('p_phone'), 
                'user_id'=>Auth::id(),
                'email' => Input::get('p_email'),
                'password' => Input::get('p_password'),
                'password_confirmation' => Input::get('p_password_confirmation'),
            );
        }

        public function validateEmailModal($email) {
            if($email != Input::get( 'm_email' ) ){
                return Validator::make( array( 'email' => Input::get( 'm_email' ) ), 
                            array( 'email' => 'required|min:6|max:64|email|unique:users' ) ); 
            } else {
                return Validator::make( array('email' => Input::get( 'm_email' ) ), 
                        array( 'email' => 'required|min:6|max:64|email' ) );
            }
        }

        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            return Validator::make($input, $rules );
        }
        
        public static function boot()
        {
            parent::boot();

            User::created(function($user)
            {
                $now = date('Y-m-d H:i:s');
                $defaults = array(
                    array(  'title'=>'added', 'show_label'=>'show', 'icon'=>'fa fa-plus-square', 'color'=>'#A9D86E', 'image'=>'', 'user_id'=>$user->id, 'project_id'=>'0', 'created_at'=> $now, 'updated_at'=>$now),
                    array(  'title'=>'fixed', 'show_label'=>'show', 'icon'=>'fa fa-check-square', 'color'=>'#59ace2', 'image'=>'', 'user_id'=>$user->id, 'project_id'=>'0', 'created_at'=> $now, 'updated_at'=>$now),
                    array(  'title'=>'removed', 'show_label'=>'show', 'icon'=>'fa fa-minus-square', 'color'=>'#FF6C60', 'image'=>'', 'user_id'=>$user->id, 'project_id'=>'0', 'created_at'=> $now, 'updated_at'=>$now),
                );
                DB::table('custom_types')->insert($defaults);
            });
            
        }
    
        public function company() {
            return $this->belongsTo('Company', 'company_id');
        }

        public function parent() {
            return $this->belongsTo('User', 'parent_id');
        }

        public function project() {
            return $this->hasMany('Project', 'user_id');
        }
        
        public function localization() {
            return $this->hasOne('LocalizationSetting', 'user_id', 'parent_id');
        }
        
        public function site_setting() {
            return $this->hasOne('ProjectSetting', 'user_id', 'parent_id');
        }
        
        public function layout_setting() {
            return $this->hasOne('LayoutSetting', 'user_id', 'parent_id');
        }
        
        public function icon_manager() {
            return $this->hasMany('CustomType', 'user_id', 'parent_id');
        }
        
        public function feature_setting() {
            return $this->hasOne('FeatureSetting', 'user_id', 'parent_id');
        }
        
        public function customize_email() {
            return $this->hasOne('CustomizeEmail', 'user_id', 'parent_id');
        }
        
        public static function getCurrentUserAllAccounts() {
            return User::whereParentId(Auth::user()->parent_id)->get();
        }

        public static function countCurrentUserAllAccounts() {
            return User::whereParentId(Auth::user()->parent_id)->count();
        }

        public function isAdminCompany() {
            return $this->parent_id == $this->id;
        }

        public function isFree() {
            if($this->isAdminCompany()) {
                return $this->free == 'yes';
            } else {
                $parent = User::find($this->parent_id);
                if(!empty($parent)) {
                    return $parent->free == 'yes';
                } else {
                    return false;
                }
            }
        }
}
