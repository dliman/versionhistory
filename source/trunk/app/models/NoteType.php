<?php

class NoteType extends Eloquent {
	protected $table = 'note_types';
    
	protected $hidden = array(
            'project_id',
            'user_id'
        );  
        
        protected $fillable = array(
            'title',
            'project_id',
            'user_id',
        );
}