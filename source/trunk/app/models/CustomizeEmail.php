<?php

class CustomizeEmail extends Eloquent {
    
	protected $table = 'customize_email';
    
	protected $hidden = array(
            'user_id',
            'project_id'
        );  
        
        protected $fillable = array(
           
            'subject',
            'body',
            'send',
            'user_id',
            'project_id',
            'send_automatic'
        );
        
        public function user() {
            return $this->belongsTo('User', 'user_id');
        }
        
        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(),
                'client' => array(),
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'subject'   => 'required',
                    'body'      => 'required',
                    'project_id'=>'required|integer'
                ),
                'client' => array(
                    'email'         => 'required|email|min:6|max:64',
                    'name'          => 'required',
                    'message_text'  => 'required|max:255',
                    'subject'       => 'required'
                )
            );
            return $rules[$key];   
        }
        
        public function setCredentials() {
            return array( 
                'subject'   => Input::get('subject'),
                'body'      => Input::get('mess_body'),
                'user_id'   => Auth::id(),
                'project_id'=> Input::get('project_id'),
                'company_id'=> Auth::user()->company_id,
            );
        }
        
        public function setCredentialsClient() {
            return array(
                'name'          => Input::get('name'),
                'email'         => Input::get('email'),
                'message_text'  => Input::get('message'),
                'subject'       => Input::get('subject'),
                'adminEmail'    => Config::get('app.adminEmail', 'info@versionhistory.io')
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
        
}