<?php

class ProjectSetting extends Eloquent {
    
        protected $table = 'project_settings';
    
	protected $hidden = array(
            'user_id',
            'project_id'
        );  
        
        protected $fillable = array(
            'page_title',
            'meta_description',
            'meta_keywords',
            'custom_domain',
            'upload_logo',
            'upload_favicon',
            'user_id',
            'project_id'            
        );
        
        public $user_id = null;


        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(
                    'custom_domain'=>'Custom Domain is not linked to server IP',
                ),
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'page_title'=>'max:255',
                    'user_id'=>'required|integer',
                    'custom_domain'=> 'max:255|regex:/^[a-z0-9_\-\.]+$/|custom_domain|unique:project_settings,custom_domain' . (is_null($this->user_id) ? '' : ",{$this->user_id},user_id" ),
          
                ),
            );
            return $rules[$key];   
        }
        
        public function setCredentials() {
            return array( 
                'page_title' => Input::get('page_title'),
                'meta_description' => Input::get('meta_description'),
                'meta_keywords' => Input::get('meta_keywords'),
                'custom_domain' => Input::get('custom_domain'),
                'user_id' => Auth::user()->parent_id,
                'project_id' => Input::get('project_id')         
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
        
        static public function getDefaultSettings($parent_id, $slug, $project_slug, $default = false) {
            $projectSettings = ProjectSetting::whereUserId($parent_id)->first();
            if(!$projectSettings || $default) {
                $projectSettings = New ProjectSetting();
            }
            if( empty($projectSettings->meta_description)
                || empty($projectSettings->meta_keywords)
                || empty($projectSettings->page_title)
                || $default
              ) {
                $company = Company::whereSlug($slug)->first();
                $projectObj = Project::whereCompany_id($company->id)->whereNull('inactived_at')->with(array(
                    'section'       => function($query) { 
                            $query->whereArchived('no')->orderBy('versions_order')->orderBy('created_at', 'desc'); 
                    }))->orderBy('projects_order')->orderBy('updated_at', 'desc')->latest();
                if($project_slug) {
                    $projectObj->whereSlug($project_slug);
                }
                $projects = $projectObj->get()->toArray();
                $versionName1 = !empty($projects[0]['section'][0]['title'])?$projects[0]['section'][0]['title']:'';
                $versionName2 = !empty($projects[0]['section'][1]['title'])?$projects[0]['section'][1]['title']:'';
                $versionName3 = !empty($projects[0]['section'][2]['title'])?$projects[0]['section'][2]['title']:'';
            }
            if(empty($projectSettings->meta_description) || $default) {
                $projectSettings->meta_description = "{$company->name}'s Version History";
                if($versionName1) {
                    $projectSettings->meta_description .= " of {$versionName1}";
                }
                if($versionName2) {
                    $projectSettings->meta_description .= ", {$versionName2}";
                }
                if($versionName3) {
                    $projectSettings->meta_description .= " and {$versionName3}";
                }
            }
            if(empty($projectSettings->meta_keywords) || $default) {
                $projectSettings->meta_keywords = "{$company->name}";
                if($versionName1) {
                    $projectSettings->meta_keywords .= ", {$versionName1}";
                }
                if($versionName2) {
                    $projectSettings->meta_keywords .= ", {$versionName2}";
                }
                if($versionName3) {
                    $projectSettings->meta_keywords .= ", {$versionName3}";
                }
                $projectSettings->meta_keywords .= ", version history, version updates, change log, release notes, versionhistory.io";
            }
            if(empty($projectSettings->page_title) || $default) {
                $projectSettings->page_title = "{$company->name}'s Version History";
            }
            return $projectSettings;
        }
        
        
}