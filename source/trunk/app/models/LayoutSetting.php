<?php

class LayoutSetting extends Eloquent {
    
        protected $table = 'layout_settings';
    
	protected $hidden = array(
            'user_id',
            'project_id'
        );  
        
        protected $fillable = array(
            'header_image',
            'header_text_color',
            'header_text_show',
            'header_background',
            'hide_header_area',
            'background_image',
            'background_color',
            'header_code',
            'footer_code',
            'custom_css',
            'project_id',
            'user_id',
        );
}