<?php

class Project extends Eloquent {
    
	protected $table = 'projects';
    
	protected $hidden = array(
            'user_id'
        );  

        protected $dates = ['inactived_at'];

        protected $fillable = array(
            'title',
            'slug',
            'description',
            'status',
            'user_id',
            'company_id',
            'projects_order'
        );

        public function section() {
            return $this->hasMany('Section', 'project_id');
        }
        
        public function user() {
            return $this->belongsTo('User', 'user_id');
        }
        
        public function company() {
            return $this->belongsTo('Company', 'company_id');
        }
        
         private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(
                    'required' => 'The Project Name field is required.',
                    'required_with' => 'Please note that Project Name field should contain at least one letter or digit.',
                    'unique' => 'The Project Name has already been taken.'
                ),
                'edit' => array(),
                
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'title'       => 'required|max:255',
                    'slug'        => 'required_with:title|max:255|regex:/^[a-z0-9_-]+$/|unique:projects,slug,NULL,NULL,' 
                    . 'user_id,' . Auth::id() . ',company_id,' . Auth::user()->company_id,
                ),
                'edit' => array(
                   'title'=>'required|max:255'
                )
            );
            return $rules[$key];   
        }
        
        public function setCreateCredentials() {
            $title = Input::get( 'title' );
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
            return array( 
                'title' => $title,
                'slug'=> $slug,
                'description' => Input::get('description'),
                'status' => Input::get('status'),
                'user_id'=> Auth::id(),
                'company_id'=>Auth::user()->company_id
            );
        }
        
        public function setEditCredentials() {
            $title = Input::get( 'title' );
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
            return array( 
                'title'=>$title,
                'slug'=>$slug,
                'description'=>Input::get('description'),
                'status'=>Input::get('status'),
                'user_id'=>Auth::id()
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
        
        public function getProjectSections($project, $slug, $section) {
            return DB::select("SELECT `s`.`id` as sid, `p`.`id` as pid, `c`.`id` as cid 
                FROM `projects` p
                LEFT JOIN `users` u ON `u`.`id`=`p`.`user_id`
                LEFT JOIN `companies` c ON `c`.`id`=`u`.`company_id` 
                LEFT JOIN `sections` s ON `s`.`project_id`=`p`.`id` 
                WHERE `p`.`slug`=? AND `p`.`company_id`=? AND `c`.`slug`=? AND `s`.`slug`=? ",
                array($project, Auth::user()->company_id, $slug, $section ));
        }
        
        public function getProjectNotes($project, $note, $section, $slug) {
            return DB::select("SELECT `s`.`id` as sid, `p`.`id` as pid, `n`.`id` as nid 
                FROM `projects` p 
                LEFT JOIN `companies` c ON `c`.`id`=`p`.`company_id` 
                LEFT JOIN `sections` s ON `s`.`project_id`=`p`.`id`  
                LEFT JOIN `notes` n ON `n`.`section_id`=`s`.`id` 
                WHERE `p`.`slug`=? AND `p`.`company_id`=? AND `n`.`slug`=? AND `s`.`slug`=? AND `c`.`slug`=?",
                        array($project, Auth::user()->company_id, $note, $section, $slug));
        }
        
        public function getProjects($project, $slug) {
            return DB::select("SELECT `p`.* 
                FROM `projects` p
                LEFT JOIN `users` u ON `u`.`id`=`p`.`user_id`
                LEFT JOIN `companies` c ON `c`.`id`=`u`.`company_id` 
                WHERE `p`.`slug`=? AND `p`.`company_id`=? AND `c`.`slug`=? ",
                array($project, Auth::user()->company_id, $slug ));
        }
        
        public function getProjectInfo($project_slug, $slug) {
            return DB::select("SELECT `p`.`id` as pid, `p`.`title` as p_title, `p`.`user_id` as uid 
                FROM `projects` p 
                LEFT JOIN `companies` c ON `c`.`id`=`p`.`company_id` 
                WHERE `p`.`slug`=? AND `p`.`company_id`=? AND `c`.`slug`=?  ", 
                array( $project_slug, Auth::user()->company_id, $slug ));
        }
        
        public function getProjectId($project, $slug) {
            return DB::select("SELECT `p`.`id` as pid 
                FROM `projects` p
                LEFT JOIN `users` u ON `u`.`id`=`p`.`user_id`
                LEFT JOIN `companies` c ON `c`.`id`=`u`.`company_id` 
                WHERE `p`.`slug`=? AND `p`.`company_id`=? AND `c`.`slug`=? ", 
                array( $project, Auth::user()->company_id, $slug ));
        }
        
        public function deleteProject($id) {
            return DB::delete("DELETE p, s, n FROM `projects` p 
                        LEFT JOIN (`sections` s, `notes` n) 
                        ON(`s`.`project_id`=`p`.`id` AND `n`.`section_id`=`s`.`id`) 
                        WHERE `p`.`id`=?  AND `p`.`company_id`=? ", array($id, Auth::user()->company_id));
        }
        
        public function getProjectClient($project_slug, $slug) {
            return DB::select("SELECT `p`.`id` as pid, `p`.`user_id` as uid,
                    `p`.`company_id`, `p`.`status` 
                FROM `projects` p 
                LEFT JOIN `companies` c ON `c`.`id`=`p`.`company_id`  
                WHERE `p`.`inactived_at` IS NULL AND `p`.`slug`=? AND `c`.`slug`=? ", array($project_slug, $slug));
        }
        
        public function getProjectClientNoteDetails($project_slug, $slug, $section_slug, $note_slug) {
            return DB::select("SELECT `s`.`id` as sid, `p`.`id` as pid, `n`.`id` as nid, `p`.`user_id` as uid,
                    `p`.`company_id`, `p`.`status` 
                FROM `projects` p 
                LEFT JOIN `companies` c ON `c`.`id`=`p`.`company_id`  
                LEFT JOIN `sections` s ON `s`.`project_id`=`p`.`id` 
                LEFT JOIN `notes` n ON ``.`section_id`=`s`.`id` 
                WHERE `p`.`slug`=? 
                AND `c`.`slug`=? 
                AND `s`.`slug`=? 
                AND `n`.`slug`=? ", array($project_slug, $slug, $section_slug, $note_slug ));
        }
        
}