<?php

class IconsSetting extends Eloquent {
	
        protected $table = 'icons';
    
	protected $hidden = array(
            'user_id'
        );  
        
        protected $fillable = array(
            'key',
            'value',
            'user_id'
        );
}