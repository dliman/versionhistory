<?php

class LocalizationSetting extends Eloquent {
	protected $table = 'localization_settings';
    
	protected $hidden = array(
            'user_id',
            'project_id',
        );  
        
        protected $fillable = array(
            'date_format',
            'time_zone',
            'user_id',
            'project_id'
            
        );
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'date_format'=>'required',
                    'time_zone'=>'required',
                    'user_id'=>'required|integer'
                ),
            );
            return $rules[$key];   
        }
        
        public function setCredentials() {
            return array( 
                'date_format'=> Input::get('date_format'),
                'time_zone'=> Input::get('time_zone'),
                'user_id'=> Auth::user()->parent_id,
                'project_id'=> Input::get('project_id')          
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            return Validator::make($input, $rules );
        }
}