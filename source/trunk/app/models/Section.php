<?php

class Section extends Eloquent {
	
	protected $table = 'sections';
    
	protected $hidden = array(
            'user_id',
            'project_id',
        );  
        
        protected $fillable = array(
            'title',
            'slug',
            'description',
            'user_id',
            'project_id',
            'company_id',
            'released_at',
            'versions_order',
            'archived'
            
        );
        
        public function project() {
            return $this->belongsTo('Project', 'project_id');
        }
        
        public function note(){
            return $this->hasMany('Note', 'section_id');
        }
        
                
        public function attachment() {
            return $this->hasMany('AttachmentSection', 'section_id');
        }
        
        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(
                    'required' => 'The Version Name field is required.',
                    'required_with' => 'Please note that Version Name field should contain at least one letter or digit.',
                    'unique' => 'The Version Name has already been taken.'
                ),
                'edit' => array(),
                
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'title'=>'required|max:255',
                    'slug'=>'required_with:title|max:255|regex:/^[a-z0-9_-]+$/|unique:sections,slug,NULL,NULL,' 
                            .'user_id,' . Auth::id() . ',company_id,' . Input::get('company_id'). ',project_id,' . Input::get('project_id'),
                    'user_id'=>'required|integer',
                    'project_id'=>'required|integer',
                    'company_id'=>'required|integer'
                ),
                'edit' => array(
                   'title'=> 'required|max:255'
                )
            );
            return $rules[$key];   
        }
        
        public function setCreateCredentials() {
            $title = Input::get( 'title' );
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
            $released_at = DateTime::createFromFormat('m-d-Y', Input::get('released_at'));
            return array( 
                'title'=>$title,
                'slug'=>$slug,
                'description'=> Input::get('editor1'),
                'user_id'=> Auth::id(),
                'project_id'=>Input::get('project_id'),
                'released_at'=>$released_at,
                'company_id'=>Input::get('company_id')
            );
        }
        
        public function setEditCredentials() {
            $released_at = DateTime::createFromFormat('m-d-Y', Input::get('released_at'));
            $title = Input::get( 'title' );
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
            return array( 
                'title'=>$title,
                'slug'=>$slug,
                'released_at'=>$released_at,
                'description'=>Input::get('editor1'),
                'archived'=>(Input::get('archived')=='yes')?'yes':'no',
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
        
        public function getSection($project, $slug, $section) {
            return DB::select("SELECT `s`.`id` as sid 
                FROM `projects` p
                LEFT JOIN `companies` c ON `c`.`id`=`p`.`company_id` 
                LEFT JOIN `sections` s ON `s`.`project_id`=`p`.`id`  AND `s`.`company_id`=`c`.`id`
                WHERE `p`.`slug`=? AND `p`.`company_id`=? AND `c`.`slug`=? AND `s`.`slug`=?",
                array($project, Auth::user()->company_id, $slug, $section) );
        }
        
        public function deleteSection($sid) {
            return DB::delete("DELETE s, n, ats, atn FROM `sections` s 
                        LEFT JOIN (`notes` n, `attachment_sections` ats, `attachment_notes` atn  )
                        ON ( `n`.`section_id`=`s`.`id` AND `ats`.`section_id`=`s`.`id` AND `atn`.`note_id`=`n`.`id` ) 
                        WHERE `s`.`id`=?  AND `s`.`company_id`='".Auth::user()->company_id."'", array($sid));
        }
        
        public function getSearchResult($select_sort, $in) {
            if ($select_sort == 1) {
                    $order = '';
                } else {
                    $order = ' ORDER BY `n`.`updated_at` DESC';
                }
                return DB::select("SELECT   `s`.`title` as s_title, 
                                            `s`.`slug` as s_slug, 
                                            `s`.`description` as s_description,
                                            `n`.`title` as n_title, 
                                            `n`.`slug` as n_slug,
                                            `n`.`description` as n_description,
                                            `t`.`title` as t_title,
                                            `t`.`icon` as t_icon,
                                            `t`.`color` as t_color,
                                            `t`.`image` as t_image,
                                            `t`.`show_label` as t_show_label,
                                            `p`.`slug` as p_slug,
                                            `p`.`user_id` as uid,
                                            `c`.`slug` as c_slug
                    FROM `sections` s 
                    LEFT JOIN `projects` p ON `p`.`id`=`s`.`project_id`
                    LEFT JOIN `companies` c ON `c`.`id`=`s`.`company_id`
                    LEFT JOIN `notes` n ON `n`.`section_id`=`s`.`id`
                    LEFT JOIN `custom_types` t ON `t`.`id`=`n`.`type_id`
                    WHERE `n`.`id` IN ({$in}){$order}");
        }
        
}