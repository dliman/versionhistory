<?php

class Note extends Eloquent {
	
	protected $table = 'notes';
    
	protected $hidden = array(
            'user_id',
        );  
        
        protected $fillable = array(
            'title',
            'slug',
            'type_id',
            'description',
            'user_id',
            'section_id',
            'project_id',
            'company_id',
            'notes_order'
            
            
            
        );
        
        public function section() {
            return $this->belongsTo('Section', 'section_id');
        }
        public function attachment(){
            return $this->hasMany('AttachmentNote', 'note_id');
        }
        public function type() {
            return $this->belongsTo('CustomType', 'type_id');
        }
        
        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(
                    'required' => 'The Note Name field is required.',
                    'required_with' => 'Please note that Note Name field should contain at least one letter or digit.',
                    'unique' => 'The Note Name has already been taken.'
                ),
                'edit' => array(),
                
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'title'=>'required|max:255',
                    'slug'=>'required_with:title|max:255|regex:/^[a-z0-9_-]+$/|unique:notes,slug,NULL,NULL,' 
                        .'user_id,' . Auth::id() . ',section_id,' . Input::get('section_id'). ',project_id,' . Input::get('project_id'),
                    'user_id'=>'required|integer',
                    'project_id'=>'required|integer',
                    'company_id'=>'required|integer'
                ),
                'edit' => array(
                    'title'=> 'required|max:255',
                    'type_id'=> 'required' 
                )
            );
            return $rules[$key];   
        }
        
        public function setCredentials() {
            $title = Input::get( 'title' );
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
            return array( 
                'title'=>$title,
                'slug'=>$slug,
                'type_id'=>Input::get('type_id'),
                'description'=>Input::get('editor1'),
                'user_id'=> Auth::id(),
                'section_id'=>Input::get('section_id'),
                'project_id'=>Input::get('project_id'),
                'company_id'=>Input::get('company_id'),
            );
        }
        
        public function setEditCredentials() {
            $title = Input::get( 'title' );
            $slug = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $title));
            return array( 
                'title'=>$title,
                'slug'=>$slug,
                'type_id'=> Input::get('type_id'),
                'description'=> Input::get('editor1')
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
        
        public function deleteNote($nid) {
            return DB::delete("DELETE n, atn FROM `notes` n 
                        LEFT JOIN `attachment_notes` atn ON  `atn`.`note_id`=`n`.`id`  
                        WHERE `n`.`id`=?  AND `n`.`company_id`='".Auth::user()->company_id."'", array($nid));
        }
        
       
}