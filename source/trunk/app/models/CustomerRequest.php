<?php

class CustomerRequest extends Eloquent {
	protected $table = 'customer_requests';
    
	protected $hidden = array(
            'project_id',
            'type_id',
        );  
        
        protected $fillable = array(
            'type_id',
            'email',
            'description',
            'project_id',
            'hidden'
        );
        
        public function project() {
            return $this->belongsTo('Project', 'project_id');
        }
        
        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(
                    
                ),
                'edit' => array(),
                
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'email'      => 'required|email|min:6|max:64',
                    'project_id' => 'required|integer',
                    'type_id'    => 'required|integer'
                ),
                'edit' => array(
                   
                )
            );
            return $rules[$key];   
        }
        
        public function setCreateCredentials() {
            
            return array( 
                'type_id'       => Input::get('feature_type'),
                'email'         => Input::get('my_email'),
                'description'   => Input::get('description'),
                'project_id'    => Input::get('project_id')
            );
        }
        
        public function setEditCredentials() {
           
            return array( 
               
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
}