<?php

class CustomType extends Eloquent {
    
	protected $table = 'custom_types';
    
	protected $hidden = array(
            'user_id',
            'project_id'
        );  
        
        protected $fillable = array(
            'title',
            'show_label',
            'icon',
            'color',
            'image',
            'user_id',
            'project_id',
            'company_id'
        );
        public function note() {
            return $this->hasMany('Note', 'type_id');
        }
        
        private function getCustomValidateMessages($key) {
            $messages = array(
                'create' => array(),
            );
            return $messages[$key];
        }
        
        private function getRules($key) {
            $rules = array(
                'create' => array(
                    'title'=>'max:255'
                ),
            );
            return $rules[$key];   
        }
        
        public function setCredentials() {
            return array( 
                'title'=> Input::get('type_title'),
                'show_label'=>Input::get( 'show_label' ) ? 'show' : 'hide',
                'icon'=> ((Input::get('icon')=='Select Icon...') || (Input::get('icon')=='')) ? '' : Input::get('icon'),
                'color'=> Input::get('type_color'),
                'image'=> $filename_icon_file,
                'user_id'=> Auth::user()->parent_id,
                'project_id'=> Input::get('project_id')
            );
        }
        
        public function validate($input, $rulesKey) {
            $rules = $this->getRules($rulesKey);
            $messages = $this->getCustomValidateMessages($rulesKey);
            return Validator::make($input, $rules, $messages );
        }
}