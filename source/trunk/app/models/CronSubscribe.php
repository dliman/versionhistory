<?php

class CronSubscribe extends \Eloquent {

        protected $table = 'cron_subscribe';
    
	protected $hidden = array();  
        
        protected $fillable = array(
            'send',
            'project_id',
            'subject',
            'body',
            'status'
        );
       

}