<?php

class CustomValidate extends Illuminate\Validation\Validator
{

    public function validateCustomDomain($attribute, $value, $parameters)
    {
        $ip = gethostbyname($value);
        $ipArr = explode('.', $ip);
        $ipServ = explode('.', $_SERVER['SERVER_ADDR']);
        $isEqual = true;
        foreach ($ipServ as $key=>$value) {
            if(!isset($ipArr[$key]) || (isset($ipArr[$key]) && intval($ipServ[$key]) !== intval($ipArr[$key]))) {
                $isEqual = false;
            }
        }

        return $isEqual;
    }

    public function validateCustomUnique($attribute, $value, $parameters)
    {
        $value = strtolower(preg_replace('/[^a-zA-Z0-9_-]/', '', $value));
        if(CustomHelper::isExistSlug($value)) {
            return false;
        }
        return $this->validateUnique($attribute, $value, $parameters);
    }

}